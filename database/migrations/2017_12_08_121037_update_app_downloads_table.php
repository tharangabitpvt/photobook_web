<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\AppDownload;

class UpdateAppDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_downloads', function (Blueprint $table) {
            $table->dropColumn('app_id');
            $table->string('app_serial', 50)->nullable(true)->default(null)->after('id');
            $table->integer('user_id')->default(0)->change();
            $table->boolean('is_activated')->default(AppDownload::APP_ACTIVATED_FALSE)->change();
            $table->string('app_key', 20)->nullable(true)->change();
            $table->datetime('activated_date')->nullable(true)->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_downloads', function (Blueprint $table) {
            //
        });
    }
}
