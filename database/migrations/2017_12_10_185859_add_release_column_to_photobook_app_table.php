<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReleaseColumnToPhotobookAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photobook_apps', function (Blueprint $table) {
            $table->tinyInteger('release')->nullable(false)->default(\App\PhotobookApp::RELEASE_UPDATE)->after('stable_level');
            $table->tinyInteger('priority')->nullable(false)->default(\App\PhotobookApp::UPDATE_PRIORITY_HIGH)->after('release');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photobook_apps', function (Blueprint $table) {
            //
        });
    }
}
