<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhotobookAppTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photobook_apps', function (Blueprint $table) {
            $table->char('file_type', 10)->nullable('false')->default("")->after('architecture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photobook_apps', function (Blueprint $table) {
            $table->dropColumn('file_type');
        });
    }
}
