<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tem_name')->nullable(false);
            $table->integer('pages');
            $table->string('sizes');
            $table->integer('placeholders');
            $table->integer('user_id');
            $table->integer('version');
            $table->float('price', 8, 2);
            $table->char('currency', 5)->nullable(false)->default('LKR');
            $table->integer('downloads');
            $table->tinyInteger('stars');
            $table->boolean('published')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
