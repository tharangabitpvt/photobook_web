<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAppDownloads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_downloads', function (Blueprint $table) {
            $table->string('app_label', 50)->nullable(true)->default(null)->after('app_serial');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_downloads', function (Blueprint $table) {
            $table->dropColumn('app_label');
        });
    }
}
