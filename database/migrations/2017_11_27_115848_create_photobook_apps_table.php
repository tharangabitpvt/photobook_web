<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotobookAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photobook_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('version');
            $table->tinyInteger('stable_level');
            $table->string('platform', 50)->nullable(false);
            $table->string('architecture')->nullable(false);
            $table->string('release_name')->nullable(false);
            $table->string('change_log')->nullable(true);
            $table->timestamp('create_date');
            $table->integer('download_count');
            $table->boolean('published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photobook_apps');
    }
}
