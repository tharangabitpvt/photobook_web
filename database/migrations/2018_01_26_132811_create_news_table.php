<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ft_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('news_title', 50)->nullable(false)->default('');
            $table->string('news_body', 100)->nullable(false)->default('');
            $table->string('news_footer', 100)->nullable(false)->default('');
            $table->string('news_link')->nullable()->default(null);
            $table->boolean('published')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ft_news');
    }
}
