<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAppDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_downloads', function (Blueprint $table) {
            $table->datetime('download_date')->nullable(true)->defualt(null)->change();
            $table->renameColumn('download_date', 'activated_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_downloads', function (Blueprint $table) {
            //
        });
    }
}
