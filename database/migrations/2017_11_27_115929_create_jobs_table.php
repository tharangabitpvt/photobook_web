<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('job_type');
            $table->integer('user_id');
            $table->integer('file_count');
            $table->timestamp('create_date');
            $table->timestamp('due_date')->nullable();
            $table->mediumText('description');
            $table->boolean('is_completed');
            $table->timestamp('completed_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
