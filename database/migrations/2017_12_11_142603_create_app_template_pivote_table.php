<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTemplatePivoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps_templats', function (Blueprint $table) {
            $table->integer('app_id')->nullable(false)->default(0);
            $table->integer('template_id')->nullable(false)->default(0);
            $table->boolean('is_installed')->nullable(false)->default(0);
            $table->datetime('install_time')->nullable(true)->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps_templats');
    }
}
