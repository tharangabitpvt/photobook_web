<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone', 255)->nullable()->default(null)->change();
            $table->boolean('is_phone_validated')->nullable(false)->default(User::PHONE_VALIDATED_NO)->change();
            $table->integer('user_type')->nullable(false)->default(User::USER_TYPE_APP_USER)->change();
            $table->boolean('is_active')->nullable(false)->default(User::USER_INACTIVE)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
