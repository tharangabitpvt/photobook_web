<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->integer('user_id')->default(0)->change();
            $table->string('f_name', 100)->default('')->change();
            $table->string('l_name', 100)->default('')->change();
            $table->string('email', 100)->default('')->change();
            $table->mediumText('message')->default('')->change();
            $table->boolean('is_new')->defaut(\App\Message::NEW_MESSAGE_TRUE)->change();
            $table->boolean('is_replied')->default(\App\Message::REPLIED_FALSE)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            //
        });
    }
}
