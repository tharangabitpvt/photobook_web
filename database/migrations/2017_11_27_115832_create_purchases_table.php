<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->tinyInteger('purchase_type');
            $table->tinyInteger('payment_method');
            $table->float('amount', 8, 2);
            $table->char('currency', 5)->nullable(false)->default('LKR');
            $table->string('ref_no');
            $table->timestamp('purchase_date')->nullable();
            $table->tinyInteger('payment_status');
            $table->timestamp('paid_date')->nullable();
            $table->string('remarks')->nullable();
            $table->integer('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
