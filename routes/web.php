<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// about
Route::get('/about', function () {
    return view('about')->with('bodyClass', 'aboutus-page');
});

// products and services
Route::get('/products_and_services', function() {
	return view('products_and_services')->with('bodyClass', 'products-page');
})->name('products_and_services');

// tempaltes
Route::get('/photobook_templates', 'TemplatesController@allTemplates');

// downloads
Route::get('/downloads', 'PhotobookAppsController@getDownloads')->name('downloads');

// contact us
Route::get('/contact-us', function(){
	return view('contactus')->with('bodyClass', 'contactus-page');
});

// update currency rates
Route::get('/update_currency_rates', 'HelperController@updateCurrencyRates');

// user guide
Route::get('/user_guide/{lang}', 'AppDownloadsController@downloadUserGuide')->name('download_user_guide');

// downloads
/*Route::get('/about', function () {
    return view('/downloads');
});*/

// contact us
Route::post('/contactus/send', 'MessagesController@contactus');


// get template thumbnails using ajax
Route::get('/template/thumb', 'TemplatesController@getThumbnails');

// add items to collection
Route::post('/purchase/addtocart', 'PurchasesController@addToCollection');

// show shoping collection
Route::post('/purchase/show_collection', 'PurchasesController@showCollection');

Route::get('/purchase/checkout', 'PurchasesController@checkout');

Route::post('/purchase/complete_purchase', 'PurchasesController@completePurchase');

Route::get('/get_app_key', 'AppDownloadsController@getAppKey');

Route::get('/purchase/free_activation', 'PurchasesController@sendFreeAppKey')->name('free_app_key');

Route::get('/dashboard', [
	'as' => 'dashboard',
	'uses' => 'NotificationsController@dashboard']);

Route::post('/profile/update/{user_id}', [
	'as' => 'user.update',
	'uses' => 'UsersController@update']);

// app download
Route::post('/download_trail', 'AppDownloadsController@download');

Auth::routes();
//Route::post('/register', 'UsersController@register');

Route::get('/home', 'HomeController@index')->name('home');

// confirm email address
Route::get('/confirmemail/{accessToken}/{userId}', 'UsersController@activate');

//Route::post('/login')->name('auth.login');

Route::post('/login', 'LoginController@authenticate');


// admin routes
Route::prefix('admin')->group(function() {

	Route::get('/', 'HomeController@admin');

	// user prifile
	Route::get('profile/{id}', 'UsersController@profile')->name('admin.userprofile');

	// messages
	Route::get('/messages', 'MessagesController@index')->name('admin.messages');

	Route::get('/messages/view/{id}', 'MessagesController@view');

	Route::put('/messages/reply/{id}', 'MessagesController@reply')->name('message.reply');

	// delete messages
	Route::delete('/messages/delete', 'MessagesController@delete')->name('admin.messages.delete');

	// user manager
	Route::get('/user_manager', 'UsersController@index')->name('admin.user_manager');

	// create user
	Route::match(['get', 'post'], '/user_manager/create', 'UsersController@create')->name('admin.create_user');

	// edit user
	Route::get('/user_manager/edit/{id}', 'UsersController@edit')->name('admin.edit_user');

	// update use
	Route::put('/update_staff_user/{id}', 'UsersController@updateStaffUser')->name('admin.update_staff_user');

	// delete user
	Route::delete('/user_manager/delete', 'UsersController@delete')->name('admin.user.delete');

	// vew user trash
	Route::get('/user_manager/trash', 'UsersController@viewTrash')->name('admin.user_manager.trashed');

	// restoer users
	Route::post('/user_manager/restore', 'UsersController@restore')->name('admin.user.restore');

	// PhotoBook apps manager
	Route::get('/apps_manager', 'PhotobookAppsController@index')->name('admin.photobook_apps');

	// publish apps
	Route::post('/apps_manager/publish', 'PhotobookAppsController@publish')->name('admin.photobook_apps.publish');

	// add new photobook app
	Route::match(['get', 'post'], '/apps_manager/create', 'PhotobookAppsController@create')->name('admin.create_photobook_app');

	// edit app
	Route::get('/apps_manager/edit/{id}', 'PhotobookAppsController@edit')->name('admin.edit_app');

	// update app
	Route::put('/apps_manager/update/{id}', 'PhotobookAppsController@update')->name('admin.update_app');

	// Templates
	Route::get('/templates', 'TemplatesController@index')->name('admin.templates');

	// create template
	Route::match(['get', 'post'], '/templates/create', 'TemplatesController@create')->name('admin.create_template');

	// publish template
	Route::post('/templates/publish', 'TemplatesController@publish')->name('admin.publish_templates');

	// edit template
	Route::get('/templates/edit/{id}', 'TemplatesController@edit')->name('admin.edit_template');

	// update template
	Route::put('/templates/update/{id}', 'TemplatesController@update')->name('admin.update_template');

	// purchases
	Route::get('/purchases', 'PurchasesController@index')->name('admin.purchases');

	// set payment status
	Route::post('/purchases/set_payment_status', 'PurchasesController@setPaymentStatus')->name('admin.purchases.set_payment_status');

	// delete purchases
	Route::delete('/purchases/delete', 'PurchasesController@delete')->name('admin.purchases.delete');

	// go to invoice
	Route::get('/purchases/invoice/{invoice_no}', 'PurchasesController@invoice')->name('admin.purchases.invoice');

	// app downloads
	Route::get('/app_downloads', 'AppDownloadsController@index')->name('admin.app_downloads');
});