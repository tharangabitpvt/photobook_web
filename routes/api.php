<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/pb_activate', 'AppDownloadsController@activateApp');

Route::post('/pb_activate/confirm', 'AppDownloadsController@confirmActivateApp');

Route::post('/app_shop', 'HelperController@getShopItems');

Route::post('/set_app_name', 'AppDownloadsController@setAppName');

Route::post('/appupdates', 'AppUpdatesController@update');

Route::post('/appupdates/auth', 'AppUpdatesController@authUpdate');

Route::post('/appupdates/completed', 'AppUpdatesController@complete');

Route::post('/fixme', 'HelperController@fixMe');
