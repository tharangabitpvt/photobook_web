<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppActivationMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The app key instance.
     *
     * @var appkey
     */
    protected $appkey, $username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $appkey=null, String $username)
    {
        $this->appkey = $appkey;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "PhotoBook Activation Key";
        if(!$this->appkey) {
            $subject = "PhotoBook Activation";
        }

        return $this->view('emails.app_activation')
                    ->subject($subject)
                    ->with([
                        'appKey' => $this->appkey,
                        'userName' => $this->username,
                    ]);
    }
}
