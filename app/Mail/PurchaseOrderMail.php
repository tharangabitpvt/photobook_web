<?php

namespace App\Mail;

use App\Purchase;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchaseOrderMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The purchase instance.
     *
     * @var Purchase
     */
    protected $purchase;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $orderRef)
    {
        $purchaseModel = Purchase::where('ref_no', 'LIKE', $orderRef)->first();
        $this->purchase = $purchaseModel;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $user = User::find($this->purchase->user_id);

        $mailSubject = "Purchase Order (NOT PAID)";
        if($this->purchase->payment_status == Purchase::PAYMENT_STATUS_PENDING) {
            $mailSubject = "Purchase Order (PENDING)";
        } else if($this->purchase->payment_status == Purchase::PAYMENT_STATUS_PAID) {
            $mailSubject = "Purchase Order (PAID)";
        } else if($this->purchase->payment_status == Purchase::PAYMENT_STATUS_CANCELED) {
            $mailSubject = "Purchase Order (CANCELED)";
        } else if($this->purchase->payment_status == Purchase::PAYMENT_STATUS_REJECTED) {
            $mailSubject = "Purchase Order (REJECTED)";
        }

        return $this->view('emails.purchase_order')
                    ->subject($mailSubject)
                    ->with([
                        'userName' => $user->name,
                        'orderRefNo' => $this->purchase->ref_no,
                        'paymentStatus' => $this->purchase->payment_status,
                    ])
                    ->attach(storage_path('app/tmp/' . $this->purchase->ref_no . '.pdf'));
    }
}
