<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Message;

class ContactusAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact_us_admin')
                    ->subject('Enquiry')
                    ->with([
                        'contactName' => $this->message->f_name . " " . $this->message->l_name,
                        'contactEmail' => $this->message->email,
                        'contactPhone' => $this->message->phone,
                        'contactReason' => $this->message->product,
                        'contactMessage' => $this->message->message,
                    ]);
    }
}
