<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Message;

class MessageReplyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reply email')
                    ->subject('Reply for your enquiry')
                    ->view('emails.message_reply')->with([
                        'username' => $this->message->f_name . " " . $this->message->l_name,
                        'messageReason' => $this->message->product,
                        'originalMessage' => $this->message->message,
                        'messageBody' => $this->message->reply,
        ]);
    }
}
