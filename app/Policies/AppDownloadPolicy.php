<?php

namespace App\Policies;

use App\User;
use App\AppDownload;
use Illuminate\Auth\Access\HandlesAuthorization;

class AppDownloadPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list the appDownloads.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->isAdmin() || $user->isStaff();
    }

    /**
     * Determine whether the user can view the appDownload.
     *
     * @param  \App\User  $user
     * @param  \App\AppDownload  $appDownload
     * @return mixed
     */
    public function view(User $user, AppDownload $appDownload)
    {
        //
    }

    /**
     * Determine whether the user can create appDownloads.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the appDownload.
     *
     * @param  \App\User  $user
     * @param  \App\AppDownload  $appDownload
     * @return mixed
     */
    public function update(User $user, AppDownload $appDownload)
    {
        //
    }

    /**
     * Determine whether the user can delete the appDownload.
     *
     * @param  \App\User  $user
     * @param  \App\AppDownload  $appDownload
     * @return mixed
     */
    public function delete(User $user, AppDownload $appDownload)
    {
        //
    }
}
