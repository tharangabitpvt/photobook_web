<?php

namespace App\Policies;

use App\User;
use App\PhotobookApp;
use Illuminate\Auth\Access\HandlesAuthorization;

class PhotobookAppPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the photobookApp.
     *
     * @param  \App\User  $user
     * @param  \App\PhotobookApp  $photobookApp
     * @return mixed
     */
    public function view(User $user, PhotobookApp $photobookApp)
    {
        //
    }

    /**
     * Determine whether the user can create photobookApps.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin() || $user->isStaff(); 
    }

    /**
    * Edit PhotoBook app
    *
    */
    public function edit(User $user, PhotobookApp $app)
    {
        return $user->isAdmin() || $user->isStaff(); 
    }

    /**
     * Determine whether the user can update the photobookApp.
     *
     * @param  \App\User  $user
     * @param  \App\PhotobookApp  $photobookApp
     * @return mixed
     */
    public function update(User $user, PhotobookApp $photobookApp)
    {
        return $user->isAdmin() || $user->isStaff(); 
    }

    /**
     * Determine whether the user can delete the photobookApp.
     *
     * @param  \App\User  $user
     * @param  \App\PhotobookApp  $photobookApp
     * @return mixed
     */
    public function delete(User $user, PhotobookApp $photobookApp)
    {
        //
    }

    /**
    * Determine whether the user can list the photobookApp.
    *
    * @param  \App\User  $user
    * @return mixed
    */
    public function index(User $user) {
        return $user->isAdmin() || $user->isStaff();
    }

    /**
    * Determine whether the use can pubish/unpublish apps
    *
    */
    public function publish(User $user)
    {
        return $user->isAdmin() || $user->isStaff();
    }
}
