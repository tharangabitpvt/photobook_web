<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $user->isAdmin() || $user->id == $model->id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->isAdmin();
    }

     /**
     * Determine whether the user can list the model.
     *
     * @param  \App\User  $user
     * @return mixed
     */
     public function index(User $user)
     {
        return $user->isAdmin();
     }

     /**
     * Determine whether the user can view the trashed model.
     */
     public function viewTrash(User $user)
     {
        return $user->isAdmin();
     }

     /**
     * restore users
     */
     public function restore(User $user)
     {
        return $user->isAdmin();
     }

     public function edit(User $user, User $model)
     {
        return $user->isAdmin() || $user->id == $model->id;
     }

     public function updateStaffUser(User $user, User $model)
     {
        return $user->isAdmin() || $user->id == $model->id;
     }
}
