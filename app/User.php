<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\AppDownload;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_APP_USER = 2;
    const USER_TYPE_PHOTOBOOK_STAFF = 3;

    const USER_ACTIVE = 1;
    const USER_INACTIVE = 0;

    const PHONE_VALIDATED_YES = 1;
    const PHONE_VALIDATED_NO = 0;

    const userType = [
        self::USER_TYPE_ADMIN => 'Super Admin',
        self::USER_TYPE_APP_USER => 'PhotoBook User',
        self::USER_TYPE_PHOTOBOOK_STAFF => 'Staff Member',
    ];

    const activeState = [
        self::USER_INACTIVE => 'Inactive',
        self::USER_ACTIVE => 'Active',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * relationships
    */
    public function messages() {
        return $this->hasMany('App\Message');
    }

    public function appDownloads() {
        return $this->hasMany('App\AppDownloads');
    }

    public function assignments() {
        return $this->belongsToMany('App\Job', 'job_assignments', 'user_id', 'job_id');
    }

    public function purchases() {
        return $this->hasMany('App\Purchase');
    }

    public function templates() {
        return $this->hasMany('App\Template');
    }
    // end relationships

    public function isAppuser() {
        return $this->user_type == self::USER_TYPE_APP_USER;
    }

    public function isAdmin() {
        return $this->user_type == self::USER_TYPE_ADMIN;
    }

    public function isStaff() {
        return $this->user_type == self::USER_TYPE_PHOTOBOOK_STAFF;
    }

    public function userPurchases() {
        return $this->purchases;
    }

    public function hasPurchased($itemId) {
        $purchased = \App\Purchase::where('item_id', $itemId)
                            ->where('user_id', $this->id)->get();
        return count($purchased);
    }

    public static function dialingCodesForDropdown() {
        $dialingCodesArray = config('pbconfig.country');
        
        $dropdownArray = [];
        foreach ($dialingCodesArray as $key => $country) {
            $dropdownArray[$country['dial_code']] = $country['dial_code'] . "  (" . $country['code'] . ")";
        }

        return $dropdownArray;
    }

    // get user country
    public static function getCountryFromDialCode($dialCode) {
        $dialingCodesArray = config('pbconfig.country');
        
        $userCountry = "";
        foreach ($dialingCodesArray as $key => $country) {
            if($country['dial_code'] === $dialCode) {
                $userCountry = $country['name'];
            }
        }

        return $userCountry;
    }

    // get user phone number
    public function getPhonenumber() {
        return $this->dial_code . $this->phone;
    }

    // users PbotoBook apps
    public function activatedApps() {
        $activatedApps = AppDownload::where('user_id', $this->id)
                                    ->where('is_activated', AppDownload::APP_ACTIVATED_TRUE)->get();
        return $activatedApps;
    }

    // get photobook app users
    public static function getAppUsers()
    {
        $appUsers = self::where('user_type', self::USER_TYPE_APP_USER)->where('is_active', self::USER_ACTIVE)->pluck('name', 'id')->toArray();

        return $appUsers;
    }
}
