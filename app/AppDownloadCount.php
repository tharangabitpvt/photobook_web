<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppDownloadCount extends Model
{
	public $timestamps = false;
	
    // relationships
    public function photobookApp() {
    	return $this->belongsTo('App\PhotobookApp', 'app_id');
    }
}
