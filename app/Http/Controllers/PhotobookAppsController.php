<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\PhotobookApp;

class PhotobookAppsController extends Controller
{
    public function getDownloads(Request $request) {
    	$photoBookAppsWindows = PhotobookApp::where('platform', 'LIKE', PhotobookApp::PLATFORM_WINDOWS)->where('published', 1)->get();
    	$photoBookAppsMac = PhotobookApp::where('platform', 'LIKE', PhotobookApp::PLATFORM_MAC)->where('published', 1)->get();

    	return view('downloads')
    			->with('photoBookAppsWin', $photoBookAppsWindows)
    			->with('photoBookAppsMac', $photoBookAppsMac)
                ->with('bodyClass', 'downloads-page');
    }

    /**
    * List Photobook apps
    */
    public function index(Request $request) {
    	$user = Auth::user();

    	if($user && $user->can('index', PhotobookApp::class)) {

    		$photobookApps = new PhotobookApp;

    		if($request->has('filter_release_name') && $request->filter_release_name != "") {
    			$photobookApps = $photobookApps->where('release_name', 'like', $request->filter_release_name .'%');
    		}

    		if($request->has('filter_version') && $request->filter_version != "") {
    			$photobookApps = $photobookApps->where('version', 'like', $request->filter_version);
    		}

    		if($request->has('filter_platform') && $request->filter_platform != "") {
    			$photobookApps = $photobookApps->where('platform', 'like', $request->filter_platform);
    		}

    		if($request->has('filter_architecture') && $request->filter_architecture != "") {
    			$photobookApps = $photobookApps->where('architecture', 'like', $request->filter_architecture);
    		}

            if($request->has('filter_file_type') && $request->filter_file_type != "") {
                $photobookApps = $photobookApps->where('file_type', 'like', $request->filter_file_type);
            }

    		if($request->has('filter_stable_level') && $request->filter_stable_level != "") {
    			$photobookApps = $photobookApps->where('stable_level', $request->filter_stable_level);
    		}

    		if($request->has('filter_release') && $request->filter_release != "") {
    			$photobookApps = $photobookApps->where('release', $request->filter_release);
    		}

    		if($request->has('filter_priority') && $request->filter_priority != "") {
    			$photobookApps = $photobookApps->where('priority', $request->filter_priority);
    		}

    		if($request->has('filter_published') && $request->filter_published != "") {
    			$photobookApps = $photobookApps->where('published', $request->filter_published);
    		}

    		$photobookApps = $photobookApps->paginate(20)->appends([
    			'filter_release_name' => $request->filter_release_name,
    			'filter_version' => $request->filter_version,
    			'filter_platform' => $request->filter_platform,
    			'filter_architecture' => $request->filter_architecture,
                'file_type' => $request->filter_file_type,
    			'filter_stable_level' => $request->filter_stable_level,
    			'filter_release' => $request->filter_release,
    			'filter_priority' => $request->filter_priority,
    			'filter_published' => $request->filter_published,
    		]);

    		return view('admin.photobook_apps')->with('photobookApps', $photobookApps);
    	} else {
    		abort(403, 'Unauthorized action.');
    	}
    }

    /**
    * Edit PhotoBook app
    *
    * @param integer $id
    * @return PhotoBookApp $app
    */
    public function edit($id)
    {
    	$app = PhotobookApp::find($id);
    	$user = Auth::user();
    	if($user && $user->can('edit', $app)) {
    		return view('admin.edit_app')->with('app', $app);
    	} else {
    		abort(403, 'Unauthorized action');
    	}
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'release_name' => 'required|string|max:100',
            'version' => 'required|string|regex:/(\d+\.)(\d+\.)(\d)/',
            'change_log' => 'required|string',
        ]);
    }

    /**
    * update model
    * 
    * @param Request form data
    */
    public function update(Request $request, $id)
    {
    	$updateApp = PhotoBookApp::find($id);
    	$user = Auth::user();
    	if($user && $user->can('update', $updateApp)){
    		$validator = $this->validator($request->all());
	        if ($validator->fails())
	        {
	           return redirect(route('admin.edit_app', ['id' => $id]))
	                        ->withErrors($validator)
	                        ->withInput();
	        }

	        $updateApp->release_name = $request->release_name;
	        $updateApp->version = $request->version;
	        $updateApp->platform = $request->platform;
	        $updateApp->architecture = $request->architecture;
            $updateApp->file_type = $request->file_type;
	        $updateApp->release = $request->release;
	        $updateApp->stable_level = $request->stable_level;
	        $updateApp->priority = $request->priority;
	        $updateApp->is_latest = $request->is_latest;
	        $updateApp->published = $request->published;
	        $updateApp->change_log = strip_tags($request->change_log);

	        if($updateApp->save()) {
	        	flash('App details updated successfully.')->success();
	        } else {
	        	flasg('Something went wrong!')->error();
	        }

	        return redirect(route('admin.photobook_apps'));

    	} else {
    		abort(403, 'Unauthorized action');
    	}
    }

    /**
    * create new photobook app. open create app HTML form
    *
    * @param 
    * @return
    */
    public function create(Request $request)
    {
    	$user = Auth::user();
    	if($user && $user->can('create', PhotobookApp::class)){
            if($request->isMethod('get')) {

                return view('admin.create_app');

            } else {
                $validator = $this->validator($request->all());
                if($validator->fails()){
                    return redirect(route('admin.create_photobook_app'))
                                    ->withErrors($validator)
                                    ->withInput();
                }

                if($this->store($request->all())) {
                    flash('App details added successfully.')->success();
                } else {
                    flasg('Something went wrong!')->error();
                }

                return redirect(route('admin.photobook_apps'));
            }
    		
    	} else {
    		abort(403, 'Unauthorized action');
    	}
    }

    /**
    * Store new photobook app details
    * 
    * @param Array form data
    * @return mixed
    */
    protected function store(Array $data)
    {
    	$newApp = new PhotobookApp;
    	$newApp->release_name = $data['release_name'];
        $newApp->version = $data['version'];
        $newApp->platform = $data['platform'];
        $newApp->architecture = $data['architecture'];
        $newApp->file_type = $data['file_type'];
        $newApp->release = $data['release'];
        $newApp->stable_level = $data['stable_level'];
        $newApp->priority = $data['priority'];
        $newApp->is_latest = $data['is_latest'];
        $newApp->create_date = date('Y-m-d H:i:s');
        $newApp->published = $data['published'];
        $newApp->change_log = strip_tags($data['change_log']);

        if($newApp->save()) {
        	return true;
        } else {
        	return false;
        }
    }

    /**
    * Publish/Unpublish app
    *
    * @param Array $ids
    *
    */
    public function publish(Request $request)
    {
    	$appIds = explode(",", $request->grid_item_ids);

    	$publish = PhotobookApp::PUBLISHED_NO;
    	$publishedText = 'un-published';
    	if($request->publish == 'publish') {
    		$publish = PhotobookApp::PUBLISHED_YES;
    		$publishedText = 'published';
    	}

    	$app = PhotobookApp::whereIn('id', $appIds)->update(['published' => $publish]);

		if($app) {
			flash('Selected apps ' . $publishedText)->success();
		} else {
			flash('Something went wrong!')->error();
		}

		return redirect(route('admin.photobook_apps'));
    }
}
