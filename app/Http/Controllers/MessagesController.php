<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\ContactusAdminMail;
use App\Mail\MessageReplyEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Message;
use URL;

class MessagesController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $v = Validator::make($data, [
            'f_name' => 'required|string|max:100',
            'email' => 'required|email',
            'message' => 'required|max:500',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $v->sometimes('l_name', 'string|max:100', function ($input) {
            return $input->l_name != "";
        });

        $v->sometimes('phone', 'regex:/[0-9]{9}/', function ($input) {
            return $input->phone != "";
        });

        return $v;
    }

    /**
    * send contact us message
    */
    public function contactus(Request $request) {
    	$redirectUrl = URL::previous() == url("/")."/" ?  URL::previous()."#contact-us" : URL::previous();

    	$validator = $this->validator($request->all());
    	if ($validator->fails())
        {
           return redirect($redirectUrl)
                        ->withErrors($validator)
                        ->withInput();
        } 

        $message = new Message;

        $isRegisteredUser = \App\User::where('email', 'LIKE', $request->email)->first();
        if($isRegisteredUser) {
        	$message->user_id = $isRegisteredUser->id;
        }
        
        $message->f_name = $request->f_name;
        $message->l_name = $request->l_name;
        $message->email = $request->email;
        $message->phone = $request->phone;
        $message->product = $request->contact_for;
        $message->message = strip_tags($request->message);
        $message->is_new = Message::NEW_MESSAGE_TRUE;
        $message->is_replied = Message::REPLIED_FALSE;
        $message->created_at = date("Y-m-d H:i:s");

        $message->save();

        Mail::to(config('pbconfig.salesEmail'))->send(new ContactusAdminMail($message));

        flash('Your message sent. PhotoBook will contact you as soon as possible.')->success();
    	return redirect($redirectUrl);
    }

    public function index(Request $request) {

        $user = Auth::user();

        if($user && $user->can('index', Message::class)) {

            $messages = new Message;

            if($request->has('filter_sender_name') && $request->filter_sender_name != "") {
                $senderNameSplit = explode(" ", $request->filter_sender_name, 2);
                $messages = $messages->where('f_name', 'like', '%' . $senderNameSplit[0] . '%');
                if(count($senderNameSplit) == 2) {
                    $messages = $messages->orWhere('l_name', 'like', '%' . $senderNameSplit[1] . '%');
                }
                                    
            }

            if($request->has('filter_sender_email') && $request->filter_sender_email != "") {
                $messages = $messages->where('email', 'like', $request->filter_sender_email . '%');
            }

            if($request->has('filter_sender_phone') && $request->filter_sender_phone != "") {
                $messages = $messages->where('phone', 'like', $request->filter_sender_phone);
            }

            if($request->has('filter_product') && $request->filter_product != "") {
                $messages = $messages->where('product', 'like', $request->filter_product);
            }

            $messages = $messages->orderBy('id', 'desc')->paginate(20)->appends([
                'filter_sender_name' => $request->filter_sender_name,
                'filter_sender_email' => $request->filter_sender_email,
                'filter_sender_phone' => $request->filter_sender_phone,
                'filter_product' => $request->filter_product,
            ]);
            $unreadMessagesCount = Message::where('is_new', Message::NEW_MESSAGE_TRUE)->count();
           
            return view('admin.messages')->with([
                'messages' => $messages,
                'unreadMessagesCount' => $unreadMessagesCount,
            ]);

        } else {

            flash('Unauthorized action!')->warning();
            return redirect('/');
            
        }
        
    }

    public function view($id) {
        $user = Auth::user();
        $message = Message::find($id);
        if($user && $user->can('view', $message)) {
            if($message->is_new == Message::NEW_MESSAGE_TRUE) {
                $message->is_new = Message::NEW_MESSAGE_FALSE;
                $message->save();
            }            

            return view('admin.message')->with([
                'message' => $message,
            ]);
        } else {
            flash('Unauthorized action!')->warning();
            return redirect('/');
        }
    }

    /**
    * Reply to contact us message
    * @param integer $id
    */
    public function reply(Request $request, $id) {
        $user = Auth::user();
        $message = Message::find($id);

        $replyText = $request->message_reply;

        $message->reply = $replyText;
        $message->is_replied = Message::REPLIED_TRUE;
        $message->save();

        Mail::to($message->email)->send(new MessageReplyEmail($message));

        flash('Reply sent successfully.')->success();
        return redirect(route('admin.messages'));
    }

    /**
    * delete messages
    * @param Array $msgIds
    */
    public function delete(Request $request) {
        $user = Auth::user();
        if($user && $user->can('delete', Message::class)) {
            $msgIds = $request->grid_item_ids;
            $msgIdAdday = explode(",", $msgIds);

            Message::destroy($msgIdAdday);

            flash('Selected messages deleted successfully.')->success();
            return redirect(route('admin.messages'));
        } else {
            flash('Unauthorized action!')->warning();
            return redirect('/');
        }
        
    }
}
