<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Template;
use App\PhotobookApp;
use App\AppDownloadCount;
use App\AppDownload;
use Charts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $latestTemplates = Template::where('published', Template::PUBLISH_TRUE)->orWhere('published', Template::COMING_SOON)->orderBy('id', 'desc')->limit(3)->get();
        $albumSizes = config('pbconfig.albumSizes');

        return view('home')
        ->with('latestTemplates', $latestTemplates)
        ->with('albumSizes',  $albumSizes);
    }

    public function admin() {
        if(Auth::check()) {
            $user = Auth::user();
            if($user->isAdmin() || $user->isStaff()) {
                $username = $user->name;
                $email = $user->email;

                // number of app downloads
                $today = date("Y-m-d");

                $downloadDates= [];
                $appDownloadCounts = [];
                for($a = 7; $a >= 1; $a--) {
                    $downloadDate = date("Y-m-d", strtotime('-' . $a . ' days'));
                    $photoBookAppDowloads = AppDownloadCount::where('download_date', 'like', $downloadDate .'%')->first();
                //dd($photoBookAppDowloads);
                    $downloadDates[] = $downloadDate;
                    if(count($photoBookAppDowloads) > 0) {                      
                        $appDownloadCounts[] = $photoBookAppDowloads->download_count;
                    } else {
                        $appDownloadCounts[] = 0;
                    }
                    
                }

                $downloadsChart = Charts::create('line', 'highcharts')
                    ->title('App downloads')
                    ->elementLabel('downloads')
                    ->labels($downloadDates)
                    ->values($appDownloadCounts)
                    ->dimensions(0, 300)
                    ->responsive(false);


                $totalDownloads = AppDownloadCount::sum('download_count');
                $activatedCount = AppDownload::where('is_activated', AppDownload::APP_ACTIVATED_TRUE)->count();

                $activateChart = Charts::create('donut', 'highcharts')
                    ->title('Trial vs Activated')
                    ->labels(['Trial', 'Activated'])
                    ->colors(['#90A4AE', '#008000'])
                    ->values([($totalDownloads - $activatedCount), $activatedCount])
                    ->dimensions(0, 300)
                    ->responsive(false);

                return view('admin.home')->with([
                    'username' => $username,
                    'email' => $email,
                    'downloadsChart' => $downloadsChart,
                    'appActivatedChart' => $activateChart,
                ]);
            } else {
                flash('Invalid request!', 'warning');
                return redirect('/');
            }
        } else {
            flash('Please login to your admin account.');
            return redirect('login');
        }
    }
}
