<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Template;
use App\PhotobookApp;
use App\AppDownload;

class HelperController extends Controller
{
    public static function currencyConvert($value, $fromCurrency='LKR', $toCurrency='USD') {
    	$exchangeRates = Storage::get('ex_rates.json');
        $jsonObj = json_decode($exchangeRates);

        $currencyRates = $jsonObj->rates;
        $fromCurrencyInUSD = $currencyRates->$fromCurrency;
        $toCurrencyInUSD = $currencyRates->$toCurrency;

        $newRate =  $toCurrencyInUSD/$fromCurrencyInUSD;
        $newValue = round($value * $newRate, 2);
        
        return $newValue . " " . $toCurrency;
    }

    /**
    * Update currency rates JSON file.
    * @return void
    */
    public static function updateCurrencyRates() {
        // update exchange rates
        
        $app_id = '4258c4cea5f14767ab73247d33c06f2b';
        $oxr_url = "https://openexchangerates.org/api/latest.json?app_id=" . $app_id;

        // Open CURL session:
        $ch = curl_init($oxr_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Get the data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $oxr_latest = json_decode($json);

        if($json) {
            Storage::disk('local')->put('ex_rates.json', $json);
        }
    }

    // get all templates for shop in app
    public function getShopItems(Request $request) {
        $appSerial = $request->app_serial;
        $appDownload = AppDownload::where('app_serial', 'LIKE', $appSerial)->first();

        // newly bought templates
        $newTemplates = $appDownload->newTemplates()->get();

        $newTemplatesData = [];
        foreach($newTemplates as $newTemplate) {
            $templatePurchase = \App\Purchase::find($newTemplate->pivot->purchase_id);

            $newTemplatesData[] = [
                    'id' => $newTemplate->id,
                    'name' => $newTemplate->tem_name,
                    'payment_status' => $templatePurchase->payment_status];
        }
        
        $currentAppVersion = $request->app_version;
        $appUpdates = PhotobookApp::where('version', 'LIKE', $currentAppVersion)
                                ->where('release', PhotobookApp::RELEASE_UPDATE)->get();

        return json_encode([
            'newTemplates' => json_encode($newTemplatesData),
            'updates' => $appUpdates,
        ]);
    }

    // buy template button blade directive
    public static function buyTemplateButton($templateId) {
        $activeApps = [];
        $activeAppTemplates = [];
        if(Auth::check()) {
            $user = Auth::user();
            $activeApps = $user->activatedApps();

            foreach($activeApps as $activeApp) {
                foreach($activeApp->templates as $userTemplate) {
                    if($userTemplate->id == $templateId) {
                        $activeAppTemplates[$activeApp->app_serial][] = $templateId;
                    }
                }
            }
        }

        return view('partials/buttons/buy_template_button')
                ->with([
                    'templateId' => $templateId,
                    'activeApps' => $activeApps,
                    'activeAppTemplates' => $activeAppTemplates,
                ]);
    }

    // activate app button
    public static function activateAppButton()
    {
        $userId = Auth::id();
        $appDownloads = AppDownload::where('user_id', $userId)->get();

        return view('partials/buttons/app_activate_button')
                ->with('appDownloads', $appDownloads);
    }

    // fix photobook apps
    public function fixMe(Request $request) {
        $appSerial = $request->app_serial;
        $templateIds = explode(",", $request->templates);

        $appDownload = AppDownload::where('app_serial', $appSerial)->first();

        if($appDownload) {
            $allTemplates = $appDownload->templates()->get();
            $templatesNotIn = [];
            foreach($allTemplates as $template) {
                if(in_array($template->id, $templateIds)){
                    // change template install status and ...
                    if($template->is_installed == 0) {
                        $appId = AppDownload::where('app_serial', $appSerial)->value('id');
                        DB::table('apps_templats')
                        ->where('app_id', $appId)
                        ->where('template_id', $template->id)
                        ->update([
                            'is_installed' => 1,
                            'install_time' => date("Y-m-d H:i:s"),
                        ]);
                    }                    
                } else {
                    $templatesNotIn[] = $template->id;
                }
            }
            /*$installedTemplates = $appDownload->installedTemplates()->get();
            //dd($installedTemplates);
            $installedTemplateIds = [];

            foreach($installedTemplates as $installedTemplate) {
                $installedTemplateIds[] = $installedTemplate->id;
            }*/

            return json_encode([
                'template_ids' => implode(",", $templatesNotIn),
            ]);
        } else {
            return json_encode([
                'template_ids' => "",
            ]);
        }
        
    }
}
