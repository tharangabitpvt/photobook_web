<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $v = Validator::make($data, [
            'name' => 'required|string|max:255',
            'phone' => 'required|regex:/[0-9]{9}/',
            'billing_address' => 'required',
            'current_password' => 'required_with:password',
        ]);

        $v->sometimes('password', 'string|min:6|confirmed', function ($input) {
            return $input->current_password != "";
        });

        return $v;
    }

    protected function validatorCreateUser(array $data)
    {
       return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|regex:/[0-9]{9}/|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function validatorUpdateUser(array $data, $user)
    {
        $v = Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
            'phone' => [
                'required', 'regex:/[0-9]{9}/',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);

        $v->sometimes('password', 'string|min:6|confirmed', function ($input) {
            return $input->password != "";
        });

        return $v;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        if($user->can('index', User::class)) {
            $users = new User;

            if($request->has('filter_name') && $request->filter_name != "") {
                $users = $users->where('name', 'like', '%' . $request->filter_name . '%');
            }

            if($request->has('filter_email') && $request->filter_email != "") {
                $users = $users->where('email', 'like', $request->filter_email . '%');
            }

            if($request->has('filter_phone') && $request->filter_phone != "") {
                $users = $users->where('phone', 'like', '%' . $request->filter_phone . '%');
            }

            if($request->has('filter_user_type') && $request->filter_user_type != "") {
                $users = $users->where('user_type', $request->filter_user_type);
            }

            if($request->has('filter_business_name') && $request->filter_business_name != "") {
                $users = $users->where('business_name', 'like', '%' . $request->filter_business_name . '%');
            }

            $users = $users->paginate(20)->appends([
                'filter_name' => $request->filter_name,
                'filter_email' => $request->filter_email,
                'filter_phone' => $request->filter_phone,
                'filter_user_type' => $request->filter_user_type,
                'business_name' => $request->business_name,
            ]);

            return view('admin.users')->with([
                'users' => $users,
            ]);
        } else {
            abort('403', 'Unauthorized action');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::user();
        if($user->can('create', User::class)){
            if($request->isMethod('get')){

                return view('admin.create_user');

            } else if($request->isMethod('post')){

                $validator = $this->validatorCreateUser($request->all());
                if ($validator->fails())
                {
                   return redirect(route('admin.create_user'))
                                ->withErrors($validator)
                                ->withInput();
                }

                if($this->store($request->all())){
                    flash('User created successfully.')->success();
                } else {
                    flash('User creation failed!')->error();
                }
            } else {
                abort('403', 'Unauthorized action');
            }

            return redirect(route('admin.user_manager'));
        } else {
            abort('403', 'Unauthorized action');
        }        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Array $data
     * @return boolean
     */
    protected function store(Array $data)
    {
        $newUser = new User;
        $newUser->name = $data['name'];
        $newUser->email = $data['email'];
        $newUser->dial_code = $data['dial_code'];
        $newUser->phone = $data['phone'];
        $newUser->country = User::getCountryFromDialCode($data['dial_code']);
        $newUser->user_type = $data['user_type'];
        $newUser->password = bcrypt($data['password']);
        $newUser->created_at = date("Y-m-d H:i:s");
        $newUser->is_active = $data['is_active'];

        if($newUser->save()) {
            return true;
        } else {
           return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, $userId)
    {
        $updateUser = User::find($userId);
        $user = Auth::user();
        if($user && $user->can('edit', $updateUser)){
            return view('admin.edit_user')->with('user', $updateUser);
        } else {
            abort('403', 'Unauthorized action');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId)
    {
        if(Auth::check()) {
            $updateUser = User::find($userId);

            $validator = $this->validator($request->all());

            if($request->password) {
                if(Auth::attempt(['email' => $updateUser->email, 'password' => $request->current_password])) {
                    $updateUser->password = bcrypt($request->password);
                } else {
                    $validator->errors()->add('current_password', 'Current password is wrong.');
                    return redirect('/dashboard')
                            ->withErrors($validator)
                            ->withInput();
                }                
            }

            if ($validator->fails())
            {
               return redirect('/dashboard')
                            ->withErrors($validator)
                            ->withInput();
            }            
            
            $updateUser->name = $request->name;
            $updateUser->dial_code = $request->dial_code;
            $updateUser->phone = $request->phone;
            $updateUser->country = User::getCountryFromDialCode($request->dial_code);
            $updateUser->business_name = $request->business_name;
            $updateUser->business_address = $request->business_address;
            $updateUser->billing_address = $request->billing_address;

            $updateUser->updated_at = date("Y-m-d H:i:s");

            $updateUser->save();

            flash('Your profile updated successfully.')->success();
            return redirect('/dashboard');

        } else {
            flash('Please login into your PhotoBook account.');
            return redirect('/login');
        }
        

    }

    /**
    * update photobook staff user
    * @param \App\User $user
    */
    public function updateStaffUser(Request $request, $userId)
    {
        $user = User::find($userId);
        $userAuth = Auth::user();
        if($userAuth && $userAuth->can('updateStaffUser', $user)){
            $validator = $this->validatorUpdateUser($request->all(), $user);
            if ($validator->fails())
            {
               return redirect(route('admin.edit_user', ['id' => $userId]))
                            ->withErrors($validator)
                            ->withInput();
            }
            
            $user->name = $request->name;
            $user->dial_code = $request->dial_code;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->country = User::getCountryFromDialCode($request->dial_code);
            $user->updated_at = date("Y-m-d H:i:s");
            $user->is_active = $request->is_active;

            if($user->save()) {
                flash('User updated successfully.')->success();
            } else {
                flash('Update process failed!')->error();
            }

            return redirect(route('admin.user_manager'));
        } else {
            abort('403', 'Unauthorized action');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
    * Soft delete users
    */
    public function delete(Request $request) {
        $user = Auth::user();
        if($user->can('delete', User::class)) {
            $userIds = explode(",", $request->grid_item_ids);
            if(User::destroy($userIds)) {
                flash('Selected user(s) moved to trash.')->success();
            } else {
                flash('Something wend wrong!')->error();
            }

            return redirect(route('admin.user_manager'));
        } else {
            abort('403', 'Unauthorized action');
        }
        
    }

    /**
    * restore trashed users
    */
    public function restore(Request $request) {
        $user = Auth::user();
        if($user->can('restore', User::class)) {
            $userIds = explode(",", $request->grid_item_ids);
            $users = User::whereIn('id', $userIds);
            
            if($users->restore()) {
                flash('User(s) restored successfully.')->success();
            } else {
                flash('Something went wrong!.')->error();
            }
            
            return redirect(route('admin.user_manager.trashed'));
        } else {
            abort('403', 'Unauthorized action');
        }
    }

    /**
    * view trashed users
    */
    public function viewTrash(Request $request){
        $user = Auth::user();
        if($user->can('viewTrash', User::class)) {
            $trashedUsers = new User;

            if($request->has('filter_name') && $request->filter_name != "") {
                $trashedUsers = $trashedUsers->where('name', 'like', '%' . $request->filter_name . '%');
            }

            if($request->has('filter_email') && $request->filter_email != "") {
                $trashedUsers = $trashedUsers->where('email', 'like', $request->filter_email . '%');
            }

            if($request->has('filter_phone') && $request->filter_phone != "") {
                $trashedUsers = $trashedUsers->where('phone', 'like', '%' . $request->filter_phone . '%');
            }

            if($request->has('filter_user_type') && $request->filter_user_type != "") {
                $trashedUsers = $trashedUsers->where('user_type', $request->filter_user_type);
            }

            if($request->has('filter_business_name') && $request->filter_business_name != "") {
                $trashedUsers = $trashedUsers->where('business_name', 'like', '%' . $request->filter_business_name . '%');
            }

            $trashedUsers = $trashedUsers->onlyTrashed()->paginate(20)->appends([
                'filter_name' => $request->filter_name,
                'filter_email' => $request->filter_email,
                'filter_phone' => $request->filter_phone,
                'filter_user_type' => $request->filter_user_type,
                'business_name' => $request->business_name,
            ]);

            return view('admin.user_trash')->with('usersTrashed', $trashedUsers);
        } else {
            abort('403', 'Unauthorized action');
        }
    }

    /**
    * Activate user account by confirm email url
    */
    public function activate($accessToken, $userId) {
        $user = User::where('access_token', $accessToken)
                    ->where('id', $userId)->first();
        if($user) {
            $user->is_active = User::USER_ACTIVE;
            $user->save();

            flash('Activation process successfull! Now you can login to your account.')->important();
            return redirect('/login');
        } else {
            abort('400', 'Bad request');
        }
        
    }
}
