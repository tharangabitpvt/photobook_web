<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Template;
use App\AppDownload;
use App\Purchase;
use Zipper;

class AppUpdatesController extends Controller
{
    public function authUpdate(Request $request)
    {
        $appSerial = $request->app_serial;
        $itemIds = $request->itemIds;
        $updateType = $request->updateType;

        if($updateType == 'template') {
            $passedIds = [];
            $appId = AppDownload::where('app_serial', $appSerial)->value('id');
            if($appId){

                $purchases = DB::table('apps_templats')
                            ->where('app_id', $appId)
                            ->whereIn('template_id', explode(",", $itemIds))
                            ->get();
                //var_dump($purchases); exit;
                foreach($purchases as $purchase) {
                    $purchaseId = $purchase->purchase_id;
                    $itemId = $purchase->template_id;
                    $isPaid = false;
                    $isPaid = Purchase::find($purchaseId)->value('payment_status') == Purchase::PAYMENT_STATUS_PAID;

                    if($isPaid) {
                        $passedIds[] = $itemId;
                    }
                }
            }
            
            if(empty($passedIds)) {
                return json_encode([
                    'authResp' => false,
                ]);
            } else {
                return json_encode([
                    'authResp' => implode(",", $passedIds),
                ]);
            }
            
        } else {
            return json_encode([
                'authResp' => false,
            ]);
        }
    }

    public function update(Request $request)
    {
    	$appSerial = $request->app_serial;
    	$itemIdsArray = explode(",", $request->itemIds);
    	$updateType = $request->updateType;

    	if($updateType == 'template'){
    		$template = Template::find($itemIdsArray)->first();
            //var_dump($template->storage_drive); exit;
    		if($template->storage_drive == 'local') {
                $templateId = $template->id;
                $templateFile = storage_path() . '/app/templates/' . $templateId . '.zip';
                //$installFile = $appSerial . time() . '_install.zip';
                //$installTempDir = $appSerial . time() . '_install';

               //\File::copyDirectory(storage_path() . '/app/templates/' . $templateId, storage_path() . '/app/tmp/' . $installTempDir . '/' . $templateId);

                //$tempFolders = glob(storage_path() . '/app/tmp/' . $installTempDir . '/');

                //Zipper::make(storage_path() . '/app/tmp/' . $installFile)->add($tempFolders)->close();

                return response()->download($templateFile, $templateId . '.zip', ['application/octet-stream']);
            } else if($template->storage_drive == 'photobookdrive') {
                $googleDrive = Storage::disk('photobookdrive');
                $filename = $template->id . ".zip";
                $dir = "templates";

                $file = self::fileFromSubDirGoogleDrive($googleDrive, $dir, $filename);

                if(!$file) {
                    return json_encode([
                        'downloadStatus' => 'file not found', 
                    ]);
                } else {
                    //var_dump($file); exit;
                    /*$rawData = $googleDrive->get($file['path']);
                    return response($rawData, 200)
                        ->header('ContentType', $file['mimetype'])
                        ->header('Content-Disposition', "attachment; filename='$filename'");*/

                     $readStream = $googleDrive->getDriver()->readStream($file['path']);
                     return response()->stream(function () use ($readStream) {
                        fpassthru($readStream);
                    }, 200, [
                        'Content-Type' => $file['mimetype'],
                        'Content-Length' => $file['size'],
                        //'Content-disposition' => 'attachment; filename="'.$filename.'"', // force download?
                    ]);
                }
            } 

    	}	

    }

    private function fileFromSubDirGoogleDrive($googleDrive, $subDirName, $filename, $recursive=false) {
        $contents = collect($googleDrive->listContents('/', false));

        $resourceDir = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', $subDirName)
                        ->first(); // There could be duplicate directory names!

        $file = collect($googleDrive->listContents($resourceDir['path'], false))
                ->where('type', '=', 'file')
                ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
                ->first();

        return $file;
    }

    private function fileFromGoogleDrive($googleDrive, $dir, $filename, $recursive=false) {
        //$recursive = false; // Get subdirectories also?
        $contents = collect($googleDrive->listContents($dir, $recursive));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!
        //return $file; // array with file info

        return $file;
    }

    public function complete(Request $request)
    {
    	$appSerial = $request->app_serial;
    	$itemIdsArray = explode(",", $request->itemIds);
    	$updateType = $request->updateType;
    	$installDir = storage_path() . '/app/tmp/' . $request->installDir;

    	if($updateType == 'template') {
    		try {
    			$appId = AppDownload::where('app_serial', $appSerial)->value('id');
	    		DB::table('apps_templats')
    			->where('app_id', $appId)
    			->whereIn('template_id', $itemIdsArray)
    			->update([
    				'is_installed' => 1,
    				'install_time' => date("Y-m-d H:i:s"),
    			]);

	    		/*if(file_exists($installDir . '.zip')){
	    			unlink($installDir . '.zip');	    			
	    		}
	    		if(is_dir($installDir) && $installDir != storage_path() . '/app/tmp/' && $installDir != storage_path() . '/app/tmp'){
	    			\File::deleteDirectory($installDir);
	    		}*/		

	    		return json_encode([
	    			'resp' => 'done',
	    		]);
    		} catch(\Exception $e) {
    			return json_encode([
	    			'resp' => 'failed',
	    			'errorMessage' => $e->getMessage(),
	    		]);
    		}
    		
    	}
    	
    }
}
