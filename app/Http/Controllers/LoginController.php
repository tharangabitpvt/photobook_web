<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
     /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => User::USER_ACTIVE])) {
            // Authentication passed...
            if(Auth::user()->isAdmin() || Auth::user()->isStaff()) {
                return redirect('/admin');
            } else {
                return redirect()->intended('/');
            }
            
        } else {
            flash('Login failed.')->error();
            return redirect('/login');
        }
    }
}
