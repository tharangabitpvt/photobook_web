<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Charts;
use App\User;
use App\Purchase;
use App\Notification;
use App\AppDownload;

class NotificationsController extends Controller
{
    public function dashboard() {
    	if(Auth::check() && Auth::user()->isAppuser()) {
    		$authUser = Auth::user();
    		$purchases = Purchase::where('user_id', $authUser->id)->simplePaginate(15, ['*'], 'purchases-page');

    		$unpaidCount = Notification::getPayStatusNotifications(Purchase::PAYMENT_STATUS_NOT_PAID);
    		$paidCount = Notification::getPayStatusNotifications(Purchase::PAYMENT_STATUS_PAID);
            $pendingCount = Notification::getPayStatusNotifications(Purchase::PAYMENT_STATUS_PENDING);
            $canceledCount = Notification::getPayStatusNotifications(Purchase::PAYMENT_STATUS_CANCELED);
            $rejectedCount = Notification::getPayStatusNotifications(Purchase::PAYMENT_STATUS_REJECTED);

    		$chart = Charts::create('donut', 'highcharts')
				    ->title('Your purchases')
				    ->labels(['Paid', 'Unpaid', 'Pending', 'Canceled', 'Rejected'])
				    ->values([$paidCount, $unpaidCount, $pendingCount, $canceledCount, $rejectedCount])
				    ->dimensions(0, 300)
				    ->responsive(false);

            // users app keys
            $appDownloads = AppDownload::where('user_id', $authUser->id)->simplePaginate(15, ['*'], 'appdownloads-page');

    		return view('dashboard')
    				->with([
    					'purchases' => $purchases,
    					'unpaidCount' => $unpaidCount,
    					'paidCount' => $paidCount,
    					'purchaseChart' => $chart,
    					'appUser' => $authUser,
                        'appDownloads' => $appDownloads,
    				]);
    	} else {
    		flash('Please login into your PhotoBook account.');
    		return redirect('/login');
    	}
    	
    }
}
