<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\PurchaseOrderMail;
use App\Mail\AppActivationMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Template;
use App\Purchase;
use App\AppDownload;
use App\User;
use Session;
use PDF;

class PurchasesController extends Controller
{
	/*public function __construct()
    {
        $this->middleware('auth');
    }*/

	/**
	* Index purchases
	* 
	*/
	public function index(Request $request)
	{
		$user = Auth::user();
		if($user && $user->can('index', Purchase::class)){
			$purchases = new Purchase;

			if($request->has('filter_user') && $request->filter_user != '') {
				$purchases = $purchases->whereHas('user', function($q) use($request){
					$q->where('name', 'like', '%' . $request->filter_user . '%');
				});
			}

		 	if($request->has('filter_purchase_type') && $request->filter_purchase_type != "") {
                $purchases = $purchases->where('purchase_type', $request->filter_purchase_type);
            }

            if($request->has('filter_payment_method') && $request->filter_payment_method != "") {
                $purchases = $purchases->where('payment_method', $request->filter_payment_method);
            }

            if($request->has('filter_ref_no') && $request->filter_ref_no != "") {
                $purchases = $purchases->where('payment_method', $request->ref_no);
            }

            if($request->has('filter_payment_status') && $request->filter_payment_status != "") {
                $purchases = $purchases->where('payment_status', $request->filter_payment_status);
            }

			$purchases = $purchases->orderBy('id', 'desc')->paginate(20);
			return view('admin.purchases')->with('purchases', $purchases);
		} else {
			abort(403, 'Unauthorized action.');
		}
	}

	/**
	* set payment status of purchase recrds
	* 
	* @param Request $request
	* @return Http response
	*/
	public function setPaymentStatus(Request $request)
	{
		$user = Auth::user();
		if($user && $user->can('setPaymentStatus', Purchase::class)) {
			$invoiceNo = $request->invoice_no;
			$newPaymentStatus = $request->payment_status;

			$purchaseItems = Purchase::where('ref_no', 'like', $invoiceNo)->get();

			$purchaseUser = User::find($purchaseItems[0]->user_id);

			$collectionItems = [];
			foreach($purchaseItems as $purchaseItem) {						
				$tempArray = [
					'item_type' => $purchaseItem->purchase_type, 
					"item_id" => $purchaseItem->item_id
				];

				if($purchaseItem->purchase_type == Purchase::PURCHASE_TYPE_APPKEY && $newPaymentStatus == Purchase::PAYMENT_STATUS_PAID) {
		    		$appKey = str_random(20);
		    		$randForSerial = rand(100, 999);
		    		$increment = AppDownload::max('id');
		    		$appSerial = 'pb' . date('y') . '-' . $randForSerial . '-' . ($increment + 1);

		    		$appDownload = new AppDownload;
		    		$appDownload->app_serial = $appSerial;
		    		$appDownload->user_id = $purchaseItem->user_id;
		    		$appDownload->is_activated = AppDownload::APP_ACTIVATED_FALSE;
		    		$appDownload->app_key = $appKey;

		    		$appDownload->save();

		    		// send app key to the client
		    		Mail::to($purchaseUser->email)->queue(new AppActivationMail($appKey, $purchaseUser->name));
		    	}

		    	if($purchaseItem->purchase_type == Purchase::PURCHASE_TYPE_TEMPLATE) {
		    		$appsTemplates = DB::table('apps_templats')
		    							->select('app_id')
		    							->where('purchase_id', $purchaseItem->id)
		    							->first();
		    		$pbAppSerial = AppDownload::find($appsTemplates->app_id)->value('app_serial');

		    		$tempArray["app_serial"] = $pbAppSerial;
				}

				$collectionItems[] = $tempArray;

		    	$purchaseItem->payment_status = $newPaymentStatus;
		    	$purchaseItem->paid_date = date('Y-m-d h:i:s');

				$purchaseItem->save();
			}

			// send paid onvoice
			$invoiceDate = date("d, F Y h:i a");
			$collectionInfo = $this->calculateCollection($collectionItems);

			$pdf = PDF::loadView('pdf.invoice', [
		    			'shoppingItems' => $collectionInfo['purchaseItemsInfo'],
		    			'total' 		=> $collectionInfo['total'],
		    			'currency'		=> 'LKR',
		    			'vat'			=> $collectionInfo['vat'],
		    			'paystatus' 	=> $newPaymentStatus,
		    			'invoiceNo' 	=> $invoiceNo,
		    			'userName'		=> $purchaseUser->name,
		    			'billingAddress' => $purchaseUser->billing_address ? $purchaseUser->billing_address : 'Billing address not provided',
		    			'country'		=> $purchaseUser->country,
		    			'invoiceDate'	=> $invoiceDate]);

	    	$invoiceTempPath = storage_path('app/tmp/' . $invoiceNo . '.pdf');
			$pdf->save($invoiceTempPath);

			Mail::to($purchaseUser->email)->queue(new PurchaseOrderMail($invoiceNo));

			flash('Invoice sent')->success();
			return redirect(route('admin.purchases'));

		} else {
			abort(403, 'Unauthorized action.');
		}
	}

	/**
	* Show invoice for admin
	* 
	* @param String invoice no
	*/
	public function invoice($invoiceNo)
	{
		$user = Auth::user();
		if($user && $user->can('invoice', Purchase::class)) {

			$purchaseItems = Purchase::where('ref_no', 'like', $invoiceNo)->get();
			
			$invoiceUser = User::find($purchaseItems[0]->user_id);

			$collectionItems = [];
			$paymentStatus = Purchase::PAYMENT_STATUS_NOT_PAID;
			foreach($purchaseItems as $purchaseItem) {						
				$tempArray = [
					'item_type' => $purchaseItem->purchase_type, 
					"item_id" => $purchaseItem->item_id
				];

				$paymentStatus = $purchaseItem->payment_status;

		    	if($purchaseItem->purchase_type == Purchase::PURCHASE_TYPE_TEMPLATE) {
		    		$appsTemplates = DB::table('apps_templats')
		    							->where('purchase_id', $purchaseItem->id)
		    							->value('app_id');
		    		//dd($appsTemplates);
		    		$pbApp = AppDownload::find($appsTemplates)->value('app_serial');
		    		
		    		$tempArray["app_serial"] = $pbApp;
				}
				//dd($tempArray);
				$collectionItems[] = $tempArray;
			}
			
			$collectionInfo = $this->calculateCollection($collectionItems);
			
			$invoiceDate = date("d, F Y h:i a");

			return view('pdf.invoice')->with([
				'shoppingItems' => $collectionInfo['purchaseItemsInfo'],
    			'total' 		=> $collectionInfo['total'],
    			'currency'		=> 'LKR',
    			'vat'			=> $collectionInfo['vat'],
    			'paystatus' 	=> $paymentStatus,
    			'invoiceNo' 	=> $invoiceNo,
    			'userName'		=> $invoiceUser->name,
    			'billingAddress' => $invoiceUser->billing_address ? $invoiceUser->billing_address : 'Billing address not provided',
    			'country'		=> $invoiceUser->country,
    			'invoiceDate'	=> $invoiceDate,
    			'html'			=> true,
			]);

		} else {
			abort(403, 'Unauthorized action.');
		}
	}

    public function addToCollection(Request $request) {
    	$user = Auth::user();
    	if($user && $user->can('addToCollection', Purchase::class)) {
    		if ($request->isMethod('post')) {
			    $collectionItems = $request->collection;
			    Session::put('buy_collection', $collectionItems);

			    if(Session::has('buy_collection')) {
			    	return Session::get('buy_collection');
			    } else {
			    	return 'false';
			    }
			    
			} else {
				return 'invalid request';
			}
    	} else {
    		abort(403, 'Unauthorized action.');
    	}
    }

    // show shoping collection
    public function showCollection(Request $request) {
    	if ($request->isMethod('post')) {
		    $collectionItems = $request->collection;
		    
		    $collectionCal = $this->calculateCollection($collectionItems);

		    $purchaseItemsInfo = $collectionCal['purchaseItemsInfo'];
		    $total = $collectionCal['total'];

		    return view('partials.shoping_list')
		    			->with('shopingItems', $purchaseItemsInfo)
		    			->with('total', $total);

		} else {
			return 'invalid request';
		}
    }

    /**
    * Canculate collection
    */
    private function calculateCollection($collectionItems) {
    	$purchaseItemsInfo = [];
	    $total = 0;
	    foreach($collectionItems as $collectionItem) {
	    	$itemType = $collectionItem["item_type"];
	    	$itemId = $collectionItem["item_id"];

	    	if($itemType == Purchase::PURCHASE_TYPE_TEMPLATE) {
	    		$appSerial = $collectionItem["app_serial"];

	    		$itemModel = Template::find($itemId);
	    		$purchaseItemsInfo[] = ["itemId"	=> $itemModel->id,
	    								"itemType"	=> $itemType,
	    								"name" 		=> $itemModel->tem_name, 
	    								"price" 	=> $itemModel->price - ($itemModel->price * $itemModel->discount/100), 
	    								"currency" 	=> $itemModel->currency,
	    								"discount"	=> $itemModel->discount,
	    								"itemTypeName"	=> Purchase::itemTypeName[$itemType],
	    								"app_serial" => $appSerial];

	    		$total = $total + ($itemModel->price - ($itemModel->price * $itemModel->discount/100));

	    	} else if($itemType == Purchase::PURCHASE_TYPE_APPKEY) {
	    		$purchaseItemsInfo[] = ["itemId"	=> 0,
	    								"itemType"	=> $itemType,
	    								"name" 		=> Purchase::itemTypeName[$itemType], 
	    								"price" 	=> config('pbconfig.photobookAppKeyPrice'), 
	    								"currency" 	=> 'LKR',
	    								"discount"	=> 0,
	    								"itemTypeName"	=> null,
	    							];

	    		$total = $total + config('pbconfig.photobookAppKeyPrice');
	    	}
	    }

	    // vat
	    $vat = config('pbconfig.vat');
	    $tax = $total * $vat/100;
		$total = $total + $tax;

	    return [
	    	'purchaseItemsInfo' => $purchaseItemsInfo,
	    	'vat' => $vat,
	    	'total' => $total,
	    ];
    }

    // checkout
    public function checkout(Request $request) {
    	if (Auth::check()) {
    		$collection = [];
    		if(Session::has('buy_collection')) {
		    	$collection = Session::get('buy_collection');

		    	$calcCollection = $this->calculateCollection($collection);

		    	return view('checkout')->with([
		    		'shoppingItems' => $calcCollection['purchaseItemsInfo'], 
		    		'total' => $calcCollection['total']
		    	]);
		    } else {
		    	flash('Your collection is empty');
		    	return redirect('/');
		    }

		} else {
			flash('Please login into your account. If you do not have a PhotoBook account yet, please <a href="/register">create your account</a> now.');
			return redirect('login');
		}
    	
    }

    // complete purchase
    public function completePurchase(Request $request) {
    	if (Auth::check()) {
    		if($request->isMethod('post')) {
    			$collection = [];
	    		if(Session::has('buy_collection')) {
			    	$collection = Session::get('buy_collection');

			    	$purchaseItems = $this->calculateCollection($collection);

			    	$maxId = Purchase::max('id');
			    	$refNo = "PBO" . date("Y") . "_" . ($maxId + 1);

			    	$invoiceDate = date("d, F Y h:i a");

			    	$user = Auth::user();

			    	foreach($purchaseItems['purchaseItemsInfo'] as $purchaseItem) {
			    		// update purchase tables
				    	$purchase = new Purchase;
				    	$purchase->user_id = $user->id;
				    	$purchase->item_id = $purchaseItem['itemId'];
				    	$purchase->purchase_type = $purchaseItem['itemType'];
				    	$purchase->payment_method = $request->payment_type;
				    	$purchase->amount = $purchaseItem['price'];
				    	$purchase->currency = $purchaseItem['currency'];
				    	$purchase->ref_no = $refNo;
				    	$purchase->purchase_date = date("Y-m-d H:i:s");
				    	$purchase->payment_status = Purchase::PAYMENT_STATUS_NOT_PAID;

				    	$purchase->save();

				    	if($purchaseItem['itemType'] == Purchase::PURCHASE_TYPE_TEMPLATE) {
				    		$templateModel = Template::findOrFail($purchaseItem['itemId']);

				    		$pbAppId = AppDownload::where('app_serial', 'LIKE', $purchaseItem['app_serial'])
				    							->value('id');
				    		$templateModel->appDownloads()->attach($pbAppId, ['purchase_id' => $purchase->id]);
				    		$templateModel->save();
				    		
				    	}
			    	}
			    	
			    	//send the invoice
			    	$pdf = PDF::loadView('pdf.invoice', [
			    			'shoppingItems' => $purchaseItems['purchaseItemsInfo'],
			    			'total' 		=> $purchaseItems['total'],
			    			'currency'		=> 'LKR',
			    			'vat'			=> $purchaseItems['vat'],
			    			'paystatus' 	=> Purchase::PAYMENT_STATUS_NOT_PAID,
			    			'invoiceNo' 	=> $refNo,
			    			'userName'		=> $user->name,
			    			'billingAddress' => $user->billing_address ? $user->billing_address : 'Billing address not provided',
			    			'country'		=> $user->country,
			    			'invoiceDate'	=> $invoiceDate]);

			    	$invoiceTempPath = storage_path('app/tmp/' . $refNo . '.pdf');
					$pdf->save($invoiceTempPath);

					Mail::to($user->email)->queue(new PurchaseOrderMail($refNo));

			    	// distray session
			    	$request->session()->forget('buy_collection');

			    	// load purchase complete page
			    	flash('Purchase completed successfully. Please check your email for the invoice.')->success()->important();

			    	return redirect('/');

			    } else {
			    	flash('Your collection is empty');
			    	return route('/');
			    }
    		} else {
    			abort(403, 'Unauthorized action');
    		}
    		
    	} else {
    		flash('Please login into your account.');
			return redirect('login');
    	}
    }

    /**
    * Delete purchased items
    * 
    * @param Request $request
    * @return Http response
    */
    public function delete(Request $request)
    {
    	$user = Auth::user();
    	if($user && $user->can('delete', Purchase::class)) {

    		$itemIds = explode(",", $request->grid_item_ids);

    		$purchaseByInvoicesAffected = Purchase::whereIn('id', $itemIds)
    												->select('*')
    												->groupBy('ref_no')
    												->get();
    		//dd($purchaseByInvoicesAffected);								
    		
    		if(Purchase::whereIn('id', $itemIds)
    					->where('payment_status', Purchase::PAYMENT_STATUS_NOT_PAID)
    					->delete()) {

    			DB::table('apps_templats')
    				->whereIn('purchase_id', $itemIds)
    				->delete();

    			foreach ($purchaseByInvoicesAffected as $affectedInvoice) {
    				$purchaseByInvoices = Purchase::where('ref_no', 'like', $affectedInvoice->ref_no)->get();

    				$purchaseUser = User::find($purchaseByInvoices[0]->user_id);
    				$invoiceNo = $purchaseByInvoices[0]->ref_no;

    				$collectionItems = [];
    				foreach($purchaseByInvoices as $purchaseByInvoice) {
	    				$tempArray = [
							'item_type' => $purchaseByInvoice->purchase_type, 
							"item_id" => $purchaseByInvoice->item_id
						];

						if($purchaseByInvoice->purchase_type == Purchase::PURCHASE_TYPE_TEMPLATE) {
				    		$appsTemplates = DB::table('apps_templats')
				    							->select('app_id')
				    							->where('purchase_id', $purchaseByInvoice->id)
				    							->first();
				    		$pbAppSerial = AppDownload::find($appsTemplates->app_id)->value('app_serial');

				    		$tempArray["app_serial"] = $pbAppSerial;
						}

						$collectionItems[] = $tempArray;
	    			}

	    			$invoiceDate = date("d, F Y h:i a");
					$collectionInfo = $this->calculateCollection($collectionItems);

					$pdf = PDF::loadView('pdf.invoice', [
				    			'shoppingItems' => $collectionInfo['purchaseItemsInfo'],
				    			'total' 		=> $collectionInfo['total'],
				    			'currency'		=> 'LKR',
				    			'vat'			=> $collectionInfo['vat'],
				    			'paystatus' 	=> \App\Purchase::PAYMENT_STATUS_NOT_PAID,
				    			'invoiceNo' 	=> $invoiceNo,
				    			'userName'		=> $purchaseUser->name,
				    			'billingAddress' => $purchaseUser->billing_address ? $purchaseUser->billing_address : 'Billing address not provided',
				    			'country'		=> $purchaseUser->country,
				    			'invoiceDate'	=> $invoiceDate]);

			    	$invoiceTempPath = storage_path('app/tmp/' . $invoiceNo . '.pdf');
					$pdf->save($invoiceTempPath);

					Mail::to($purchaseUser->email)->queue(new PurchaseOrderMail($invoiceNo));
    			}
    			
    			flash('Items deleted successfully and revised invoice(s) sent.')->success();
    		} else {
    			flash('Something went wrong!')->error();
    		}

    		return redirect(route('admin.purchases'));

    	} else {
    		abort(403, 'Unauthorized action.');
    	}
    	
    }

    /**
    * claim free activation
    *
    */
    public function sendFreeAppKey()
    {
    	$user = Auth::user();
    	if($user->can('sendFreeAppKey', Purchase::class)) {
    		$appKey = str_random(20);
    		$randForSerial = rand(100, 999);
    		$increment = AppDownload::max('id');
    		$appSerial = 'pb' . date('y') . '-' . $randForSerial . '-' . ($increment + 1);

    		$appDownload = new AppDownload;
    		$appDownload->app_serial = $appSerial;
    		$appDownload->user_id = $user->id;
    		$appDownload->is_activated = AppDownload::APP_ACTIVATED_FALSE;
    		$appDownload->app_key = $appKey;

    		$appDownload->save();

    		// send app key to the client
    		Mail::to($user->email)->queue(new AppActivationMail($appKey, $user->name));

    		flash('Your Free Lifetime Activation key was sent to your email. Please check.')->success()->important();

    		return redirect('/');
    	} else {
    		abort(403, 'Unauthorized action.');
    	}
    }
}
