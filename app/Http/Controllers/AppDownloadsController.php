<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Mail\AppActivationMail;
use App\AppDownload;
use App\PhotobookApp;

class AppDownloadsController extends Controller
{
    public function download(Request $request) {
        $googleDrive = Storage::disk('photobookdrive');

        $filename = "";
    	
    	if($request->app_latest == 'true') {
            $platform = $request->app_platform;
            $arch = $request->app_arch;
            $type = $request->app_file_type;

    		// download latest verion
    		$latestApp = PhotobookApp::where('platform', 'LIKE', $platform)
    								->where('architecture', 'LIKE', $arch)
                                    ->where('file_type', 'LIKE', $type)
    								->where('is_latest', 1)->first();

            if($latestApp) {
                if($latestApp->platform == PhotobookApp::PLATFORM_MAC) {
                    $filename = "PhotoBook_" . $latestApp->version . "_" . str_replace(" ", "_", $latestApp->platform) ."_" . $latestApp->architecture ."bit_" . $latestApp->file_type . ".zip";
                } else {
                    $filename = "PhotoBook_" . $latestApp->version . "_" . str_replace(" ", "_", $latestApp->platform) . "_" . $latestApp->architecture ."bit_" . $latestApp->file_type .".zip";
                }
            }    		

    		//return response()->download(storage_path() . "/app/pbapps/" . $downloadName);
    	} else {
            $installerId = $request->installer_id;

            if(!$installerId || $installerId == "0") {
                flash('Something went wrong with the app download. Please try again later.')->warning();
                return redirect(route('downloads'));
            }
    		$prevVersionApp = PhotobookApp::find($installerId);

            if($prevVersionApp) {
                if($prevVersionApp->platform == PhotobookApp::PLATFORM_MAC) {
                    $filename = "PhotoBook_" . $prevVersionApp->version . "_" . str_replace(" ", "_", $prevVersionApp->platform) ."_" . $prevVersionApp->architecture ."bit_" .$prevVersionApp->file_type . ".zip";
                } else {
                    $filename = "PhotoBook_" . $prevVersionApp->version . "_" . str_replace(" ", "_", $prevVersionApp->platform) . "_" . $prevVersionApp->architecture ."bit_" . $prevVersionApp->file_type .".zip";
                }
            }

    		//return response()->download(storage_path() . "/app/pbapps/" . $downloadName);
    	}

        //var_dump($filename); exit;
        if($filename == "") {
            flash('File not found. Please find your download here.')->important();
            return redirect(route('downloads'));
        }

        $dir = '/';
        $file = self::fileFromGoogleDrive($googleDrive, $dir, $filename);

        if(!$file) {
            flash('File not found. Please contact our tech team. ' . config('pbconfig.techEmail'). ', ' . config('pbconfig.contactNo'))->important();
            return redirect(route('downloads'));
        }

        /*$rawData = $googleDrive->get($file['path']);

        return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename='$filename'");*/

        $readStream = $googleDrive->getDriver()->readStream($file['path']);
         return response()->stream(function () use ($readStream) {
            fpassthru($readStream);
        }, 200, [
            'Content-Type' => $file['mimetype'],
            'Content-Length' => $file['size'],
            'Content-disposition' => 'attachment; filename="'.$filename.'"', // force download?
        ]);
    }

    private function fileFromSubDirGoogleDrive($googleDrive, $subDirName, $filename, $recursive=false) {
        $contents = collect($googleDrive->listContents('/', false));

        $resourceDir = $contents->where('type', '=', 'dir')
                        ->where('filename', '=', $subDirName)
                        ->first(); // There could be duplicate directory names!

        $file = collect($googleDrive->listContents($resourceDir['path'], false))
                ->where('type', '=', 'file')
                ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
                ->first();

        return $file;
    }

    private function fileFromGoogleDrive($googleDrive, $dir, $filename, $recursive=false) {
        //$recursive = false; // Get subdirectories also?
        $contents = collect($googleDrive->listContents($dir, $recursive));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!
        //return $file; // array with file info

        return $file;
    }

    public function downloadUserGuide(Request $request) {
        $lang = $request->lang;

        $filename = 'PhotoBookGuide_sinhala.pdf';
        if($lang == 'en') {
            $filename = 'PhotoBookGuide_en.pdf';
        }

        $googleDrive = Storage::disk('photobookdrive');

        $subdir = 'user_guides';
        $file = self::fileFromSubDirGoogleDrive($googleDrive, $subdir, $filename);

        if(!$file) {
            flash('File not found. Please contact our tech team. ' . config('pbconfig.techEmail'). ', ' . config('pbconfig.contactNo'))->important();
            return redirect(route('home'));
        }

        $rawData = $googleDrive->get($file['path']);

        return response($rawData, 200)
            ->header('Content-Type', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename='".$filename."'");
    }

    // get PhotoBook app key
    public function getAppKey() {
        if(!Auth::check()) {
            flash("To purchase your App Key, please login into your PhotoBook account.");
            return redirect('login');
        } else {
            return redirect(route('free_app_key'));
        }
    }

    // app key installation
    public function activateApp(Request $request) {
        $activationKey = $request->appkey;
               
        $appDownload = AppDownload::where('app_key', 'LIKE', $activationKey)->first();
       
        $respArray = [];
        if(count($appDownload) > 0) {
            $isActivated = $appDownload->is_activated;
            $serialNo = $appDownload->app_serial;

            if($isActivated == AppDownload::APP_ACTIVATED_TRUE) {
                $respArray = ['activation_response' => 'used'];
            } else {
                $appDownload->activated_date = date("Y-m-d H:i:s");
                $appDownload->is_activated = AppDownload::APP_ACTIVATED_TRUE;
                $appDownload->save();

                Mail::to($appDownload->user->email)->queue(new AppActivationMail(null, $appDownload->user->name));
                
                $respArray = ['activation_response' => true, 'serial_no' => $serialNo];
            }
        } else {
            $respArray = ['activation_response' => false];
        }
        
        return json_encode($respArray);
    }

    // set app name
    public function setAppName(Request $request) {
        $appName = $request->app_name;
        $appSerial = $request->app_serial;

        $respArray = [];
        $appDownloadModel = AppDownload::where('app_serial', 'LIKE', $appSerial)->first();

        $checkForExists = AppDownload::where('user_id', 'LIKE', $appDownloadModel->user_id)
                                        ->where('app_label', 'LIKE', $appName)->get();

        if(count($checkForExists) > 0) {
            $respArray = ['set_app_name_resp' => 'exists'];
        } else {            
            $appDownloadModel->app_label = $appName;
            $appDownloadModel->save();

            $respArray = ['set_app_name_resp' => true];
        }

        return json_encode($respArray);        
    }

    /**
    * Index app downloads
    *
    */
    public function index(Request $request)
    {
        $user = Auth::user();
        if($user && $user->can('index', AppDownload::class)) {

            $appDownloads = new AppDownload;

            if($request->has('filter_user') && $request->filter_user != "") {
                $appDownloads = $appDownloads->whereHas('user', function($q) use($request){
                    $q->where('name', 'like', '%' . $request->filter_user . '%');
                });
            }

            if($request->has('filter_app_serial') && $request->filter_app_serial != "") {
                $appDownloads = $appDownloads->where('app_serial', 'like', $request->filter_app_serial);
            }

            if($request->has('filter_app_label') && $request->filter_app_label != "") {
                $appDownloads = $appDownloads->where('app_label', 'like', $request->filter_app_label);
            }

            if($request->has('filter_is_activated') && $request->filter_is_activated != "") {
                $appDownloads = $appDownloads->where('is_activated', $request->filter_is_activated);
            }

            $appDownloads = $appDownloads->paginate(20);

            return view('admin.app_downloads')->with([
                        'appDownloads' => $appDownloads,
                    ]);

        } else {
            if(!$user) {
                flash('Please login into an admin account.');
                return redirect(route('login'));
            } else {
                abort(403, 'Unauthoriazed action.');
            }
        }
    }
}
