<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resonse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Template;

class TemplatesController extends Controller
{
    public function getThumbnails(Request $request) {
    	if($request->ajax()) {
    		$id =  $request->id;
	    	$template = Template::find($id);
	    	$pages = $template->pages;

	    	$thumbFiles = [];
	    	for($a = 1; $a <= $pages; $a++) {
	    		$thumbUrl = '/templates/' . $template->id . '/' . $a . '_thumb.jpg';
	    		$noImage = "no_image";

	    		if(file_exists(public_path() . $thumbUrl)) {
	    			$thumbFiles[$a] = asset($thumbUrl);
	    		} else {
	    			$thumbFiles[$a] = $noImage;
	    		}
	    	}

	    	//return view('partials.carousel')->with('items', $thumbFiles);
            return view('partials.flipbook')->with('items', $thumbFiles);
    	} else {
    		return 'invalid request';
    	}
    	
    }

    protected function templateThumbs($id)
    {
        $template = Template::find($id);
        $pages = $template->pages;

        $thumbFiles = [];
        for($a = 1; $a <= $pages; $a++) {
            $thumbUrl = '/templates/' . $template->id . '/' . $a . '_thumb.jpg';
            $noImage = "no_image";

            if(file_exists(public_path() . $thumbUrl)) {
                $thumbFiles[$a] = asset($thumbUrl);
            }
        }

        return $thumbFiles;
    }

    // get all templates
    public function allTemplates(Request $request) {
    	$templates = Template::where('published', Template::PUBLISH_TRUE)->orWhere('published', Template::COMING_SOON)->orderBy('id', 'desc')->get();
    	$albumSizes = config('pbconfig.albumSizes');

    	return view('templates')
    			->with('allTemplates', $templates)
    			->with('albumSizes', $albumSizes)
                ->with('bodyClass', 'templates-page');
    }

    /**
    * List templates
    * 
    */

    public function index(Request $request)
    {
        $user = Auth::user();
        if($user && $user->can('index', Template::class)) {
            $templates = new Template;

            if($request->has('filter_tem_name') && $request->filter_tem_name != "") {
                $templates = $templates->where('tem_name', 'like', '%' . $request->filter_tem_name . '%');
            }

            if($request->has('filter_sizes') && $request->filter_sizes != "") {
                $templates = $templates->where('filter_sizes', 'like', '%' . $request->filter_sizes . '%');
            }

            if($request->has('filter_user') && $request->filter_user != "") {
                $templates = $templates->whereHas('user', function($q) use($request){
                    $q->where('name', 'like', '%' . $request->filter_user . '%');
                });
            }

            if($request->has('filter_storage_drive') && $request->filter_storage_drive != "") {
                $templates = $templates->where('storage_drive', $request->filter_storage_drive);
            }

            if($request->has('filter_published') && $request->filter_published != "") {
                $templates = $templates->where('published', $request->filter_published);
            }

            $templates = $templates->paginate(50);

            return view('admin.templates')->with('templates', $templates);
        } else {
            if(!$user) {
                return redirect(route('login'));
            } else {
                abort(403, 'Unauthorized action.');
            }            
        }
    }

    /**
    * edit templates
    * 
    * @param integer $id
    * @return mixed
    */
    public function edit($id)
    {
        $user = Auth::user();
        if($user && $user->can('edit', Template::class)){
            $template = Template::find($id);
            $thumbs = $this->templateThumbs($id);
            return view('admin.edit_template')->with(['template' => $template, 'thumbs' => $thumbs]);
        } else {
            if(!$user) {
                return redirect(route('login'));
            } else {
                abort(403, 'Unauthorized action.');
            }   
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $v = Validator::make($data, [
            'tem_name' => 'required|string|max:255',
            'version' => 'required|integer',
            'pages' => 'required|integer',
            'placeholders' => 'required|integer',
            'sizes' => 'required',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'thumbnails.*' => 'mimes:jpeg,jpg',
        ]);

        return $v;
    }

    /**
    * update template details
    * 
    */
    public function update(Request $request, $id)
    {
        $template = Template::find($id);
        $user = Auth::user();
        if($user && $user->can('update', $template)){
            $validator = $this->validator($request->all());
            if($validator->fails()){
                return redirect(route('admin.edit_template', ['id' => $id]))
                        ->withErrors($validator)
                        ->withInput();
            }     

            $template->tem_name = $request->tem_name;
            $template->version = $request->version;
            $template->pages = $request->pages;
            $template->sizes = implode(",", $request->sizes);
            $template->placeholders = $request->placeholders;
            $template->user_id = $request->user_id;
            $template->price = $request->price;
            $template->currency = $request->currency;
            $template->discount = $request->discount;
            $template->published = $request->published;
            $template->updated_at = date("Y-m-d H:i:s");
            $template->storage_drive = $request->storage_drive;

            if($template->save()) {
                if($request->thumbnails) {
                    foreach($request->thumbnails as $thumb){
                        $thumbFileName = $thumb->getClientOriginalName();
                        $thumb->storeAs($template->id, $thumbFileName, 'template_thumb');
                    }        
                }

                flash('Template details updated successfully.')->success();
            } else {
                flash('Something went wrong!')->error();
            }

            return redirect(route('admin.templates'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
    * create new template
    * 
    */
    public function create(Request $request)
    {
        $user = Auth::user();
        if($user && $user->can('create', Template::class)){
            if($request->isMethod('get')) {

                return view('admin.create_template');

            } else if($request->isMethod('post')) {
                $validator = $this->validator($request->all());
                if($validator->fails()) {
                    return redirect(route('admin.create_template'))
                            ->withErrors($validator)
                            ->withInput();
                }

                if($this->store($request->all())){
                    flash('Template details saved successfully.')->success();
                } else {
                    flash('Something went wrong!')->error();
                }
            } else {
                abort(403, 'Unauthorized action.');
            }
            
            return redirect(route('admin.templates'));
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
    * store template data
    * 
    */
    protected function store(Array $data)
    {
        $template = new Template;
        $template->tem_name = $data['tem_name'];
        $template->version = $data['version'];
        $template->pages = $data['pages'];
        $template->sizes = implode(",", $data['sizes']);
        $template->placeholders = $data['placeholders'];
        $template->user_id = $data['user_id'];
        $template->price = $data['price'];
        $template->currency = $data['currency'];
        $template->discount = $data['discount'];
        $template->published = $data['published'];
        $template->stars = 0;
        $template->downloads = 0;
        $template->created_at = date("Y-m-d H:i:s");
        $template->storage_drive = $data['storage_drive'];

        if($template->save()) {
            if($data['thumbnails']) {
                foreach($data['thumbnails'] as $thumb){
                    $thumbFileName = $thumb->getClientOriginalName();
                    $thumb->storeAs($template->id, $thumbFileName, 'template_thumb');
                }        
            }

            // create template directory
            Storage::makeDirectory(storage_path() . '/app/templates/' . $template->id);
        } else {
            return false;
        }

        return true;
    }

    /**
    * Publish/Unpublish templates
    * @param Request $request
    * @return Http response
    */
    public function publish(Request $request)
    {
        $user = Auth::user();
        if($user && $user->can('publish', Template::class)){
            $templateIds = explode(",", $request->grid_item_ids);

            $publish = Template::PUBLISH_FALSE;
            $publishedText = 'un-published';
            if($request->publish == 'publish') {
                $publish = Template::PUBLISH_TRUE;
                $publishedText = 'published';
            } else if($request->publish == 'coming_soon') {
                $publish = Template::COMING_SOON;
                $publishedText = ' status changed to coming soon';
            }

            $template = Template::whereIn('id', $templateIds)->update(['published' => $publish]);

            if($template) {
                flash('Selected template(s) ' . $publishedText)->success();
            } else {
                flash('Something went wrong!')->error();
            }

            return redirect(route('admin.templates'));
        }
    }

}
