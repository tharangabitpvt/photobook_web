<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAssignment extends Model
{
    public $timestamps = false;
    protected $table = 'ft_job_assignments';

}
