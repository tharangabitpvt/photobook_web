<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
	const PUBLISH_FALSE = 0;
	const PUBLISH_TRUE = 1;
    const COMING_SOON = 2;

	const publishState = [
		self::PUBLISH_FALSE => 'Unpublished',
		self::PUBLISH_TRUE => 'Published',
        self::COMING_SOON => 'Coming soon',
	];

	// relationships
	public function appDownloads() {
    	return $this->belongsToMany('App\AppDownload', 'apps_templats', 'template_id', 'app_id');
    }

    // user
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
    * get model thumbmail
    * 
    */
    public function thumbnail()
    {
        $thumbUrl = '/templates/' . $this->id . '/1_thumb.jpg';

        $thumbFile = null;
        if(file_exists(public_path() . $thumbUrl)) {
            $thumbFile = asset($thumbUrl);
        }

        return $thumbFile;
    }
}
