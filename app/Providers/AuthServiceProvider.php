<?php

namespace App\Providers;

use App\Message;
use App\User;
use App\PhotobookApp;
use App\Template;
use App\Purchase;
use App\AppDownload;
use App\Policies\MessagePolicy;
use App\Policies\UserPolicy;
use App\Policies\PhotobookAppPolicy;
use App\Policies\TemplatePolicy;
use App\Policies\PurchasePolicy;
use App\Policies\AppDownloadPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Message::class => MessagePolicy::class,
        User::class => UserPolicy::class,
        PhotobookApp::class => PhotobookAppPolicy::class,
        Template::class => TemplatePolicy::class,
        Purchase::class => PurchasePolicy::class,
        AppDownload::class => AppDownloadPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
