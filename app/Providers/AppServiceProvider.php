<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Storage;

class AppServiceProvider extends ServiceProvider
{
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('datetime', function ($expression) {
            return "<?php echo $expression && $expression != '' ? Carbon\Carbon::parse($expression)->format('d, M Y h:i a') : 'n/a'; ?>";
        });

        // convert amount to any currency format
        Blade::directive('currency', function ($value, $fromCurrency='LKR', $toCurrency='USD') {
            return "<?php echo App\Http\Controllers\HelperController::currencyConvert($value, '$fromCurrency', '$toCurrency'); ?>";
        });

        // buy template button
        Blade::directive('buyTemplateBtn', function($templateId) {
            return "<?php echo App\Http\Controllers\HelperController::buyTemplateButton($templateId); ?>";
        });

        Blade::directive('statusIcon', function($status){
            return "<?php echo $status == 1 ? '<i class=\'fa fa-check-circle text-success ft-status-icon\' aria-hidden=\'true\' title=\'published\'></i>' : '<i class=\'fa fa-minus-circle text-danger ft-status-icon\' aria-hidden=\'true\' title=\'un-published\'></i>' ?>";
        });

        Blade::directive('tempStatusIcon', function($status){
            return "<?php if($status == 1){ echo '<i class=\'fa fa-check-circle text-success ft-status-icon\' aria-hidden=\'true\' title=\'published\'></i>';} elseif($status == 2) { echo '<i class=\'fa fa-clock-o text-info ft-status-icon\' aria-hidden=\'true\' title=\'coming soon\'></i>';} else { echo '<i class=\'fa fa-minus-circle text-danger ft-status-icon\' aria-hidden=\'true\' title=\'un-published\'></i>';} ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
