<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppDownload extends Model
{
	public $timestamps = false;
	
	const APP_ACTIVATED_TRUE = 1;
	const APP_ACTIVATED_FALSE = 0;

	const appKeyStatus = [
		self::APP_ACTIVATED_FALSE => 'Trial/Inactive',
		self::APP_ACTIVATED_TRUE => 'Activated',
	];

    // relationships
    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function templates() {
    	return $this->belongsToMany('App\Template', 'apps_templats', 'app_id', 'template_id');
    }

    public function newTemplates() {
    	return $this->belongsToMany('App\Template', 'apps_templats', 'app_id', 'template_id')->withPivot('purchase_id')->wherePivot('is_installed', 0);
    }

    public function installedTemplates() {
        return $this->belongsToMany('App\Template', 'apps_templats', 'app_id', 'template_id')->withPivot('purchase_id')->wherePivot('is_installed', 1);
    }
}
