<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Purchase;
use App\User;

class Notification extends Model
{
    // show not paid notification
    public static function getPayStatusNotifications($purchaseStatus) {
    	if(Auth::check()) {
    		$user = Auth::user();

    		$notPaidPurchasesCount = 0;
    		if($user->user_type == User::USER_TYPE_APP_USER) {
    			$notPaidPurchasesCount = Purchase::where('user_id', $user->id)
    										->where('payment_status', $purchaseStatus)
    										->count();
    		}

    		return $notPaidPurchasesCount;
    	}
    }
}
