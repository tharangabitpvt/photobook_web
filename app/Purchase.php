<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	public $timestamps = false;
	
    const PURCHASE_TYPE_TEMPLATE = 1;
	const PURCHASE_TYPE_JOB = 2;
	const PURCHASE_TYPE_APPKEY = 3;
	const PURCHASE_TYPE_TEMPLATE_UPDATE = 4;

	const PAYMENT_METHOD_INFORM_LATER = 0;
	const PAYMENT_METHOD_BANK = 1;
	const PAYMENT_METHOD_CREDIT_CARD = 2;
	const PAYMENT_METHOD_MCASH = 3;
	const PAYMENT_METHOD_EZCASH = 4;

	const PAYMENT_STATUS_NOT_PAID = 0;
	const PAYMENT_STATUS_PENDING = 1;
	const PAYMENT_STATUS_PAID = 2;
	const PAYMENT_STATUS_REJECTED = 3;
	const PAYMENT_STATUS_CANCELED = 4;

	const itemTypeName = [
		self::PURCHASE_TYPE_TEMPLATE 	=> "Template",
		self::PURCHASE_TYPE_JOB 		=> "Design job",
		self::PURCHASE_TYPE_APPKEY	=> "PhotoBook App activation key",
		self::PURCHASE_TYPE_TEMPLATE_UPDATE => "Template update",
	];

	const paymentStatus = [
		self::PAYMENT_STATUS_NOT_PAID => 'NOT PAID',
		self::PAYMENT_STATUS_PENDING => 'PENDING',
		self::PAYMENT_STATUS_PAID => 'PAID',
		self::PAYMENT_STATUS_REJECTED => 'REJECTED',
		self::PAYMENT_STATUS_CANCELED => 'CANCELED',
	];

	const paymentMethod = [
		self::PAYMENT_METHOD_INFORM_LATER => 'Inform later',
		self::PAYMENT_METHOD_BANK => 'Bank transfer',
		self::PAYMENT_METHOD_CREDIT_CARD => 'Credit card',
		self::PAYMENT_METHOD_MCASH => 'mCash',
		self::PAYMENT_METHOD_EZCASH => 'ezCash',
	];

	// relation ships
	public function user() {
    	return $this->belongsTo('App\User');
    }

    public function getPurchaseItem() {
    	$itemId = $this->item_id;
    	$purchasedItem = 'n/a';
    	//dd($itemId);
		if($this->purchase_type === self::PURCHASE_TYPE_TEMPLATE) {
    		$purchasedItem = \App\Template::find($itemId)->tem_name;
    	} else if($this->purchase_type === self::PURCHASE_TYPE_JOB) {
    		$jobType = \App\Job::find($itemId)->job_type;
    		$purchasedItem = \App\Job::jobTypeName($jobType);
    	} else if($this->purchase_type === self::PURCHASE_TYPE_APPKEY || $this->purchase_type === self::PURCHASE_TYPE_TEMPLATE_UPDATE) {
    		$purchasedItem = self::itemTypeName[$this->purchase_type];
    	}
    	

    	return $purchasedItem;
    }
}
