<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
	public $timestamps = false;
	protected $table = 'ft_jobs';

	const JOB_TYPE_TEMPLATE_DESIGN = 1;
	const JOB_TYPE_ALBUM_DESIGN = 2;
	const JOB_TYPE_FONT_DESIGN = 3;
	const JOB_TYPE_ARTWORK_DESIGN = 4;

	const jobTypeName = [
		self::JOB_TYPE_TEMPLATE_DESIGN => 'Template design',
		self::JOB_TYPE_ALBUM_DESIGN => 'Album design',
		self::JOB_TYPE_FONT_DESIGN => 'Font design',
		self::JOB_TYPE_ARTWORK_DESIGN => 'Artwork design',
	];

    // relationships
    public function users() {
    	return $this->belongsToMany('App\User');
    }
}
