<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotobookApp extends Model
{
    public $timestamps = false;
    
    const APP_STABLE_LEVEL_ALPHA 	= 1;
    const APP_STABLE_LEVEL_BETA 	= 2;
    const APP_STABLE_LEVEL_STABLE 	= 3;

    const PLATFORM_WINDOWS = 'Windows';
    const PLATFORM_MAC = 'Mac OS';

    const RELEASE_UPDATE = 1;
    const RELEASE_NEW = 2;

    const UPDATE_PRIORITY_LOW = 1;
    const UPDATE_PRIORITY_MEDUIM = 2;
    const UPDATE_PRIORITY_HIGH = 3;

    const PUBLISHED_YES = 1;
    const PUBLISHED_NO = 0;

    const FILE_TYPE_EXE = 'exe';
    const FILE_TYPE_MSI = 'msi';
    const FILE_TYPE_DMG = 'dmg';
    const FILE_TYPE_PKG = 'pkg';

    const appStableLevel = [
    	self::APP_STABLE_LEVEL_ALPHA 	=> "Alpha",
    	self::APP_STABLE_LEVEL_BETA		=> "Beta",
    	self::APP_STABLE_LEVEL_STABLE	=> "Stable"
    ];

    const releaseType = [
        self::RELEASE_UPDATE => "Update",
        self::RELEASE_NEW => "New version",
    ];

    const releasePriority = [
        self::UPDATE_PRIORITY_LOW => 'Low',
        self::UPDATE_PRIORITY_MEDUIM => 'Medium',
        self::UPDATE_PRIORITY_HIGH => 'High',
    ];

    const platforms = [
        self::PLATFORM_WINDOWS => 'Windows',
        self::PLATFORM_MAC => 'Mac OS',
    ];

    const architectures = [
        '64' => 'x64 bit',
        '32' => 'x32 bit',
    ];

    const fileTypes = [
        self::FILE_TYPE_EXE => 'exe',
        self::FILE_TYPE_MSI => 'msi',
        self::FILE_TYPE_DMG => 'dmg',
        self::FILE_TYPE_PKG => 'pkg',
    ];

    const publishState = [
        self::PUBLISHED_NO => 'Unpublished',
        self::PUBLISHED_YES => 'Published',
    ];


    // relationships
    public function downloadCount() {
        return $this->hasMany('App\AppDownloadCount', 'app_id', 'id');
    }
}
