<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	const NEW_MESSAGE_TRUE = 1;
	const NEW_MESSAGE_FALSE = 0;

	const REPLIED_TRUE = 1;
	const REPLIED_FALSE = 0;

    // relationships
    public function user() {
    	return $this->hasOne('App\User');
    }

    public static function unreadMessages() {
    	$unreadedMessages = self::where('is_new', self::NEW_MESSAGE_TRUE)->count();
    	return $unreadedMessages;
    }
}
