<div class="modal fade" id="user_guide_modal" tabindex="-1" role="dialog" aria-labelledby="user_guide_modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<div class="modal-header">
    		<h3><i class="fa fa-info-circle" aria-hidden="true"></i> User guide</h3>
    	</div>
       	<div class="modal-body text-center">
    			<h4>Sinhala (සිංහල) and English versions are available</h4>
    			<a href="{{route('download_user_guide', ['lang' => 'sinhala'])}}" class="btn ft-btn" title="Sinhala"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> සිංහල</a>
    			<a href="{{route('download_user_guide', ['lang' => 'en'])}}" class="btn ft-btn" title="English"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> English</a>
      	</div>
      	<div class="modal-footer">
	      	<button class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>
    </div>
  </div>
</div>