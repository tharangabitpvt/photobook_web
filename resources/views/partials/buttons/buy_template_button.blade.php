@if(Auth::check())
	@if(count($activeApps) > 0)		
		<div class="dropdown buy-template-dropdown inline-block">
		  <button id="temp_{{ $templateId }}" class="btn ft-btn btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    Add to PhotoBook
		    <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" aria-labelledby="temp_{{ $templateId }}">
		    @foreach($activeApps as $activeApp)
		    	@if(isset($activeAppTemplates[$activeApp->app_serial]) && in_array($templateId, $activeAppTemplates[$activeApp->app_serial]))
		    		<li class="label-bought" title="already bought"><i class="fa fa-check fa-fw" aria-hidden="true"></i> {{ $activeApp->app_label ? $activeApp->app_label : $activeApp->app_serial }}</li>
		    	@else
		    		<li class="buy-template-btn" data-ft-itemid="{{ $templateId }}" data-ft-item-type="1" data-app-serial="{{ $activeApp->app_serial }}">
		    			<i class="fa fa-shopping-bag fa-fw" aria-hidden="true"></i> {{ $activeApp->app_label ? $activeApp->app_label : $activeApp->app_serial }}
		    		</li>
				@endif
		    @endforeach
		  </ul>
		</div>
	@else
		<div class="label-bought" title="You don't have activated apps yet."><i class="fa fa-exclamation-circle" aria-hidden="true"></i> No active apps</div>	
	@endif							
@else
	<a href="{{ route('login') }}" class="btn ft-btn btn-sm"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Login and buy</a>
@endif