<div class="modal fade" id="shop_collection_modal" tabindex="-1" role="dialog" aria-labelledby="shop_collection_modalLabel" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    		<h4><i class="fa fa-shopping-bag" aria-hidden="true"></i> Your collection <span class="item-count"></span></h4>
    	</div>
      	<div class="modal-body">
	      	<div class="shop-cart">
	      		<div class="row">
	      			<div class="shoping-items-list">
	      				<div class="loader"></div>
		      		</div>
	      		</div>
	      	</div>
      	</div>
      	<div class="modal-footer">
          <div>
        		<button class="btn btn-default btn-danger ft-discard-collection-btn"><i class="fa fa-times-circle" aria-hidden="true"></i> Discard collection</button>
        		<button class="btn btn-default btn-info" data-dismiss="modal"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Collect more</button>
        		<a href="/purchase/checkout" class="btn ft-btn ft-checkout-btn"><i class="fa fa-money" aria-hidden="true"></i> Checkout</a>
          </div>
          <div>
            <small>By purchasing with photobook.lk you are agreed to our <a class="ft-tc-link" href="js:void(0)">Terms &amp; conditions</a></small>
          </div>
      	</div>
    </div>
  </div>
</div>
