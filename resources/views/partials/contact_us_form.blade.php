{!! Form::open(['url' => '/contactus/send']) !!}
	<div class="row">
		<div class="col-lg-6 col-md-6">
			<div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
				@php 
					$fname = $lname = $auEmail = $auPhone = ''; 
					$auDialCode = '+94';
				@endphp
				@if (Auth::check())
					@php 
						$nameExplode = explode(" ", Auth::user()->name, 2);
						$fname = $nameExplode[0];
						$lname = isset($nameExplode[1]) ? $nameExplode[1] : "";

						$auEmail = Auth::user()->email;
						$auPhone = Auth::user()->phone;
						$auDialCode = Auth::user()->dial_code;
					@endphp
				@endif
				{{ Form::label('f_name', 'First name *') }}
				{{ Form::text('f_name', $fname, ['class' => 'form-control', 'placeholder' => 'Your first name (required)']) }}

				@if ($errors->has('f_name'))
                    <span class="error-block">
                        {{ $errors->first('f_name') }}
                    </span>
                @endif
			</div>
		</div>
		<div class="col-lg-6 col-md-6">
			<div class="form-group{{ $errors->has('l_anme') ? ' has-error' : '' }}">
				{{ Form::label('l_anme', 'Last name') }}
				{{ Form::text('l_name', $lname, ['class' => 'form-control', 'placeholder' => 'Your last name']) }}

				@if ($errors->has('l_anme'))
                    <span class="error-block">
                        {{ $errors->first('l_anme') }}
                    </span>
                @endif
			</div>
		</div>
	</div>

	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		{{ Form::label('email', 'Email *') }}
		{{ Form::email('email', $auEmail, ['class' => 'form-control', 'placeholder' => 'Your email address (required)']) }}

		@if ($errors->has('email'))
            <span class="error-block">
                {{ $errors->first('email') }}
            </span>
        @endif
	</div>

	<div class="row">
		<div class="col-lg-7 col-md-7">
			<div class="form-group">
				{{ Form::label('phone', 'Phone number') }}
				<div class="row">
					<div class="col-md-5 col-lg-5">
						{!! Form::select('dial_code', \App\User::dialingCodesForDropdown(), $auDialCode, ['id' => 'dial_code', 'class' => 'form-control ft-select2']) !!}
					</div>
					<div class="col-md-7 col-lg-7{{ $errors->has('phone') ? ' has-error' : '' }}">
						<div class="row">
							{{ Form::text('phone', $auPhone, ['class' => 'form-control', 'placeholder' => 'mobile number']) }}

							@if ($errors->has('phone'))
			                    <span class="error-block">
			                        {{ $errors->first('phone') }}
			                    </span>
			                @endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5 col-md-5">
			<div class="form-group">
				{{ Form::label('contact_for', 'Product or service') }}
				{{ Form::select('contact_for', config('pbconfig.productAndServices'), 'General info', ['class' => 'form-control']) }}
			</div>
		</div>
	</div>

	<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
		{{ Form::label('message', 'Message') }} <small>(max 500 characters)</small>
		{{ Form::textarea('message', '', ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Explain what you need to know']) }}

		@if ($errors->has('message'))
            <span class="error-block">
                {{ $errors->first('message') }}
            </span>
        @endif
	</div>

	<div class="form-group{{ $errors->has('g_recaptcha') ? ' has-error' : '' }}">
		{!! NoCaptcha::display() !!}

		@if ($errors->has('g-recaptcha-response'))
		    <span class="error-block">
		        {{ $errors->first('g-recaptcha-response') }}
		    </span>
		@endif
	</div>
	<div class="form-group">
		<small>Read <a class="ft-tc-link" href="js:void(0)">Terms &amp; conditions</a></small><br/>
		{{ Form::submit('Send', ['class' => 'btn btn-primary', 'name' => 'enquiry_submit_btn', 'id' => 'enquiry_submit_btn']) }}
	</div>
{!! Form::close() !!}