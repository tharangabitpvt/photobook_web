<div class="modal fade" id="template_preview_modal" tabindex="-1" role="dialog" aria-labelledby="template_preview_modalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
       	<div class="modal-body">
      		<div class="loader"></div>
      	</div>
      	<div class="modal-footer">
	      	<div class="row">
		      	<div class="col-md-8 col-lg-8 footer-title">
			      	<h3><span class="flaticon flaticon-brush-history"></span>&nbsp;<span id="album_name_on_preview"></span></h3>
		      	</div>
		      	<div class="col-md-4 col-lg-4">
		      		<div class="album-buttons">
						<span class="price">
							<span class="price-lkr"></span><br/>
							<span class="price-usd"></span>
						</span>
						<button data-ft-itemid="" data-ft-item-type="1" class="btn ft-btn btn-sm buy-template-btn"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Add to collection</button>
					</div>
		      	</div>
	      	</div>
	      	<div class="hint"> The preview images are in 10x24 size. The layout may be varied according to the album size. But overall design is still the same.</div>
      	</div>
    </div>
  </div>
</div>
