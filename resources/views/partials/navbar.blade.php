<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ft_navbar" aria-expanded="false">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
      <a class="navbar-brand" href="/"><img src="{{asset('/images/square_logo.png')}}" width="50"></a>
    </div>

    <div class="collapse navbar-collapse" id="ft_navbar">
    	<ul class="nav navbar-nav">
    		<li class="{{ Request::is('about') ? 'active' : '' }}">
                <a href="/about">About</a>
            </li>
    		<li class="{{ Request::is('products_and_services') ? 'active' : '' }}">
                <a href="/products_and_services">Prducts &amp; services</a>
            </li>
    		<li class="{{ Request::is('photobook_templates') ? 'active' : '' }}">
                <a href="/photobook_templates">Templates</a>
            </li>
    		<li class="{{ Request::is('downloads') ? 'active' : '' }}">
                <a href="/downloads">Downloads</a>
            </li>
    		<li class="{{ Request::is('contact-us') ? 'active' : '' }}">
                <a href="/contact-us">Contact us</a>
            </li>
            <!--li class="app-key-link" title="PhotoBook life time activtion">
                @guest
                <a href="/get_app_key">
                    <small>Trial period over?</small><br/>
                    <i class="fa fa-key" aria-hidden="true"></i> ACTIVATE NOW!
                </a> 
                @else
                <a class="get-app-key" data-item-type="{{ \App\Purchase::PURCHASE_TYPE_APPKEY }}" href="javascript:void()">
                    <small>Trial period over?</small><br/>
                    <i class="fa fa-key" aria-hidden="true"></i> ACTIVATE NOW!
                </a>  
                @endguest              
            </li -->
            <li class="app-key-link" title="PhotoBook life time activtion">
                @guest
                <a href="/get_app_key">
                    <small>special offer!!!</small><br/>
                    FREE LIFETIME ACTIVATION
                </a> 
                @else
                <a href="{{ route('free_app_key') }}">
                    <!--small>Trial period over?</small><br/>
                    <i class="fa fa-key" aria-hidden="true"></i> ACTIVATE NOW! -->
                     <small>special offer!!!</small><br/>
                    FREE LIFETIME ACTIVATION
                </a>  
                @endguest              
            </li>
    	</ul>

    	<div id="ft_shop_collection" class="ft-shop-collection pull-right" title="your collection" data-toggle="popover" data-trigger="manual" data-placement="bottom" data-content="Items added to the collection">
	    	<div class="icon">
	    		<i class="fa fa-shopping-bag" aria-hidden="true"></i>
	    	</div>
	    	@if(Session::has('buy_collection'))
	    	<div class="collection-items animated bounce">{{count(Session::has('buy_collection'))}}</div>
	    	@else
	    	<div class="collection-items animated bounce">0</div>
	    	@endif
	    </div>

	    <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @guest
                <li><a href="{{ route('login') }}">Login</a></li>
                <li><a href="{{ route('register') }}">Register</a></li>
            @else
                <li>
                    <a href="/dashboard" role="button" aria-expanded="false">
                        @php $notificationBadgeClass = ''; @endphp
                        @if(\App\Notification::getPayStatusNotifications(\App\Purchase::PAYMENT_STATUS_NOT_PAID) > 0)
                            @php $notificationBadgeClass = 'badge-danger'; @endphp
                        @endif
                        <i class="fa fa-bell-o" title="notifications" aria-hidden="true"></i> <span class="badge notification-badge {{ $notificationBadgeClass }}">{{ \App\Notification::getPayStatusNotifications(\App\Purchase::PAYMENT_STATUS_NOT_PAID) }}</span>
                    </a>
                </li>
                <li class="dropdown" style="width: 160px;">
                    <a href="#" class="dropdown-toggle ft-truncate" title="{{ Auth::user()->name }}" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('dashboard') }}"><i class="fa fa-fw fa-desktop" aria-hidden="true"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                                <i class="fa fa-fw fa-sign-out" aria-hidden="true"></i> Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        </ul>
    </div>
  </div>
</nav>