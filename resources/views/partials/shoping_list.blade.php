<div class="container-fluid">
	@foreach($shopingItems as $key => $shopingItem)
		@php $bgClass = 'odd-bg'; @endphp
		@if($key%2 == 0)
			@php $bgClass = 'even-bg'; @endphp
		@endif
		<div class="shoping-item {{$bgClass}}">
			<div class="shoping-item-name ft-truncate"><i class="fa fa-times-circle text-danger remove-item" data-item-id="{{$shopingItem['itemId']}}" data-item-type="{{$shopingItem['itemType']}}" title="remove item"></i>&nbsp;&nbsp;{{$shopingItem['name']}} <span class="item-type">({{$shopingItem['itemTypeName']}})</span></div>
			<div class="shoping-item-price">{{$shopingItem['price']}} {{$shopingItem['currency']}}<br/>
				<span class="price-usd">@currency($shopingItem['price'])</span>
			</div>
		</div>
	@endforeach
	<div class="total-amout">{{$total}} {{$shopingItem['currency']}}<br/>
		<span class="price-usd">@currency($total)</span>
	</div>
</div>