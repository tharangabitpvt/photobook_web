<div class="ft-sub-section latest-templates-section ft-scrollify-section" data-section-name="latest-templates">
	<div class="container">
		<div class="ft-content">
			<div class="ft-inner">
				<div class="desc">
					<h3>Latest templates</h3>
					<p>Check our latest templates here. To view all templates click <a href="/photobook_templates">here</a>.</p>
				</div>
				<div class="row templates-list">
					@for($i = 0; $i < 3; $i++)
						@php $template = isset($latestTemplates[$i]) ? $latestTemplates[$i] : null @endphp
						@if($template)
							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
								<div class="ft-template-box">
									<div class="album-title">
										<h4 class="ft-truncate"><span class="flaticon flaticon-brush-history"></span> {{ $template->tem_name }}</h4>
									</div>
									@php
										$templatePrice = $template->price;
										$dicountValue = 0;
										if($template->discount > 0) {
											$dicountValue = ($template->price * $template->discount)/100;
											$templatePrice = $template->price - $dicountValue;
										}
									@endphp
									<div class="enlarge-icon" title="enlarge">
										<a class="btn btn-default btn-zoom show-temp-preview" data-toggle="modal" data-target="#template_preview_modal" data-template-id="{{$template->id}}" data-template-name="{{ $template->tem_name }}" data-noofpages="{{$template->pages}}" data-price-lkr="{{ $templatePrice . ' ' . $template->currency }}" data-price-usd="@currency($templatePrice)" data-tmpl-status="{{ $template->published }}"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
									</div>
									<div class="album-thumb">
										@php
											$thumbImage = '/images/file_not_found.png';
											$hasImage = '';
											if(file_exists(public_path() . '/templates/' . $template->id . '/1_thumb.jpg')) {
												$thumbImage = '/templates/' . $template->id . '/1_thumb.jpg';
												$hasImage = 'has-thumbnail';
											}
										@endphp
										<img src="{{asset($thumbImage)}}" class="{{$hasImage}}" />
									</div>
									
									<div class="album-sizes">
									@foreach($albumSizes as $albumSize)
										@php 
											$temAlbumSizeArray = explode(",", $template->sizes);
										@endphp
										@if(in_array($albumSize, $temAlbumSizeArray))
											<span class="badge selected">{{ $albumSize }}</span>
										@else
											<span class="badge">{{ $albumSize }}</span>
										@endif
									@endforeach
									</div>
									<div class="album-info">
										<span class="item badge" title="number of pages">
											<i class="fa fa-columns" aria-hidden="true"></i> {{ $template->pages }}
										</span>
										<span class="item badge" title="number of placeholders">
											<i class="fa fa-picture-o" aria-hidden="true"></i> {{ $template->placeholders }}
										</span>
										<span class="item" title="last update">
											<i class="fa fa-pencil-square-o" aria-hidden="true"></i> @datetime($template->updated_at)
										</span>
										
									</div>
									@if($template->published == \App\Template::PUBLISH_TRUE)
									<div class="album-buttons">
										<span class="price">
											@if($template->discount > 0)
											<span class="discount-price badge badge-blue">{{ round($template->discount) . "% Off " }}</span>
											{{ $templatePrice . " " . $template->currency }}<br/>
											<span class="price-usd">@currency($templatePrice)</span>
											@else
											{{ $templatePrice . " " . $template->currency }}<br/>
											<span class="price-usd">@currency($templatePrice)</span>
											@endif
										</span>
										
										@buyTemplateBtn($template->id)
									</div>
									@else
									<div class="album-buttons">
										<span class="coming-soon">
											<i class="fa fa-clock-o" aria-hidden="true"></i> coming soon...
										</span>
									</div>
									@endif
								</div>
							</div>
						@else
							<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
								<div class="ft-template-box no-template">
									<div class="title">
										<span class="flaticon flaticon-brush-history"></span>&nbsp;stay tuned...
									</div>
									<div class="desc">
										We are working on bunch of new templates. They will be released soon. Stay tuned...
									</div>
								</div>
							</div>
						@endif
					@endfor
				</div>

				<div class="row">
					<div class="col-md-12 col-lg-12 text-right">
						<a class="btn ft-btn btn-sm show-all-btn" href="/photobook_templates">show all templates <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>