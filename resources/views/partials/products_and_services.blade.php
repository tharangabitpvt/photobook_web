<div class="ft-sub-section our-services-section ft-scrollify-section" data-section-name="our-services">
	<div class="container">
		<div class="ft-inner">
			<div class="desc">
				<h3>Products and services</h3>
				<p>Curretly we are providing PhotoBook application and Template designing services. Other services will be started soon...</p>
			</div>
			<div class="row">
				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
					<div class="ft-service-box">
						<div class="ft-service-icon"><img src="{{asset('/images/square_logo.png')}}"></div>
						<div class="ft-service-detail">
							<h4>PhotoBook</h4>
							<div>One stop album design solution. deliver your albums quickly. make your clients happy.</div>
							<div class="btn-pannel">
								<a class="btn btn-primary btn-sm" href="/downloads" title="downloads">Download now!</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
					<div class="ft-service-box">
						<div class="ft-service-icon"><div class="round-icon-lg themes-box"><i class="fa fa-paint-brush" aria-hidden="true"></i></div></div>
						<div class="ft-service-detail">
							<h4>Template designing</h4>
							<div>specially templates for your PhotoBook. Uniqueness guaranteed.</div>
							<div class="btn-pannel">
								<button class="btn btn-primary btn-sm contact-us-btn" data-contactfor="Template designing">contact us</button>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
					<div class="ft-service-box">
						<div class="ft-service-icon"><div class="round-icon-lg album-design-box"><i class="flaticon-brush-history" aria-hidden="true"></i></div></div>
						<div class="ft-service-detail">
							<h4>Album designing</h4>
							<div>just upload your photos and select a template. We will take care the rest. On time job completion.</div>
							<div class="btn-pannel">
								(coming soon...)&nbsp;&nbsp;
								<button class="btn btn-primary btn-sm contact-us-btn" data-contactfor="Album designing">contact us</button>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
					<div class="ft-service-box">
						<div class="ft-service-icon"><div class="round-icon-lg font-design-box"><i class="fa fa-font" aria-hidden="true"></i></div></div>
						<div class="ft-service-detail">
							<h4>Fonts designing</h4>
							<div>Artistic Sinhalese/Tamil fonts for you. Convert your hand writing into computer font.</div>
							<div class="btn-pannel">
								(coming soon...)&nbsp;&nbsp;
								<button class="btn btn-primary btn-sm contact-us-btn" data-contactfor="Fonts designing">contact us</button>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
					<div class="ft-service-box">
						<div class="ft-service-icon"><div class="round-icon-lg artwork-design-box"><i class="flaticon-pen" aria-hidden="true"></i></div></div>
						<div class="ft-service-detail">
							<h4>Artworks designing</h4>
							<div>We can blend tranditional art and modern designs together to produce amazing reslts.</div>
							<div class="btn-pannel">
								(coming soon...)&nbsp;&nbsp;
								<button class="btn btn-primary btn-sm contact-us-btn" data-contactfor="Artwork designing">contact us</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-lg-12 text-right">
					<a class="btn ft-btn btn-sm show-all-btn" href="{{ route('products_and_services') }}">Read more <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
				</div>					
			</div>
		</div>
	</div>
</div>