<div class="row">
	<div class="carousel">
		<div id="bb-bookblock" class="bb-bookblock">
			@foreach($items as $key => $item)
			<div class="bb-item">
				@if($item == 'no_image')
	      			<div class='no-thumb'><h3>Preview image not found!</h3></div>
	      		@else
	      			<img src="{{$item}}">
	      		@endif
			</div>
			@endforeach
		</div>
		<nav>
			<a id="bb-nav-first" href="js:void(0);" title="first page" class="bb-nav"><i class="fa fa-fast-backward" aria-hidden="true"></i></a>
			<a id="bb-nav-prev" href="js:void(0);" title="prev page (left arrow key)" class="bb-nav"><i class="fa fa-flip-horizontal fa-play" aria-hidden="true"></i></a>
			<a id="bb-nav-next" href="js:void(0);" title="next page (right arrow key)" class="bb-nav"><i class="fa fa-play" aria-hidden="true"></i></a>
			<a id="bb-nav-last" href="js:void(0);" title="last page" class="bb-nav"><i class="fa fa-fast-forward" aria-hidden="true"></i></a>
		</nav>
	</div>
</div>

<script>
	var Page = (function() {
		
		var config = {
				$bookBlock : $( '#bb-bookblock' ),
				$navNext : $( '#bb-nav-next' ),
				$navPrev : $( '#bb-nav-prev' ),
				$navFirst : $( '#bb-nav-first' ),
				$navLast : $( '#bb-nav-last' )
			},
			init = function() {
				config.$bookBlock.bookblock( {
					speed : 800,
					shadowSides : 0.8,
					shadowFlip : 0.7
				} );
				initEvents();
			},
			initEvents = function() {
				
				var $slides = config.$bookBlock.children();

				// add navigation events
				config.$navNext.on( 'click touchstart', function() {
					config.$bookBlock.bookblock( 'next' );
					return false;
				} );

				config.$navPrev.on( 'click touchstart', function() {
					config.$bookBlock.bookblock( 'prev' );
					return false;
				} );

				config.$navFirst.on( 'click touchstart', function() {
					config.$bookBlock.bookblock( 'first' );
					return false;
				} );

				config.$navLast.on( 'click touchstart', function() {
					config.$bookBlock.bookblock( 'last' );
					return false;
				} );
				
				// add swipe events
				$slides.on( {
					'swipeleft' : function( event ) {
						config.$bookBlock.bookblock( 'next' );
						return false;
					},
					'swiperight' : function( event ) {
						config.$bookBlock.bookblock( 'prev' );
						return false;
					}
				} );

				// add keyboard events
				$( document ).keydown( function(e) {
					var keyCode = e.keyCode || e.which,
						arrow = {
							left : 37,
							up : 38,
							right : 39,
							down : 40
						};

					switch (keyCode) {
						case arrow.left:
							config.$bookBlock.bookblock( 'prev' );
							break;
						case arrow.right:
							config.$bookBlock.bookblock( 'next' );
							break;
					}
				} );
			};

			return { init : init };

	})();
</script>
<script>
		Page.init();
</script>