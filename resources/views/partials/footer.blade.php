<div class="ft-scrollify-section footer" data-section-name="footer-section">
	<div class="container-fluide">
		<div class="col-md-3 col-lg-3 footer-social">
			<div class="fb-page" data-href="https://www.facebook.com/photobooklk" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/photobooklk" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/photobooklk">PhotoBook</a></blockquote></div>

			<ul class="footer-menu footer-contact">
				<li>
					<i class="fa fa-fw fa-volume-control-phone" aria-hidden="true"></i> {{ config('pbconfig.contactNo') }}
				</li>
				<li>
					<i class="fa fa-fw fa-envelope" aria-hidden="true"></i> {{ config('pbconfig.salesEmail') }}
				</li>
			</ul>
		</div>
		<div class="col-md-6 col-lg-6 footer-menu-items">
			<div class="col-md-6 col-lg-6">
				<ul class="footer-menu">
					<li><a href="/">Home</a></li>
					<li><a href="/about">About</a></li>
					<li><a href="/products_and_services">Products &amp; services</a></li>
					<li><a href="/photobook_templates">Templates</a></li>
					<li><a href="/downloads">Downloads</a></li>
					<li><a href="/contact-us">Contact us</a></li>
					<li><a class="ft-tc-link" href="js:void(0)">Terms &amp; conditions</a></li>
				</ul>

				<div class="copyright">&copy; photobook.lk <?php echo '2017 - ' . date('Y'); ?></div>
				<div class="copyright">Web site by <span style="color: #fff;">PhotoBook IT team</span></div>
			</div>
			<div class="col-md-6 col-lg-6">
				<div class="ft-buttons">
					<div class="platform-btn" title="download for Widows">
						<a href="/downloads"><i class="fa fa-windows fa-2x" aria-hidden="true"></i><br/>
							<span class="platform">Windows</span></a>
					</div>
					<div class="platform-btn" title="download for Mac">
						<a href="/downloads"><i class="fa fa-apple fa-2x" aria-hidden="true"></i><br/>
							<span class="platform">Mac</span></a>
					</div>

					<div class="platform-btn down-for-this-computer" title="download for this computer">
                        <i class="fa fa-arrow-circle-o-down fa-2x" aria-hidden="true"></i><br/>
                        <span class="ft-platform"></span>
                    </div>

                    <div class="platform-btn" title="User guide">
                        <a href="/" class="download-userguide"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i><br/><span class='platform'>Guide</span></a>
                    </div>
				</div>

				<form name="latest_app_download_form" id="latest_app_download_form" method="post" action="/download_trail">
					{{ csrf_field() }}
                    <input type="hidden" name="app_platform" id="app_platform" value="" />
                    <input type="hidden" name="app_arch" id="app_arch" value="" />
                    <input type="hidden" name="app_latest" id="app_latest" value="true" />
                    <input type="hidden" name="app_file_type" id="app_file_type" value="true" />
                </form>
			</div>
		</div>
		<div class="col-md-3 col-lg-3 footer-payment-options">
			<div class="row">
				<div class="col-mg-6 col-lg-6 col-sm-12 payment-option">
					<img src="{{ asset('/images/mcash.png') }}" />
				</div>
				<div class="col-mg-6 col-lg-6 col-sm-12 payment-option">
					<img src="{{ asset('/images/ez_cash.png') }}" />
				</div>
			</div>
			<div class="row">
				<div class="col-mg-6 col-lg-6 col-sm-12 payment-option">
					<i class="fa fa-3x fa-university" aria-hidden="true"></i><br/>Bank transfer
				</div>
				<div class="col-mg-6 col-lg-6 col-sm-12 payment-option">
					<i class="fa fa-3x fa-credit-card" aria-hidden="true"></i><br/>Credit card<br/><small>(will be available soon)</small>
				</div>
			</div>
		</div>
	</div>
</div>