<div class="ft-sub-section about-section ft-scrollify-section" data-section-name="about-photobook">
	<div class="container">
		<div class="ft-content">
			<div id="why_photobook" class="ft-inner">
				<h3>Why PhotoBook?</h3>
				<p>PhotoBook is an elegant solution for busy photographers and designers for delivering high quality photo albums to their customers with less effort. It consists of amazing pre-designed templates collection which always ready for produce phenomenal result. More templates are available for purchasing on our website photobook.lk.</p>
			</div>
			<div class="ft-inner">					
				<div class="row">
					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="ft-summary ft-summary-box-one">
							<div class="summary-icon"><i class="fa fa-paint-brush" aria-hidden="true"></i></div>
							<div>creativeness &amp; uniqueness</div>
							<div class="ft-summary-details">
								<ul>
									<li>We are always unique and creative.</li>
									<li>Your satisfaction is our asset.</li>
								</ul>
							</div>
						</div>							
					</div>

					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="ft-summary ft-summary-box-two">
							<div class="summary-icon"><i class="fa fa-hourglass-half" aria-hidden="true"></i></div>
							<div>less design time</div>
							<div class="ft-summary-details">
								<ul>
									<li>Album designing with PhotoBook is</li>
									<li>thousand times faster</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="ft-summary ft-summary-box-three">
							<div class="summary-icon"><i class="fa fa-magic" aria-hidden="true"></i></div>
							<div>intelligent</div>
							<div class="ft-summary-details">
								<ul>
									<li>Think less.</li>
									<li>PhotoBook will do the hard part.</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="ft-summary ft-summary-box-foure">
							<div class="summary-icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
							<div>frequent updates</div>
							<div class="ft-summary-details">
								<ul>
									<li>This place will never get old.</li>
									<li>New templates every month</li>
								</ul>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="ft-summary ft-summary-box-five">
							<div class="summary-icon"><i class="fa fa-money" aria-hidden="true"></i></div>
							<div>save money</div>
							<div class="ft-summary-details">
								<ul>
									<li>Time is money.</li>
									<li>It saves your time a lot.</li>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
						<div class="ft-summary ft-summary-box-six">
							<div class="summary-icon"><i class="fa fa-user-secret" aria-hidden="true"></i></div>
							<div>private template designs</div>
							<div class="ft-summary-details">
								<ul>
									<li>It's so private. So uniue.</li>
									<li>Specially for you.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>