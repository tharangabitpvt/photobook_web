@extends('layouts.layout')

@section('content')
	@macro('payment_status_labels', $status)
		@switch($status)
			@case(\App\Purchase::PAYMENT_STATUS_NOT_PAID)
				@php $badgeClass = 'badge-red' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_PAID)
				@php $badgeClass = 'badge-green' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_PENDING)
				@php $badgeClass = 'badge-blue' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_REJECTED)
				@php $badgeClass = 'badge-brown' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_CANCELED)
				@php $badgeClass = 'badge-gray' @endphp
				@break
		@endswitch

		<span class="label label-lg {{ $badgeClass }}">{{ \App\Purchase::paymentStatus[$status] }}</span>
	@endmacro

	@macro('app_key_status', $appkeyStatus)
		@switch($appkeyStatus)
			@case(\App\AppDownload::APP_ACTIVATED_FALSE)
				@php $badgeClass = 'badge-red' @endphp
				@break
			@case(\App\AppDownload::APP_ACTIVATED_TRUE)
				@php $badgeClass = 'badge-green' @endphp
				@break
		@endswitch

		<span class="label label-lg {{ $badgeClass }}">{{ \App\AppDownload::appKeyStatus[$appkeyStatus] }}</span>
	@endmacro

	<div class="container inner-page">
		<h3>Dashboard</h3>
		<div class="row">
			<div class="container-fluid">
				<ul class="nav nav-tabs" role="tablist" id="dashboard_tabs">
				    <li role="presentation" class="active"><a href="#purchases" aria-controls="purchases" role="tab" data-toggle="tab"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Purchase history</a></li>
				    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Profile</a></li>
			  	</ul>

			  	<div class="tab-content dashboard-tab-content">
				    <div role="tabpanel" class="tab-pane fade in active" id="purchases">
				    	<div class="col-md-7 col-lg-7">
							<h4>Purchase records</h4>
							<table class="purchase-records">
								<tr><th>#</th><th>Item</th><th>Type</th><th>Purchased on</th><th class="text-center">Status</th></tr>
								@php $itemCouter = ($purchases->perPage() * $purchases->currentPage()) - $purchases->perPage() + 1; @endphp
								@foreach($purchases as $k => $purchase)
									<tr class="bg_{{ $k%2 }}">
										<td>{{ $itemCouter++ }}</td>
										<td>{{ $purchase->getPurchaseItem() }}</td>
										<td>{{ \App\Purchase::itemTypeName[$purchase->purchase_type] }}</td>
										<td>@datetime($purchase->purchase_date)</td>
										<td class="text-center">{!! Html::payment_status_labels($purchase->payment_status) !!}</td>
									</tr>
								@endforeach
								<tr><td colspan="5">{{ $purchases->links() }}</td></tr>
							</table>

							<h4>PhotoBook App keys</h4>
							<table class="purchase-records">
								<tr><th>#</th><th>App key</th><th>App name</th><th class="text-center">Status</th><th>Activated on</th></tr>
								@php $appDownloadsItemCouter = ($appDownloads->perPage() * $appDownloads->currentPage()) - $appDownloads->perPage() + 1; @endphp
								@foreach($appDownloads as $k => $appDownload)
									<tr class="bg_{{ $k%2 }}">
										<td>{{ $appDownloadsItemCouter++ }}</td>
										<td>{{ $appDownload->app_key }}</td>
										<td>{{ $appDownload->app_label }}</td>
										<td class="text-center">{!! Html::app_key_status($appDownload->is_activated) !!}</td>
										<td>@datetime($appDownload->activated_date)</td>							
									</tr>
								@endforeach
								<tr><td colspan="4">{{ $appDownloads->links() }}</td></tr>
							</table>					
						</div>
						<div class="col-md-5 col-lg-5">
							{!! $purchaseChart->html() !!}
						</div>						
				    </div>
				    <div role="tabpanel" class="tab-pane fade" id="profile">				    	
						<div class="user-profile">
							<div class="container-fluid">
								<h4>Your profile</h4>				    	
								{!! Form::model($appUser, ['route' => ['user.update', $appUser->id]]) !!}
									{{ csrf_field() }}
									<div class="row">
										<div class="col-md-6 col-lg-6">
											<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
												{!! Form::label('name', 'Name') !!}
												{!! Form::text('name', $appUser->name, ['class' => 'form-control']) !!}
												@if ($errors->has('name'))
					                                <span class="error-block">
					                                    {{ $errors->first('name') }}
					                                </span>
					                            @endif
											</div>

											<div class="form-group">
												<label>Email </label><div>{{ $appUser->email }} <i class="fa fa-check-circle text-success" aria-hidden="true"></i></div> 
												{!! Form::hidden('email', $appUser->email); !!}
											</div>	

											<div class="form-group">
												{!! Form::label('phone', 'Phone') !!}
												<div class="row">
													<div class="col-md-4 col-lg-4">
														{!! Form::select('dial_code', \App\User::dialingCodesForDropdown(), $appUser->dial_code, ['class' => 'form-control ft-select2', 'id' => 'dial_code']) !!}
													</div>
													<div class="col-md-8 col-lg-8{{ $errors->has('phone') ? ' has-error' : '' }}">
														{!! Form::text('phone', $appUser->phone, ['class' => 'form-control']); !!}
														@if ($errors->has('phone'))
							                                <span class="error-block">
							                                    {{ $errors->first('phone') }}
							                                </span>
							                            @endif
													</div>
												</div>
											</div>

											<div class="form-group">
												{!! Form::label('business_name', 'Business name') !!}
												{!! Form::text('business_name', $appUser->business_name, ['class' => 'form-control']); !!}
											</div>

											<div class="form-group">
												{!! Form::label('business_address', 'Business address') !!}
												{!! Form::textarea('business_address', $appUser->business_address, ['class' => 'form-control', 'rows' => 3]); !!}
											</div>
								    	</div>
								    	<div class="col-md-6 col-lg-6">
								    		<div class="form-group{{ $errors->has('billing_address') ? ' has-error' : '' }}">
												{!! Form::label('billing_address', 'Billing address') !!}
												{!! Form::textarea('billing_address', $appUser->billing_address, ['class' => 'form-control', 'rows' => 3]); !!}
												@if ($errors->has('billing_address'))
					                                <span class="error-block">
					                                    {{ $errors->first('billing_address') }}
					                                </span>
					                            @endif
												<div class="hint">All official documents including invoices will be addressed to the billing address.</div>
											</div>

											<h5><b>NB:</b> If you want to change the password use following entries. Otherwise leave them blank.</h5>
											<div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
												{!! Form::label('current_password', 'Current password') !!}
												{!! Form::password('current_password', ['class' => 'form-control']); !!}
												@if ($errors->has('current_password'))
					                                <span class="error-block">
					                                    {{ $errors->first('current_password') }}
					                                </span>
					                            @endif
											</div>

											<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
												{!! Form::label('password', 'New password') !!}
												{!! Form::password('password', ['class' => 'form-control']); !!}
												@if ($errors->has('password'))
					                                <span class="error-block">
					                                    {{ $errors->first('password') }}
					                                </span>
					                            @endif
											</div>

											<div class="form-group">
												{!! Form::label('password_confirmation', 'Confirm password') !!}
												{!! Form::password('password_confirmation', ['class' => 'form-control']); !!}
											</div>

											<div class="form-group">
												{!! Form::submit('Submit', ['class' => 'btn btn-primary']); !!}
											</div>
								    	</div>
							    	</div>
								{!! Form::close() !!}
							</div>
						</div>
				    </div>
			  	</div>
			</div>
		</div>
	</div>

	{!! Charts::scripts() !!}
    {!! $purchaseChart->script() !!}

@endsection