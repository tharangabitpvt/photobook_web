@extends('layouts.layout')

@section('content')
<div class="container inner-page">
	<div class="row">
		<div class="col-md-6 col-lg-6">
			<h3>Contact us</h3>
			<h4>Read the instructions before submit the form</h4>
			<p>Select the <b>Product or service</b> that you want to get information about.</p>
			<p>Phone number would be the fast and more convenient way to communicate with you.</p>
			<p>If you provide wrong information, we won't be able to contact you.</p>
			<p>Please fill the form using valid details of yours and click on <b>send</b> button.</p>
			<p>We will get back to you as soon as possible.</p>

			<div class="conactus-via-phone">
				<h3>feel free to give us a call</h3>
				<div class="phone">{{ config('pbconfig.contactNo') }}</div>
			</div>
		</div>
		<div class="col-md-6 col-lg-6">
			@include('partials.contact_us_form')	
		</div>
	</div>
</div>
@endsection