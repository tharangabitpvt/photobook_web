@extends('layouts.layout')

@section('content')
	<div class="container inner-page">
		<h3>About PhotoBook</h3>
		<p>
			PhotoBook is a software/hardware/creative-designing service provider. The company is founded in late 2017. We aim to provide professional solutions for problems photographers and graphic designers face. As the main software product, we are introducing PhotoBook desktop application, which is a photography album designing software. PhotoBook.lk is developing and maintaining the application. As our main services we design album templates for PhotoBook application. Apart from that we are doing album designing as well. The company is looking forward to expand in many areas soon with new bunch of products and services.
		</p>

		<h4>PhotoBook application</h4>
		<p>PhotoBook is an elegant solution for busy photographers and designers for delivering high quality photo albums to their customers with less effort. It consists of amazing pre-designed templates collection which always ready for produce phenomenal results. More templates are available for purchasing on our website photobook.lk.</p>

		<h4>Why is PhotoBook special?</h4>
		<ul>
			<li>PhotoBook is specially designed for Sri Lankan photography market.</li> 
			<li>It contains uniquely designed templates suitable for Sinhalese/Tamil and Western photography events.</li>
			<li>Every template design is carefully designed by our dedicated and experienced team of designers.</li>
			<li>Our templates database is ceaselessly being updated with new templates as well as with the changes for existing templates.</li>
			<li>Every updates are reflected on PhotoBook application itself. Therefore you can easily update your PhotoBook connecting to the Internet at anytime when updates are available.</li>
			<li>We develop completely personal templates (on request) who is willing to use truly unique and private templates.</li>
			<li>PhotoBook application has continuous updates. Our software development team is always ready for delivering the best version on each release.</li>
		</ul>
	</div>
@endsection