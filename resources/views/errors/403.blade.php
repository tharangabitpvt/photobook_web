@extends('layouts.layout')

@section('content')
<div class="container-fluid error-page">
	<div><i class="fa fa-ban fa-3x text-danger" aria-hidden="true"></i></div>
	<h2>{{ $exception->getMessage() }}</h2>
	<a href="{{ route('home') }}">Go somewhere nice</a>
</div>
@endsection