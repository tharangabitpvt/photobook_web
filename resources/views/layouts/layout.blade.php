<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="PhotoBook is a templates based photography album designing software for professionals. It saves your time and money." />
	<meta name="keywords" content="wedding,album,storybook,graphic,design,photography,templates" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'PhotoBook') }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bookblock.css') }}">

    <script type="text/javascript" src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.scrollify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/platform.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/modernizr.custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.bookblock.js') }}"></script>
    
    {!! Charts::styles() !!}

    {!! NoCaptcha::renderJs() !!}

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
         });
    </script>
</head>
<body class='{{ $bodyClass or "default" }} ft-overflow-hidden'>
	<div id="page_preloader">
		<div id="loading_spinner" class="spinner">
		  	<div class="double-bounce1"></div>
		  	<div class="double-bounce2"></div>
		</div>
	</div>
	<div id='app'>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=140531546000404';
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		@include('partials.navbar')
		
		<div class="container-fluid ft-main-container">
			<div class="row">
				@include('flash::message')

				@yield('content')

				@if(Request::url() != "/")
					@include('partials.footer')
				@endif
			</div>
		</div>
	</div>

	@include('partials.template_preview')
	@include('partials.shoping_collection')
	@include('partials.tc')
	@include('partials.download_user_guides')

	<div class="today-currency-rates">
		1 USD = @currency(1, 'USD', 'LKR')
	</div>

	{{-- Session::flush() --}}
	<script>
		var shopColSession = "";
	</script>
	@if(Session::has('buy_collection'))
		<script>
			shopColSession = {!! json_encode(Session::get('buy_collection')) !!};
		</script>

	@endif

	<script>
	    //$('#flash-overlay-modal').modal();
	    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
	</script> 

	<!-- Preloader -->
	<script type="text/javascript">
	    //<![CDATA[
	        $(window).on('load', function() { // makes sure the whole site is loaded 
	            $('#loading_spinner').fadeOut(); // will first fade out the loading animation 
	            $('#page_preloader').delay(350).fadeOut(300);
	            $('body').delay(350).removeClass('ft-overflow-hidden');
	          })
	    //]]>
	</script>    

	<!-- My Live Chat -->
	<!--script type="text/javascript">function add_chatinline(){var hccid=52394734;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script -->
</body>
</html>