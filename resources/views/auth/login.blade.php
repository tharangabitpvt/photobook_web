@extends('layouts.layout')

@section('content')
<div class="container inner-page">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><i class="fa fa-sign-in" aria-hidden="true"></i> PhotoBook Login</h4></div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Enter your registered email address">

                                @if ($errors->has('email'))
                                    <span class="error-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Enter your password">

                                @if ($errors->has('password'))
                                    <span class="error-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="row">
                                    <div class="col-md-5 col-lg-5 col-sm-12">
                                        <button type="submit" class="btn ft-btn" style="width: 100%;">
                                            Login
                                        </button>
                                        <a class="btn btn-link" href="{{ route('password.request') }}" style="padding: 5px 0; font-size: 12px;">
                                            Forgot Your Password?
                                        </a>
                                    </div>
                                    <div class="col-md-7 col-lg-7 col-sm-12">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            </label>
                                        </div>                                    
                                    </div>
                                </div>                                
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="text-align: center;">                                
                                <hr style="margin: 10px 0;" />
                                Don't have a PhotoBook account yet?
                                <a class="btn ft-btn" href="{{ route('register') }}">Create your account</a> now!
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
