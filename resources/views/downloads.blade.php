@extends('layouts.layout')

@section('content')
	<div class="container inner-page">
		<h3>User guide</h3>
		<div class="raw">
			<a href="{{route('download_user_guide', ['lang' => 'sinhala'])}}" class="btn ft-btn" title="Sinhala"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> සිංහල</a>
			<a href="{{route('download_user_guide', ['lang' => 'en'])}}" class="btn ft-btn" title="English"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> English</a>
		</div>
		<br/>
		<h3>App Downloads</h3>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<h4><i class="fa fa-windows" aria-hidden="true"></i> Windows</h4>
				@foreach($photoBookAppsWin as $photoBookAppWin)
					<div class="download-item">
						<div class="download-item-desc">
							{{$photoBookAppWin->release_name}} {{$photoBookAppWin->version}} <span class="text-emp">{{$photoBookAppWin->architecture}}bit</span> ({{App\PhotobookApp::appStableLevel[$photoBookAppWin->stable_level]}}) - @datetime($photoBookAppWin->create_date)
						</div>
						<div class="download-item-actions">
							<button class="btn btn-primary btn-sm app-download-btn" title="download" data-download-itemid="{{$photoBookAppWin->id}}">
								<i class="fa fa-download" aria-hidden="true"></i>
							</button>
							<a href="#change_log_{{$photoBookAppWin->id}}" class="btn btn-default btn-sm" title="change-log">
								<i class="fa fa-file-o" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				@endforeach
			</div>
			<div class="col-md-6 col-lg-6">
				<h4><i class="fa fa-apple" aria-hidden="true"></i>  Mac OS</h4>
				@foreach($photoBookAppsMac as $photoBookAppMac)
					<div class="download-item">
						<div class="download-item-desc">
							{{$photoBookAppMac->release_name}} {{$photoBookAppMac->version}}  ({{App\PhotobookApp::appStableLevel[$photoBookAppMac->stable_level]}}) - @datetime($photoBookAppMac->create_date)
						</div>
						<div class="download-item-actions">
							<button class="btn btn-primary btn-sm app-download-btn" title="download" data-download-itemid="{{$photoBookAppMac->id}}">
								<i class="fa fa-download" aria-hidden="true"></i></button>
							<a href="#change_log_{{$photoBookAppMac->id}}" class="btn btn-default btn-sm" title="change-log">
								<i class="fa fa-file-o" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>

		<div class="row">
			<div class="container-fluid change-logs">				
				<div class="col-md-6 col-lg-6">		
					<h4><i class="fa fa-windows" aria-hidden="true"></i> Changelogs</h4>			
					@foreach($photoBookAppsWin as $photoBookAppWin)
						<div id="change_log_{{$photoBookAppWin->id}}" class="change-log">
							<h5>{{$photoBookAppWin->release_name}} {{$photoBookAppWin->version}} <span class="badge">{{App\PhotobookApp::appStableLevel[$photoBookAppWin->stable_level]}}</span></h5>
							<p>{{$photoBookAppWin->change_log}}</p>
						</div>
					@endforeach
				</div>
				<div class="col-md-6 col-lg-6">
					<h4><i class="fa fa-apple" aria-hidden="true"></i> Changelogs</h4>
					@foreach($photoBookAppsMac as $photoBookAppMac)
						<div id="change_log_{{$photoBookAppMac->id}}" class="change-log">
							<h5>{{$photoBookAppMac->release_name}} {{$photoBookAppMac->version}} <span class="badge">{{App\PhotobookApp::appStableLevel[$photoBookAppMac->stable_level]}}</span></h5>
							<p>{{$photoBookAppMac->change_log}}</p>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<form name="app_download_form" id="app_download_form" method="post" action="/download_trail">
		{{ csrf_field() }}
        <input type="hidden" name="installer_id" id="installer_id" value="0" />
    </form>
@endsection