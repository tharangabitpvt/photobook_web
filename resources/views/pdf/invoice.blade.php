@php $isHtml = false; @endphp
@if(isset($html) && $html)
	@php $isHtml = true; @endphp
@endif
<!DOCTYPE html>
<html>
<head>
	@if($isHtml)
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/sweetalert2.min.css') }}">

	<script type="text/javascript" src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/admin/script.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/js/sweetalert2.min.js') }}"></script>
	@endif
	<style type="text/css">
		body {
			font-family: 'sans-serif';
			color: #333;
			background: url(/images/pb_watermark.png) no-repeat center center;
			font-size: 14px;
		}

		h3, h4 {
			margin: 5px 0;
		}

		hr {
			border-color: #eee;
			height: 1px;
		}

		.container {
			width: 100%;
		}

		table.outer-table {
			width: 94%;
			border: none;
		}

		table.outer-table th, table.outer-table td {
			text-align: left;
			vertical-align: top;
		}

		.pblogo {
			height: 150px;
			background: url(/images/square_logo.jpg) no-repeat left top;
		}

		.odd-bg {
			background: rgba(221, 221, 221, 0.6); /* #eee */
		}

		.even-bg {
			background:  rgba(238, 238, 238, 0.6);/* #f5f5f5; */
		}

		table.checkout-table {
			width: 100%;
			text-align: right;
			border: 0;
		}

		table.checkout-table th, table.checkout-table td {
			text-align: right;
			padding: 5px 10px;
		}

		.price-usd {
			font-size: 12px;
		}

		.total-price {
			border-top: 1px solid #333;
			border-bottom: 4px double #333;
			font-size: 18px;
		}

		.pament-options {
			margin-top: 20px;
			font-size: 12px;
		}

		.pament-options h4 {
			margin: 5px 0;
		}

		.pament-options hr {
			margin-bottom: 5px 0;
		}

		.payment-option {
			display: inline-block;
			padding: 5px 10px;
		}

		.payment-option:first-child {
			padding-left: 0;
		}

		.payment-option:last-child {
			padding-right: 0;
		}

		.paystatus {
			width: 100%;
			padding: 20px;
			border: 3px dashed #fff;
			border-radius: 10px;
			color: #fff;
			text-align: center;
		}

		.paystatus-not-paid {
			background: url(/images/unpaid_corner.png) no-repeat top right;
		}

		.paystatus-paid {
			background: url(/images/paid_corner.png) no-repeat top right;
		}

		.paystatus-pending {
			background: url(/images/pending_corner.png) no-repeat top right;
		}

		.paystatus-rejected {
			background: url(/images/rejected_corner.png) no-repeat top right;
		}

		.paystatus-canceled {
			background: url(/images/canceled_corner.png) no-repeat top right;
		}

		.invoice-to {
			font-size: 14px;
		}

		.text-right {
			text-align: right;
		}

		.invoice-footer {
			font-size: 12px;
			padding-top: 20px;
		}

		.col-50 {
			width: 49%;
			display: inline-block;
		}
	</style>
</head>
<body>
	@php $pay_status_class = 'paystatus-not-paid'; @endphp
	@if($paystatus == \App\Purchase::PAYMENT_STATUS_PENDING)
		@php $pay_status_class = 'paystatus-pending'; @endphp
	@elseif($paystatus == \App\Purchase::PAYMENT_STATUS_PAID)
		@php $pay_status_class = 'paystatus-paid'; @endphp
	@elseif($paystatus == \App\Purchase::PAYMENT_STATUS_REJECTED)
		@php $pay_status_class = 'paystatus-rejected'; @endphp
	@elseif($paystatus == \App\Purchase::PAYMENT_STATUS_CANCELED)
		@php $pay_status_class = 'paystatus-canceled'; @endphp
	@endif

	@if($isHtml)
		<div style="width: 900px; margin: 20px auto;">
			<div class="inline-buttons">
				@if($paystatus != \App\Purchase::PAYMENT_STATUS_PAID)
				<h3 class="text-center"><span class="label label-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> IMPORTANT! This action cannot be undone!. Please use it very carefully.</span></h3>
				<div style="display: inline-block; margin-top: 10px;">
					<div class="dropdown">
					  	<button class="btn ft-btn btn-sm dropdown-toggle" type="button" id="change_payment_status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    Set payment status
			    			<span class="caret"></span>
					  	</button>
					  	<ul id="change_payment_status_dropdown" class="dropdown-menu" aria-labelledby="change_payment_status">
						    <li>
						    	<a href="#" data-payment-status="{{ \App\Purchase::PAYMENT_STATUS_PAID }}"><i class="fa fa-fw fa-check text-success" aria-hidden="true"></i> Paid</a></li>
						    <li>
						    	<a href="#" data-payment-status="{{ \App\Purchase::PAYMENT_STATUS_NOT_PAID }}"><i class="fa fa-fw fa-minus-circle text-danger" aria-hidden="true"></i> Unpaid</a>
						    </li>
						    <li role="separator" class="divider"></li>
						    <li><a href="#" data-payment-status="{{ \App\Purchase::PAYMENT_STATUS_CANCELED }}"><i class="fa fa-fw fa-times text-warning" aria-hidden="true"></i> Canceled</a></li>
						    <li><a href="#" data-payment-status="{{ \App\Purchase::PAYMENT_STATUS_REJECTED }}"><i class="fa fa-fw fa-ban text-info" aria-hidden="true"></i> Rejected</a></li>
					  	</ul>
					</div>
				</div>
				@endif
				<div style="display: inline-block;">
					<a href="{{ route('admin.purchases') }}" class="btn btn-default btn-sm"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
				</div>
			</div>
		</div>
	@endif

	<div class="container {{ $pay_status_class }}"@if($isHtml) style="width: 900px; margin: 20px auto;" @endif>
		<table class="outer-table">
			<tr class="header">
				<td width="30%">
					<div class="pblogo"></div>
				</td>
				<td>
					<h3>PhotoBook.lk INVOICE</h3>
					<div class="pament-options">
						@php $banks = config('pbconfig.bankDetails'); @endphp
						@foreach($banks as $key => $bankDetail)
								<div class="col-50">
									<div><h4>{{$bankDetail['bank']}}</h4></div>
									<div><label>Account name:</label> {{$bankDetail['accountName']}}</div>
									<div><label>Account number:</label> {{$bankDetail['accountNumber']}}</div>
									<div><label>Branch:</label> {{$bankDetail['branch']}}</div>
									@if($bankDetail['SWIFT'] != "")
										<div><label>SWIFT code:</label> {{$bankDetail['SWIFT']}}</div>
									@endif

									@if($bankDetail['additionalInfo'] != "")
										<div>{{$bankDetail['additionalInfo']}}</div>
									@endif
								</div>
							
						@endforeach
						<!--hr/>
						<div>
							<div class="payment-option">
								<h4>mCash number</h4> 
								{{config('pbconfig.mCashNumber')}}
							</div>
							<div class="payment-option">
								<h4>ezCash number</h4> 
								{{config('pbconfig.ezCashNumber')}}
							</div>
						</div-->
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="invoice-to">
						<b>Invoice to: </b><br/>
						{{ $userName }},<br/>
						{{ $billingAddress }}<br/>
						{{ $country }}
					</div><br/>
					<div><b>Invoice date and time: </b>{{ $invoiceDate }}</div>
					<div><b>Invoice No: </b>{{ $invoiceNo }}</div>
					<div><b>Payment status: </b>{{ \App\Purchase::paymentStatus[$paystatus] }}</div>
					
					<hr />
					<table class="checkout-table">
						<tr>
							<th style="text-align: left;" width="60%">Item</th><th>Price</th>
						</tr>
						@foreach($shoppingItems as $key => $shoppingItem)
							@php $bgClass = 'odd-bg'; @endphp
							@if($key%2 == 0)
								@php $bgClass = 'even-bg'; @endphp
							@endif
							<tr class="{{$bgClass}}">
								<td style="text-align: left;">{{$shoppingItem['name']}} {{ $shoppingItem['itemTypeName'] ? '(' . $shoppingItem['itemTypeName'] . ')' : ''}}</td>
								<td class="text-right">{{$shoppingItem['price']}} {{$shoppingItem['currency']}} <span class="price-usd">(dis -{{ $shoppingItem['discount'] }}%)</span></td>
							</tr>
						@endforeach
						<tr class="text-right">
							<td>Tax</td>
							<td>{{$vat}} %</td>
						</tr>
						<tr class="text-right">
							<td><b>Total</b></td>
							<td class="total-price">{{$total . ' ' . $currency}}</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div style="margin: 10px 0"><b>NB: </b>If you have purchased a PhotoBook activation key, we will send you the key in a separate email once your payment is confirmed.</div>
	</div>
	
	<div class="invoice-footer"@if($isHtml) style="width: 900px; margin: 20px auto;" @endif>
		<hr/>
		This invoice is a computer generated document. No signature required. <br/>
		Invoice time: {{ $invoiceDate }} (timezone: Asia/Colombo)<br/>
		If you have any inquiry about this document please contact PhotoBook.lk	{{ config('pbconfig.contactNo') }},  {{ config('pbconfig.salesEmail') }} <br/>
		[currency rate: 1 USD = @currency(1, 'USD', 'LKR')]
	</div>

	@if($isHtml)
		{!! Form::open(['route' => ['admin.purchases.set_payment_status'], 'method' => 'post', 'name' => 'set_payment_method_form', 'id' => 'set_payment_method_form']) !!}
				{!! Form::hidden('invoice_no', $invoiceNo, ['id' => 'invoice_no']) !!}
				{!! Form::hidden('payment_status', '', ['id' => 'payment_status']) !!}
		{!! Form::close() !!}
	@endif
</body>
</html>