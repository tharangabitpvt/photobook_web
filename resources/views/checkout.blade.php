@extends('layouts.layout')

@section('content')
	<div class="container inner-page">
		<h3>Checkout</h3>

		<div class="row">
			<div class="container-fluid">
				<p>Please select your payment option and complete the purchase.</p>
				<p>If you haven't decided what the payment method is, select 'Inform later' option. Then you can pay using whatever payment method as you wish and then inform us the payment details as soon as possible.</p>

				<div class="row">
					<form name="checkout_form" id="checkout_form" action="/purchase/complete_purchase" method="post">
						{{ csrf_field() }}
						<div class="col-md-6 col-lg-6">
							<h4>Payment options</h4>
							<div class="payment-options">
								<div class="row">
									<div class="col-md-4 col-lg-4 col-sm-6">
										<div class="item bank-payment">
											<div class="payment-icon">
												<i class="fa fa-bank fa-3x"></i>
											</div>
											<div class="payment-name">
												<input type="radio" class="payment-type-radio" name="payment_type" value="{{App\Purchase::PAYMENT_METHOD_BANK}}" /> Bank transfer
											</div>
										</div>

										<div class="not-available">
											<div class="text">currently not available.</div>
										</div>
									</div>

									<div class="col-md-4 col-lg-4 col-sm-6">
										<div class="item mcash-payment na">
											<div class="payment-icon">
												<img src="{{asset('/images/mcash.png')}}" />
											</div>
											<div class="payment-name">
												<input type="radio" class="payment-type-radio" name="payment_type" value="{{App\Purchase::PAYMENT_METHOD_MCASH}}" /> mCash
											</div>

											<div class="not-available">
												<div class="text">currently not available.</div>
											</div>
										</div>
									</div>

									<div class="col-md-4 col-lg-4 col-sm-6">
										<div class="item ezcash-payment na">
											<div class="payment-icon">
												<img src="{{asset('/images/ez_cash.png')}}" />
											</div>
											<div class="payment-name"><input type="radio" class="payment-type-radio" name="payment_type" value="{{App\Purchase::PAYMENT_METHOD_EZCASH}}" /> ez Cash</div>

											<div class="not-available">
												<div class="text">currently not available.</div>
											</div>
										</div>
									</div>

									<div class="col-md-4 col-lg-4 col-sm-6">
										<div class="item creditcard-payment na">
											<div class="payment-icon">
												<i class="fa fa-credit-card fa-3x"></i>
											</div>
											<div class="payment-name"><input type="radio" class="payment-type-radio" name="payment_type" value="{{App\Purchase::PAYMENT_METHOD_CREDIT_CARD}}" /> Credit card</div>

											<div class="not-available">
												<div class="text">currently not available.</div>
											</div>
										</div>
									</div>

									<div class="col-md-4 col-lg-4 col-sm-6">
										<div class="item inform-later-payment">
											<div class="payment-icon">
												<i class="fa fa-comment-o fa-3x"></i>
											</div>
											<div class="payment-name"><input type="radio" class="payment-type-radio" name="payment_type" value="{{App\Purchase::PAYMENT_METHOD_INFORM_LATER}}" checked="checked" /> Inform later</div>

											<div class="not-available">
												<div class="text">currently not available.</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-lg-6">
							<table class="checkout-table">
								<tr>
									<th style="text-align: left;" width="60%">Item</th><th>Price</th>
								</tr>
								@foreach($shoppingItems as $key => $shoppingItem)
									@php $bgClass = 'odd-bg'; @endphp
									@if($key%2 == 0)
										@php $bgClass = 'even-bg'; @endphp
									@endif
									<tr class="{{$bgClass}}">
										<td style="text-align: left;">{{$shoppingItem['name']}} {{ $shoppingItem['itemTypeName'] != "" ? '(' . $shoppingItem['itemTypeName'] .')' : '' }}</td>
										<td>{{$shoppingItem['price']}} {{$shoppingItem['currency']}}</td>
									</tr>
								@endforeach
								<tr>
									<td><b>Total</b></td>
									<td class="total-price">{{$total}} {{$shoppingItem['currency']}}</td>
								</tr>
							</table>
							<br/>
							<div><b>NOTES:</b><br/>We DO NOT refund under any circumstances.<br/>By purchasing with photobook.lk you are agreed to our <a class="ft-tc-link" href="js:void(0)">Terms &amp; conditions</a></div>
							<br/>
							<button id="purchase_complete_btn" class="btn btn-primary pull-right">Agreed &amp; Complete purchase</button>
						</div>
					</form>
				</div>

				<h3>Payment source details</h3>
				<p>
					<h4>Bank details</h4>
					<div class="row">
					@php $banks = config('pbconfig.bankDetails'); @endphp
					@foreach($banks as $key => $bankDetail)
						<div class="col-md-4 col-lg-4">
							<div><h4><img src="{{asset($bankDetail['logo'])}}" height="42" /> {{$bankDetail['bank']}}</h4></div>
							<div><label>Account name:</label> {{$bankDetail['accountName']}}</div>
							<div><label>Account number:</label> {{$bankDetail['accountNumber']}}</div>
							<div><label>Branch:</label> {{$bankDetail['branch']}}</div>
							@if($bankDetail['SWIFT'] != "")
								<div><label>SWIFT code:</label> {{$bankDetail['SWIFT']}}</div>
							@endif

							@if($bankDetail['additionalInfo'] != "")
								<div>{{$bankDetail['additionalInfo']}}</div>
							@endif
						</div>
					@endforeach
					</div>
				</p>
				<hr/>
				<p>
					<h4>mCash number</h4> 
					{{config('pbconfig.mCashNumber')}}
				</p>
				<hr/>
				<p>
					<h4>ezCash number</h4> 
					{{config('pbconfig.ezCashNumber')}}
				</p>
			</div>
		</div>
	</div>
@endsection