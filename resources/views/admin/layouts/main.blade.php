<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'PhotoBook') }}</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.scrollbar.css') }}">

    <script type="text/javascript" src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js/admin/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery.scrollbar.min.js') }}"></script>
    {!! Charts::styles() !!}

    <script type="text/javascript">
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
         });
    </script>
</head>
<body>
	<div id='app'>		

        <div class="sidebar scrollbar-macosx">
            @include('admin.partials.sidebar')
        </div>
        <div class="admin-content">
            <div class="container-fluid">
                @include('flash::message')

                @yield('content')
            </div>
        </div>				

	</div>

	<script>
	    //$('#flash-overlay-modal').modal();
	    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
	</script>     
</body>
</html>