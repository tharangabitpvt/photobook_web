@extends('admin.layouts.main')

@section('content')
	<div class="row admin-content-header">
		<div class="col-md-7 col-lg-7">
			<h3>User manager</h3>
		</div>
		<div class="col-md-5 col-lg-5">
			<div class="ft-buttons">
				<a href="{{ route('admin.create_user') }}" class="btn ft-btn btn-sm"><i class="fa fa-user-plus" aria-hidden="true"></i> Create</a>
				<a href="{{ route('admin.user_manager') }}" class="btn ft-btn btn-sm">Reset filter</a>
				<button class="btn ft-btn btn-sm" id="trash_user_btn"><i class="fa fa-trash" aria-hidden="true"></i> Trash</button>
			</div>
		</div>
	</div>
	<hr/>
	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
		{
			$checked = $isChecked ? 'checked="checked"' : '';
			$htmlOptions = '';
			foreach($options as $k => $option) {
				$htmlOptions .= $k . "='" . $option . "' ";
			}

		    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
		}); @endphp

		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="btn-group btn-group-sm ft-button-group" role="group" aria-label="...">
					<a class="btn btn-default active" href="{{ route('admin.user_manager') }}"><i class="fa fa-user" aria-hidden="true"></i> Users</a>
					<a class="btn btn-default" href="{{ route('admin.user_manager.trashed') }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Trashed</a>
				</div>
			</div>
			<div class="col-md-6 col-lg-6">
				<div class="pagination-row-count text-right">
					{{ $users->count() . ' of ' . $users->total() }} records
				</div>
			</div>
		</div>

		{!! Form::open(['route' => 'admin.user_manager', 'method' => 'get', 'name' => 'grid_filter_form', 'id' => 'grid_filter_form']) !!}
		<table class="grid-table">
			<tr>
				<th class="text-center">{!! Form::myCheckbox('check_all_grid_items', '1', false, ['id' => 'check_all_grid_items']) !!}</th>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>User type</th>
				<th>Business name</th>
				<th>Created at</th>
				<th>Is active</th>
				<th></th>
			</tr>

			<tr>
				<td></td>
				<td>
					{!! Form::text('filter_name', '', ['class' => 'form-control input-sm filter-input']) !!}
				</td>
				<td>
					{!! Form::text('filter_email', '', ['class' => 'form-control input-sm filter-input']) !!}
				</td>
				<td>
					{!! Form::text('filter_phone', '', ['class' => 'form-control input-sm filter-input']) !!}
				</td>
				<td>
					{!! Form::select('filter_user_type', ['' => 'All'] + \App\User::userType, '', ['class' => 'form-control input-sm filter-input-select']) !!}
				</td>
				<td>
					{!! Form::text('filter_business_name', '', ['class' => 'form-control input-sm filter-input']) !!}
				</td>
				<td></td>
				<td>
					{!! Form::select('filter_is_active', ['' => 'All'] + \App\User::activeState, '', ['class' => 'form-control input-sm filter-input-select']) !!}
				</td>
				<td></td>
			</tr>

			@if(count($users) == 0)
			<tr class="no-data"><td colspan="8">No records found!</td></tr>
			@else
				@foreach($users as $user)
					<tr class="grid-row">
						<td>{!! Form::myCheckbox('user_'.$user->id, $user->id, false, ['class' => 'check-grid-item']) !!}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td class="text-center">{{ $user->phone ? $user->dial_code . $user->phone : 'n/a' }}</td>
						<td>{{ \App\User::userType[$user->user_type] }}</td>
						<td>{{ $user->business_name }}</td>
						<td>@datetime($user->created_at)</td>
						<td class="text-center">@statusIcon($user->is_active)</td>
						<td class="text-center">
							<a class="btn btn-primary btn-xs" href="{{ route('admin.edit_user', ['id' => $user->id]) }}" title="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						</td>
					</tr>
				@endforeach
			@endif
		</table>
		{{ $users->links() }}
		
		{!! Form::close() !!}

		{!! Form::open(['route' => ['admin.user.delete'], 'method' => 'delete', 'name' => 'delete_user_form', 'id' => 'delete_user_form']) !!}
  			{!! Form::hidden('grid_item_ids', '', ['id' => 'grid_item_ids']) !!}
  		{!! Form::close() !!}

@endsection