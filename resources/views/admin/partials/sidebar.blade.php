<div class="sidebar-inner">
	<div class="sidebar-header">
		<div class="content text-center">
			<h4 class="ft-truncate">{{ substr(@auth::user()->name, 0, 1) }}</h4>
				<div class="sidebar-item">
					<a class="btn btn-default" href="admin/profile/{{ @auth::user()->id }}"><i class="fa fa-user-circle-o fa-fw" aria-hidden="true"></i></a>
				</div>
				<div class="sidebar-item">
					<a class="btn btn-default" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
				</div>
		</div>
	</div>
	<div class="sidebar-body">
		<div class="content">
			<ul class="menu-items">
				<li class="{{ Request::is('admin') ? 'active' : '' }}" data-toggle="popover" data-placement="right" data-content="Dashboard">
					<a href="/admin">
						<i class="fa fa-home fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				<li class="{{ Request::is('admin/messages*') ? 'active' : '' }}" title="Inbox">
					<span class="badge {{ \App\Message::unreadMessages() > 0 ? 'badge-red' : '' }}">{{ \App\Message::unreadMessages() }}</span>
					<a href="/admin/messages">
						<i class="fa fa-inbox fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				@if(auth::user()->isAdmin())
				<li class="{{ Request::is('admin/user_manager*') ? 'active' : '' }}" title="Users">
					<a href="{{ route('admin.user_manager') }}">
						<i class="fa fa-user fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				@endif
				<li class="{{ Request::is('admin/apps_manager*') ? 'active' : '' }}" title="PhotoBook Apps">
					<a href="/admin/apps_manager">
						<i class="fa fa-desktop fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				<li class="{{ Request::is('admin/templates*') ? 'active' : '' }}" title="Templates">
					<a href="{{ route('admin.templates') }}">
						<i class="fa fa-paint-brush fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				<li class="{{ Request::is('admin/jobs_manager*') ? 'active' : '' }}" title="Jobs">
					<a href="/admin/jobs_manager">
						<i class="fa fa-briefcase fa-fw" aria-hidden="true"></i>
					</a>
				</li>

				@if(auth::user()->isAdmin())
				<li class="{{ Request::is('admin/purchases*') ? 'active' : '' }}" title="Purchases">
					<a href="{{ route('admin.purchases') }}">
						<i class="fa fa-shopping-bag fa-fw" aria-hidden="true"></i>
					</a>
				</li>
				<li class="{{ Request::is('admin/app_downloads*') ? 'active' : '' }}" title="App downloads">
					<a href="{{ route('admin.app_downloads') }}">
						<i class="ftf-square_logo" aria-hidden="true"></i>
					</a>
				</li>
				@endif
				
				<li class="{{ Request::is('admin/tickets*') ? 'active' : '' }}" title="Tickets">
					<a href="/admin/tickets">
						<i class="fa fa-bug fa-fw" aria-hidden="true"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>