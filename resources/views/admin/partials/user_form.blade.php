@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); 
@endphp

{!! Form::open(['route' => ['admin.create_user'], 'method' => 'post', 'id' => 'user_form']) !!}
	<div class="col-md-6 col-lg-6">
		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			{!! Form::label('name', 'Name') !!}
			{!! Form::text('name', '', ['class' => 'form-control', 'id' => 'name']) !!}

			@if ($errors->has('name'))
                <span class="error-block">
                    {{ $errors->first('name') }}
                </span>
            @endif	
		</div>
		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			{!! Form::label('email', 'Email') !!}
			{!! Form::email('email', '', ['class' => 'form-control', 'id' => 'email']) !!}

			@if ($errors->has('name'))
                <span class="error-block">
                    {{ $errors->first('name') }}
                </span>
            @endif
		</div>
		<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			{!! Form::label('password', 'Password') !!}
			{!! Form::password('password', ['class' => 'form-control']) !!}

			@if ($errors->has('password'))
                <span class="error-block">
                    {{ $errors->first('password') }}
                </span>
            @endif
		</div>
		<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
			{!! Form::label('password-confirm', 'Confirm password') !!}
			{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}

			@if ($errors->has('password_confirmation'))
                <span class="error-block">
                    {{ $errors->first('password_confirmation') }}
                </span>
            @endif
		</div>
		<div class="form-group">
			{!! Form::label('phone', 'Phone') !!}
			<div class="row">
				<div class="col-md-5 col-lg-5">
					{!! Form::select('dial_code', \App\User::dialingCodesForDropdown(), '+94', ['id' => 'dial_code', 'class' => 'form-control ft-select2']) !!}
				</div>
				<div class="col-md-7 col-lg-7{{ $errors->has('phone') ? ' has-error' : '' }}">		
					{{ Form::text('phone', '', ['class' => 'form-control', 'placeholder' => 'mobile number']) }}

					@if ($errors->has('phone'))
	                    <span class="error-block">
	                        {{ $errors->first('phone') }}
	                    </span>
	                @endif	
				</div>
			</div>
		</div>
		<div class="form-group">
			{!! Form::label('user_type', 'User type') !!}
			{!! Form::select('user_type', \App\User::userType, \App\User::USER_TYPE_PHOTOBOOK_STAFF, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('is_active', 'Active') !!}
			{!! Form::select('is_active',\App\User::activeState,  \App\User::USER_ACTIVE, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Submit', ['class' => 'btn ft-btn', 'id' => 'save_user_btn']) !!}
		</div>
	</div>
	<div class="col-md-6 col-lg-6">

	</div>
	<script type="text/javascript">
		$('.ft-select2').select2({
			'theme': 'bootstrap'
		});
	</script>
{!! Form::close() !!}