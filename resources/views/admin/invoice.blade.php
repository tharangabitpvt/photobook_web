@extends('admin.layouts.main')

@section('content')
	@macro('payment_status_labels', $status)
		@switch($status)
			@case(\App\Purchase::PAYMENT_STATUS_NOT_PAID)
				@php $badgeClass = 'badge-red' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_PAID)
				@php $badgeClass = 'badge-green' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_PENDING)
				@php $badgeClass = 'badge-blue' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_REJECTED)
				@php $badgeClass = 'badge-brown' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_CANCELED)
				@php $badgeClass = 'badge-gray' @endphp
				@break
		@endswitch

		<span class="label label-lg {{ $badgeClass }}">{{ \App\Purchase::paymentStatus[$status] }}</span>
	@endmacro
	<div class="row admin-content-header">
		<div class="col-md-6 col-lg-6">
			<h3>Invoice <small>{{ $invoiceNumber }}</small></h3>
		</div>
		<div class="col-md-6 col-lg-6">
			<div class="ft-buttons">
				<div class="dropdown">
				  	<button class="btn ft-btn btn-sm dropdown-toggle" type="button" id="change_payment_status" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    Set payment status
		    			<span class="caret"></span>
				  	</button>
				  	<ul class="dropdown-menu" aria-labelledby="change_payment_status">
					    <li>
					    	<a href="#"><i class="fa fa-fw fa-check text-success" aria-hidden="true"></i> Paid</a></li>
					    <li>
					    	<a href="#"><i class="fa fa-fw fa-minus-circle text-danger" aria-hidden="true"></i> Unpaid</a>
					    </li>
					    <li role="separator" class="divider"></li>
					    <li><a href="#"><i class="fa fa-fw fa-times text-warning" aria-hidden="true"></i> Canceled</a></li>
					    <li><a href="#"><i class="fa fa-fw fa-ban text-info" aria-hidden="true"></i> Rejected</a></li>
				  	</ul>
				</div>
			</div>
		</div>
	</div>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); @endphp

	<div class="pagination-row-count text-right">
		{{ $purchases->count() . ' of ' . $purchases->total() }} records
	</div>
	{!! Form::open(['route' => ['admin.purchases'], 'method' => 'get', 'id' => 'grid_filter_form']) !!}
		<table class="grid-table">
	  		
	  		@foreach($invoiceItems as $invoiceItem)
	  			<tr>
	  				<td>{{ $invoiceItem['name'] }}</td><td>{{ $invoiceItem['price'] }}</td>
	  			</tr>
	  		@endforeach
	  		
	  	</table>

	{!! Form::open(['route' => ['admin.purchases.delete'], 'method' => 'post', 'name' => 'set_payment_method_form', 'id' => 'set_payment_method_form']) !!}
			{!! Form::hidden('invoice_no', {{ $invoiceNumber }}, ['id' => 'invoice_no']) !!}
			{!! Form::hidden('payment_status', '', ['id' => 'payment_status']) !!}
	{!! Form::close() !!}

@stop