@extends('admin.layouts.main')

@section('content')
	@macro('is_replied', $isReplied)
		<i class="fa fa-reply {{ $isReplied == \App\Message::REPLIED_TRUE ? 'text-success' : 'text-danger' }}"></i>
	@endmacro

	<div class="row admin-content-header">
		<div class="col-md-6 col-lg-6">
			<h3>Messages <span class="badge {{ $unreadMessagesCount > 0 ? 'badge-red' : '' }}">{{ $unreadMessagesCount }}</span></h3>
		</div>
		<div class="col-md-6 col-lg-6">
			<div class="ft-buttons">
				<button class="btn ft-btn btn-sm" id="msg_delete_btn"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
				<a href="{{ route('admin.messages') }}" class="btn ft-btn btn-sm">Reset filter</a>
			</div>
		</div>
	</div>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
		{
			$checked = $isChecked ? 'checked="checked"' : '';
			$htmlOptions = '';
			foreach($options as $k => $option) {
				$htmlOptions .= $k . "='" . $option . "' ";
			}

		    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
		}); @endphp
	
	<div class="pagination-row-count text-right">
		{{ $messages->count() . ' of ' . $messages->total() }} records
	</div>

	{!! Form::open(['route' => ['admin.messages'], 'method' => 'get', 'id' => 'grid_filter_form']) !!}	
	<table class="grid-table">
  		<tr>
  			<th class="text-center">{!! Form::myCheckbox('check_all_grid_items', '1', false, ['id' => 'check_all_grid_items']) !!}</th>
  			<th></th>
  			<th>Sender</th>
  			<th>Email</th>
  			<th>Phone</th>
  			<th>Product/Service</th>
  			<th>Send time</th>
  		</tr>
  		<tr>
  			<td></td>
  			<td></td>
  			<td>{!! Form::text('filter_sender_name', '', ['id' => 'filter_sender_name', 'class' => 'form-control input-sm filter-input']) !!}</td>
  			<td>{!! Form::text('filter_sender_email', '', ['id' => 'filter_sender_email', 'class' => 'form-control input-sm filter-input']) !!}</td>
  			<td>{!! Form::text('filter_sender_phone', '', ['id' => 'filter_sender_phone', 'class' => 'form-control input-sm filter-input']) !!}</td>
  			<td>{!! Form::select('filter_product', ['' => 'All'] + config('pbconfig.productAndServices'), '', ['id' => 'filter_product', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
  			<td></td>
  		</tr>
		@if(count($messages) > 0)		
			@foreach($messages as $message)
	  		<tr class="grid-row inbox-message {{ $message->is_new == \App\Message::NEW_MESSAGE_TRUE ? 'unread' : '' }}" data-message-id="{{ $message->id }}">
	  			<td>{!! Form::myCheckbox('msg_'.$message->id, $message->id, false, ['class' => 'check-grid-item']) !!}</td>
	  			<td>{!! Html::is_replied($message->is_replied) !!}</td>
	  			<td>{{ $message->f_name }} {{ $message->l_name }}</td>
	  			<td>{{ $message->email }}</td>
	  			<td>{{ $message->phone }}</td>
	  			<td><span class="badge">{{ $message->product }}</span></td>
	  			<td>@datetime($message->created_at)</td>
	  		</tr>
	  		@endforeach

		@else
			<tr class="no-data"><td colspan="8" >No messages found!</td></tr>
		@endif
	</table>
	{!! Form::close() !!}

	{{ $messages->links() }}
	

	{!! Form::open(['route' => ['admin.messages.delete'], 'method' => 'delete', 'name' => 'delete_msg_form', 'id' => 'delete_msg_form']) !!}
  		{!! Form::hidden('grid_item_ids', '', ['id' => 'grid_item_ids']) !!}
  	{!! Form::close() !!}

@endsection