@extends('admin.layouts.main')

@section('content')
	<h3>{{ $template->tem_name }} <small>version {{ $template->version }}</small></h3>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); 
	@endphp

	{!! Form::model($template, ['route' => ['admin.update_template', $template->id], 'method' => 'put', 'files' => true, 'id' => 'edit_template_form']) !!}
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group{{ $errors->has('tem_name') ? ' has-error' : '' }}">
					{!! Form::label('tem_name', 'Name') !!}
					{!! Form::text('tem_name', $template->tem_name, ['class' => 'form-control']) !!}

					@if ($errors->has('tem_name'))
		                <span class="error-block">
		                    {{ $errors->first('tem_name') }}
		                </span>
		            @endif
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-4 col-lg-4{{ $errors->has('version') ? ' has-error' : '' }}">
							{!! Form::label('version', 'Version') !!}
							{!! Form::text('version', $template->version, ['class' => 'form-control']) !!}
							
							@if ($errors->has('version'))
				                <span class="error-block">
				                    {{ $errors->first('version') }}
				                </span>
				            @endif
						</div>
						<div class="col-md-4 col-lg-4{{ $errors->has('pages') ? ' has-error' : '' }}">
							{!! Form::label('pages', 'Pages') !!}
							{!! Form::text('pages', $template->pages, ['class' => 'form-control']) !!}
							
							@if ($errors->has('pages'))
				                <span class="error-block">
				                    {{ $errors->first('pages') }}
				                </span>
				            @endif
						</div>
						<div class="col-md-6 col-lg-4{{ $errors->has('placeholders') ? ' has-error' : '' }}">
							{!! Form::label('placeholders', 'Placeholders') !!}
							{!! Form::text('placeholders', $template->placeholders, ['class' => 'form-control']) !!}
							
							@if ($errors->has('placeholders'))
				                <span class="error-block">
				                    {{ $errors->first('placeholders') }}
				                </span>
				            @endif
						</div>
					</div>					
				</div>

				<div class="form-group{{ $errors->has('sizes') ? ' has-error' : '' }}">
					{!! Form::label('sizes', 'Sizes') !!}&nbsp;&nbsp;<a href="javascript:void(0);" id="select_all_sizes" data-sizes="{{implode(',', config('pbconfig.albumSizes'))}}">[select all]</a>
					<select name="sizes[]" id="sizes" class="form-control" multiple="multiple">
						@php $sizesArray = explode(',', $template->sizes); @endphp
						@foreach(config('pbconfig.albumSizes') as $albumSize)
							<option value="{{ $albumSize }}"{{ in_array($albumSize, $sizesArray) ? ' selected=selected' : '' }}>{{ $albumSize }}</option>
						@endforeach
					</select>
					@if ($errors->has('sizes'))
		                <span class="error-block">
		                    {{ $errors->first('sizes') }}
		                </span>
		            @endif
				</div>

				<script type="text/javascript">
					$('#sizes').select2({
						theme: 'bootstrap',
						allowClear: true,
						multiple: true,
						tokenSeparators: [','],
					});
				</script>

				<div class="form-group">
					<div class="row">
						<div class="col-md-4 col-lg-4{{ $errors->has('price') ? ' has-error' : '' }}">
							{!! Form::label('price', 'Price') !!}
							{!! Form::text('price', $template->price, ['class' => 'form-control']) !!}

							@if ($errors->has('price'))
				                <span class="error-block">
				                    {{ $errors->first('price') }}
				                </span>
				            @endif
						</div>
						<div class="col-md-4 col-lg-4">
							{!! Form::label('currency', 'Currency') !!}
							{!! Form::select('currency', config('pbconfig.currency'), $template->currency, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-4 col-lg-4{{ $errors->has('discount') ? ' has-error' : '' }}">
							{!! Form::label('discount', 'Discounts') !!}
							<div class="input-group">
							 {!! Form::text('discount', $template->discount, ['class' => 'form-control', 'aria-describedby' => 'discount_addon']) !!}
							 <span class="input-group-addon" id="discount_addon">%</span>
							</div>
							

							@if ($errors->has('discount'))
				                <span class="error-block">
				                    {{ $errors->first('discount') }}
				                </span>
				            @endif
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							{!! Form::label('storage_drive', 'Storage drive') !!}
							{!! Form::select('storage_drive', config('pbconfig.drives'), $template->storage_drive, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6 col-lg-6">
							{!! Form::label('published', 'Published') !!}
							{!! Form::select('published', \App\Template::publishState, $template->published, ['class' => 'form-control']) !!}
						</div>
					</div>					
				</div>
			</div>
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					{!! Form::label('user_id', 'Ownership') !!}
					{!! Form::select('user_id', [0 => 'Public'] + \App\User::getAppUsers(), $template->user_id, ['class' => 'form-control', 'id' => 'user_id']) !!}	
					<div class="hint">Select relevant user if this is a private template.</div>				
				</div>

				<script type="text/javascript">
					$('#user_id').select2({
						theme: 'bootstrap'
					});
				</script>

				<div class="form-group{{ count($errors->get('thumbnails.*')) > 0 ? ' has-error' : '' }}">
					{!! Form::label('Thumbnail images (JPG only)') !!}
					@if(count($thumbs) > 0)
					<div style="margin-bottom: 10px;"><img src="{{ $thumbs[1] }}" width="400" /></div>
					@endif
					<div style="margin-bottom: 10px;">{{ count($thumbs) }} thumbnails uploaded.</div>
					{!! Form::file('thumbnails[]', ['id' => 'thumbnails', 'class' => 'form-control', 'multiple' => 'multiple']); !!}
					<div class="hint">File names must be in the format 1_thumb.jpg, 2_thumb.jpg, ...</div>

					@php $fileErrors = $errors->get('thumbnails.*'); @endphp
					@if (count($fileErrors) > 0)
		                <span class="error-block">
		                    {{ $errors->first('thumbnails.*') }}
		                </span>
		            @endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-lg-12 text-right">
				{!! Form::submit('Save', ['class' => 'btn ft-btn']) !!}
				<a href="{{ route('admin.templates') }}" class="btn btn-default">Cancel</a>
			</div>
		</div>
	{!! Form::close() !!}
@stop