@extends('admin.layouts.main')

@section('content')
	<div class="row admin-content-header">
		<div class="col-md-7 col-lg-7">
			<h3>{{ $user->name }}</h3>
		</div>
		<div class="col-md-5 col-lg-5">
			<div class="ft-buttons">
				<a class="btn ft-btn" href="{{ route('admin.user_manager') }}">Back</a>
			</div>
		</div>
	</div>
	<hr/>

	@include('admin.partials.user_edit_form')

@endsection