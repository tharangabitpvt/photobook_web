@extends('admin.layouts.main')

@section('content')
	<h3>Add new PhotoBook app</h3>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); 
	@endphp

	{!! Form::open(['route' => 'admin.create_photobook_app', 'method' => 'post', 'id' => 'create_app_form']) !!}
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group{{ $errors->has('release_name') ? ' has-error' : '' }}">
					{!! Form::label('release_name', 'Release name') !!}
					{!! Form::text('release_name', '', ['class' => 'form-control']) !!}
					@if ($errors->has('release_name'))
		                <span class="error-block">
		                    {{ $errors->first('release_name') }}
		                </span>
		            @endif	
				</div>
				
				<div class="form-group{{ $errors->has('version') ? ' has-error' : '' }}">
					{!! Form::label('version', 'Version') !!}
					{!! Form::text('version', '', ['class' => 'form-control']) !!}
					@if ($errors->has('version'))
		                <span class="error-block">
		                    {{ $errors->first('version') }}
		                </span>
		            @endif	
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							{!! Form::label('platform', 'Platform') !!}
							{!! Form::select('platform', \App\PhotobookApp::platforms, null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6 col-lg-6">
							{!! Form::label('architecture', 'Architecture') !!}
							{!! Form::select('architecture', \App\PhotobookApp::architectures, null, ['class' => 'form-control']) !!}
						</div>
					</div>
					
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							{!! Form::label('release', 'Release type') !!}
							{!! Form::select('release', \App\PhotobookApp::releaseType, \App\PhotobookApp::RELEASE_UPDATE, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6 col-lg-6">
							{!! Form::label('stable_level', 'Stability') !!}
							{!! Form::select('stable_level', \App\PhotobookApp::appStableLevel, \App\PhotobookApp::APP_STABLE_LEVEL_STABLE, ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							{!! Form::label('priority', 'Release priority (for updates)') !!}
							{!! Form::select('priority', \App\PhotobookApp::releasePriority, \App\PhotobookApp::UPDATE_PRIORITY_HIGH, ['class' => 'form-control']) !!}
							<div class="hint">This option is specially for app updates.</div>
						</div>
						<div class="col-md-6 col-lg-6">
							{!! Form::label('is_latest', 'Is latest release?') !!}
							{!! Form::select('is_latest', [0 => 'No', 1 => 'Yes'], 1, ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							{!! Form::label('published', 'Published') !!}
							{!! Form::select('published', \App\PhotobookApp::publishState, \App\PhotobookApp::PUBLISHED_YES, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6 col-lg-6">
						
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-6">
				<div class="form-group{{ $errors->has('change_log') ? ' has-error' : '' }}">
					{!! Form::label('change_log', 'Change log') !!}
					{!! Form::textarea('change_log', '', ['class' => 'form-control', 'rows' => '3']) !!}
					@if ($errors->has('change_log'))
		                <span class="error-block">
		                    {{ $errors->first('change_log') }}
		                </span>
		            @endif
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-lg-12 text-right">
				{!! Form::submit('Save', ['class' => 'btn ft-btn']) !!}
				<a href="{{ route('admin.photobook_apps') }}" class="btn btn-default">Cancel</a>
			</div>
		</div>
	{!! Form::close() !!}
@endsection