@extends('admin.layouts.main')

@section('content')
	<div class="row admin-content-header">
		<div class="col-md-6 col-lg-6">
			<h3>Templates</h3>
		</div>
		<div class="col-md-6 col-lg-6">
			<div class="ft-buttons">
				<a href="{{ route('admin.create_template') }}" class="btn ft-btn btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add new</a>
				<a href="{{ route('admin.templates') }}" class="btn ft-btn btn-sm">Reset filter</a>
				<button class="btn ft-btn btn-sm" id="publish_template_btn"><i class="fa fa-check" aria-hidden="true"></i> Publish</button>
				<button class="btn ft-btn btn-sm" id="unpublish_template_btn"><i class="fa fa-minus-circle" aria-hidden="true"></i> Unpublish</button>
				<button class="btn ft-btn btn-sm" id="commingsoon_template_btn"><i class="fa fa-clock-o" aria-hidden="true"></i> Coming soon</button>
			</div>
		</div>
	</div>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); @endphp

	<div class="pagination-row-count text-right">
		{{ $templates->count() . ' of ' . $templates->total() }} records
	</div>
	{!! Form::open(['route' => 'admin.templates', 'method' => 'get', 'name' => 'grid_filter_form', 'id' => 'grid_filter_form']) !!}
	<table class="grid-table">
		<tr>
			<th class="text-center">{!! Form::myCheckbox('check_all_grid_items', '1', false, ['id' => 'check_all_grid_items']) !!}</th>
			<th>Name</th>
			<th width="5%">Version</th>
			<th width="5%">#Pages</th>
			<th>Sizes</th>
			<th width="10%">#Placeholders</th>
			<th>Owner</th>
			<th>Price</th>
			<th width="5%">Discount</th>
			<th width="10%">Storage drive</th>
			<th>Created at</th>
			<th>Published</th>
			<th></th>
		</tr>

		<tr>
			<td></td>
			<td>
				{!! Form::text('filter_tem_name', '', ['class' => 'form-control input-sm filter-input']) !!}
			</td>
			<td></td>
			<td></td>
			<td>
				{!! Form::text('filter_sizes', '', ['class' => 'form-control input-sm filter-input']) !!}
			</td>
			<td></td>
			<td>
				{!! Form::text('filter_user', '', ['class' => 'form-control input-sm filter-input']) !!}
			</td>
			<td></td>
			<td></td>
			<td>
				{!! Form::select('filter_storage_drive', ['' => 'All'] + config('pbconfig.drives'), '', ['class' => 'form-control input-sm filter-input-select']) !!}
			</td>
			<td></td>
			<td>
				{!! Form::select('filter_published', ['' => 'All'] + \App\Template::publishState, '', ['class' => 'form-control input-sm filter-input-select']) !!}
			</td>
			<td></td>
		</tr>

		@if(count($templates) == 0)
		<tr class="no-data"><td colspan="13">No records found!</td></tr>
		@else
			@foreach($templates as $template)
				<tr class="grid-row">
					<td>{!! Form::myCheckbox('template_'.$template->id, $template->id, false, ['class' => 'check-grid-item']) !!}</td>
					<td class="text-center">
						@if($template->thumbnail())
						<img src="{{ $template->thumbnail() }}" width="100" />
						@endif
						<br/>
						{{ $template->tem_name }}
					</td>
					<td class="text-center">{{ $template->version }}</td>
					<td class="text-center">{{ $template->pages }}</td>
					<td>{{ $template->sizes }}</td>
					<td class="text-center">{{ $template->placeholders }}</td>
					<td>{{ $template->user_id == 0 ? 'Public' : $template->user->name }}</td>
					<td>{{ $template->price . $template->currency }}</td>
					<td class="text-center">{{ $template->discount .'%' }}</td>
					<td class="text-center">{{ config('pbconfig.drives')[$template->storage_drive] }}</td>
					<td>@datetime($template->created_at)</td>
					<td class="text-center">@tempStatusIcon($template->published)</td>
					<td class="text-center">
						<a class="btn btn-primary btn-xs" href="{{ route('admin.edit_template', ['id' => $template->id]) }}" title="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach
		@endif
	</table>	
	{!! Form::close() !!}

	{{ $templates->links() }}

	{!! Form::open(['route' => ['admin.publish_templates'], 'method' => 'post', 'name' => 'publish_template_form', 'id' => 'publish_template_form']) !!}
			{!! Form::hidden('grid_item_ids', '', ['id' => 'grid_item_ids']) !!}
			{!! Form::hidden('publish', '', ['id' => 'publish']) !!}
	{!! Form::close() !!}

@stop