@extends('admin.layouts.main')

@section('content')
	<div class="row admin-content-header">
		<div class="col-md-6 col-lg-6">
			<h3>App downloads</h3>
		</div>
		<div class="col-md-6 col-lg-6">
			<div class="ft-buttons">
				<a href="{{ route('admin.app_downloads') }}" class="btn ft-btn btn-sm">Reset filter</a>
			</div>
		</div>
	</div>
	<hr/>

	<div class="pagination-row-count text-right">
		{{ $appDownloads->count() . ' of ' . $appDownloads->total() }} records
	</div>

	{!! Form::open(['route' => ['admin.app_downloads'], 'method' => 'get', 'id' => 'grid_filter_form']) !!}
		<table class="grid-table">
	  		<tr>
	  			<th>User</th>
	  			<th>App serial</th>
	  			<th>App name</th>
	  			<th>#Templates</th>
	  			<th>Activated</th>
	  			<th>Activated on</th>	  			
	  		</tr>
	  		<tr>
	  			<td>{!! Form::text('filter_user', '', ['id' => 'filter_user', 'class' => 'form-control input-sm filter-input']) !!}</td>
	  			<td>{!! Form::text('filter_app_serial', '', ['id' => 'filter_app_serial', 'class' => 'form-control input-sm filter-input']) !!}</td>
	  			<td>{!! Form::text('filter_app_label', '', ['id' => 'filter_app_label', 'class' => 'form-control input-sm filter-input']) !!}</td>
	  			<td></td>	  			
	  			<td>{!! Form::select('filter_is_activated', ['' => 'All'] + \App\AppDownload::appKeyStatus, '', ['id' => 'filter_is_activated', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
	  			<td></td>
	  		</tr>

	  		@if(count($appDownloads) > 0)		
				@foreach($appDownloads as $appDownload)
		  		<tr class="grid-row">
		  			<td>
		  				{{ $appDownload->user->name }}<br/>
		  				<small>{{ $appDownload->user->email }}</small><br/>
		  				<small>{{ $appDownload->user->dial_code.$appDownload->user->phone }}</small>
		  			</td>
		  			<td>{{ $appDownload->app_serial }}</td>
		  			<td>{{ $appDownload->app_label }}</td>
		  			<td class="text-center">{{ $appDownload->templates()->count() }}</td>
		  			<td class="text-center">@statusIcon($appDownload->is_activated)</td>
		  			<td>@datetime($appDownload->activated_date)</td>
		  		</tr>
		  		@endforeach

			@else
				<tr class="no-data"><td colspan="6" >No app downloads found!</td></tr>
			@endif

  		</table>
	{!! Form::close() !!}

	{{ $appDownloads->links() }}
@stop