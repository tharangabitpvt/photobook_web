@extends('admin.layouts.main')

@section('content')
	<div class="row admin-content-header">
		<div class="col-md-7 col-lg-7">
			<h3>User manager <small>(Trashed)</small></h3>
		</div>
		<div class="col-md-5 col-lg-5">
			<div class="ft-buttons">
				<a href="{{ route('admin.create_user') }}" class="btn ft-btn btn-sm"><i class="fa fa-user-plus" aria-hidden="true"></i> Create</a>
				<a href="{{ route('admin.user_manager.trashed') }}" class="btn ft-btn btn-sm">Reset filter</a>
				<button class="btn ft-btn btn-sm" id="restore_user_btn"><i class="fa fa-reply" aria-hidden="true"></i> Restore</button>
			</div>
		</div>
	</div>
	<hr/>
	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
		{
			$checked = $isChecked ? 'checked="checked"' : '';
			$htmlOptions = '';
			foreach($options as $k => $option) {
				$htmlOptions .= $k . "='" . $option . "' ";
			}

		    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
		}); @endphp
	<div class="container-fluid">
		<div class="row">
			<div class="btn-group btn-group-sm ft-button-group" role="group" aria-label="...">
				<a class="btn btn-default" href="{{ route('admin.user_manager') }}"><i class="fa fa-user" aria-hidden="true"></i> Users</a>
				<a class="btn btn-default active" href="{{ route('admin.user_manager.trashed') }}"><i class="fa fa-trash-o" aria-hidden="true"></i> Trashed</a>
			</div>
			{!! Form::open(['route' => 'admin.user_manager.trashed', 'method' => 'get', 'name' => 'grid_filter_form', 'id' => 'grid_filter_form']) !!}
			<table class="grid-table">
				<tr>
					<th class="text-center">{!! Form::myCheckbox('check_all_grid_items', '1', false, ['id' => 'check_all_grid_items']) !!}</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>User type</th>
					<th>Business name</th>
					<th>Deleted at</th>
					<th>Is active</th>
				</tr>

				<tr>
					<td></td>
					<td>
						{!! Form::text('filter_name', '', ['class' => 'form-control input-sm filter-input']) !!}
					</td>
					<td>
						{!! Form::text('filter_email', '', ['class' => 'form-control input-sm filter-input']) !!}
					</td>
					<td>
						{!! Form::text('filter_phone', '', ['class' => 'form-control input-sm filter-input']) !!}
					</td>
					<td>
						{!! Form::select('filter_user_type', ['' => 'All'] + \App\User::userType, '', ['class' => 'form-control input-sm filter-input-select']) !!}
					</td>
					<td>
						{!! Form::text('filter_business_name', '', ['class' => 'form-control input-sm filter-input']) !!}
					</td>
					<td></td>
					<td>
						{!! Form::select('filter_is_active', ['' => 'All'] + \App\User::activeState, '', ['class' => 'form-control input-sm filter-input-select']) !!}
					</td>
				</tr>

				@if(count($usersTrashed) == 0)
				<tr class="no-data"><td colspan="8">No records found!</td></tr>
				@else
					@foreach($usersTrashed as $user)
						<tr class="grid-row">
							<td>{!! Form::myCheckbox('user_'.$user->id, $user->id, false, ['class' => 'check-grid-item']) !!}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->email }}</td>
							<td class="text-center">{{ $user->phone ? $user->dial_code . $user->phone : 'n/a' }}</td>
							<td>{{ \App\User::userType[$user->user_type] }}</td>
							<td>{{ $user->business_name }}</td>
							<td>@datetime($user->deleted_at)</td>
							<td class="text-center">@statusIcon($user->is_active)</td>
						</tr>
					@endforeach
				@endif
			</table>
			<div class="row">
				<div class="col-md-9 col-lg-9">{{ $usersTrashed->links() }}</div>
				<div class="col-md-3 col-lg-3 text-right">
					<div class="pagination-row-count">
						{{ $usersTrashed->count() . ' of ' . $usersTrashed->total() }}
					</div>
				</div>
			</div>
			
			{!! Form::close() !!}

			{!! Form::open(['route' => ['admin.user.restore'], 'method' => 'post', 'name' => 'restore_user_form', 'id' => 'restore_user_form']) !!}
	  			{!! Form::hidden('grid_item_ids', '', ['id' => 'grid_item_ids']) !!}
	  		{!! Form::close() !!}
		</div>
	</div>
@endsection