@extends('admin.layouts.main')

@section('content')
	@macro('payment_status_labels', $status)
		@switch($status)
			@case(\App\Purchase::PAYMENT_STATUS_NOT_PAID)
				@php $badgeClass = 'badge-red' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_PAID)
				@php $badgeClass = 'badge-green' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_PENDING)
				@php $badgeClass = 'badge-blue' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_REJECTED)
				@php $badgeClass = 'badge-brown' @endphp
				@break
			@case(\App\Purchase::PAYMENT_STATUS_CANCELED)
				@php $badgeClass = 'badge-gray' @endphp
				@break
		@endswitch

		<span class="label label-lg {{ $badgeClass }}">{{ \App\Purchase::paymentStatus[$status] }}</span>
	@endmacro
	<div class="row admin-content-header">
		<div class="col-md-6 col-lg-6">
			<h3>Purchases</h3>
		</div>
		<div class="col-md-6 col-lg-6">
			<div class="ft-buttons">
				<div>
					<a href="{{ route('admin.purchases') }}" class="btn ft-btn btn-sm">Reset filter</a>
				</div>
				<div>
					<button class="btn ft-btn btn-sm" id="delete_purchas_items_btn" title="Remove items"><i class="fa fa-times" aria-hidden="true"></i> Remove &amp; send revised invoice</button>
				</div>
			</div>
		</div>
	</div>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); @endphp

	<div class="pagination-row-count text-right">
		{{ $purchases->count() . ' of ' . $purchases->total() }} records
	</div>
	{!! Form::open(['route' => ['admin.purchases'], 'method' => 'get', 'id' => 'grid_filter_form']) !!}
		<table class="grid-table">
	  		<tr>
	  			<th class="text-center">{!! Form::myCheckbox('check_all_grid_items', '1', false, ['id' => 'check_all_grid_items']) !!}</th>
	  			<th>User</th>
	  			<th>Purchase type</th>
	  			<th>Payment method</th>
	  			<th>Price</th>
	  			<th>Invoice No</th>
	  			<th>Purchase time</th>
	  			<th>Payment status</th>
	  			<th>Paid date</th>
	  			<th>Remarks</th>
	  		</tr>
	  		<tr>
	  			<td></td>
	  			<td>{!! Form::text('filter_user', '', ['id' => 'filter_user', 'class' => 'form-control input-sm filter-input']) !!}</td>
	  			<td>{!! Form::select('filter_purchase_type', ['' => 'All'] + \App\Purchase::itemTypeName, '', ['id' => 'filter_purchase_type', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
	  			<td>{!! Form::select('filter_payment_method', ['' => 'All'] + \App\Purchase::paymentMethod, '', ['id' => 'filter_payment_method', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
	  			<td></td>	  			
	  			<td>{!! Form::text('filter_ref_no', '', ['id' => 'filter_ref_no', 'class' => 'form-control input-sm filter-input']) !!}</td>
	  			<td></td>
	  			<td>{!! Form::select('filter_payment_status', ['' => 'All'] + \App\Purchase::paymentStatus, '', ['id' => 'filter_payment_status', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
	  			<td></td>
	  			<td></td>
	  		</tr>

	  		@if(count($purchases) > 0)		
				@foreach($purchases as $purchase)
		  		<tr class="grid-row">
		  			<td>
		  				@if($purchase->payment_status != \App\Purchase::PAYMENT_STATUS_PAID)
		  					{!! Form::myCheckbox('purchase_'.$purchase->id, $purchase->id, false, ['class' => 'check-grid-item']) !!}
		  				@else
		  					<i class="fa fa-check-square text-success" aria-hidden="true"></i>
		  				@endif
		  			</td>
		  			<td>
		  				{{ $purchase->user->name }}<br/>
		  				<small>{{ $purchase->user->email }}</small><br/>
		  				<small>{{ $purchase->user->dial_code.$purchase->user->phone }}</small>
		  			</td>
		  			<td>{{ \App\Purchase::itemTypeName[$purchase->purchase_type] }}</td>
		  			<td>{{ \App\Purchase::paymentMethod[$purchase->payment_method] }}</td>
		  			<td>{{ $purchase->amount . $purchase->currency }}</td>
		  			<td><a href="{{ route('admin.purchases.invoice', ['incoive_no' => $purchase->ref_no]) }}">{{ $purchase->ref_no }}</a></td>
		  			<td>@datetime($purchase->purchase_date)</td>
		  			<td class="text-center">{!! Html::payment_status_labels($purchase->payment_status) !!}</td>
		  			<td>@datetime($purchase->paid_date)</td>
		  			<td>{{ $purchase->remarks }}</td>
		  		</tr>
		  		@endforeach

			@else
				<tr class="no-data"><td colspan="8" >No messages found!</td></tr>
			@endif
	  	</table>
	{!! Form::close() !!}

	{{ $purchases->links() }}

	{!! Form::open(['route' => ['admin.purchases.delete'], 'method' => 'delete', 'name' => 'delete_purchase_form', 'id' => 'delete_purchase_form']) !!}
			{!! Form::hidden('grid_item_ids', '', ['id' => 'grid_item_ids']) !!}
	{!! Form::close() !!}

@stop