@extends('admin.layouts.main')

@section('content')
	<div class="row admin-content-header">
		<div class="col-md-6 col-lg-6">
			<h3>PhotoBook Apps</h3>
		</div>
		<div class="col-md-6 col-lg-6">
			<div class="ft-buttons">
				<a href="{{ route('admin.create_photobook_app') }}" class="btn ft-btn btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add new</a>
				<a href="{{ route('admin.photobook_apps') }}" class="btn ft-btn btn-sm">Reset filter</a>
				<button class="btn ft-btn btn-sm" id="publish_app_btn"><i class="fa fa-check" aria-hidden="true"></i> Publish</button>
				<button class="btn ft-btn btn-sm" id="unpublish_app_btn"><i class="fa fa-minus-circle" aria-hidden="true"></i> Unpublish</button>
			</div>
		</div>
	</div>
	<hr/>

	@php Form::macro('myCheckbox', function($name, $val, $isChecked, $options=[])
	{
		$checked = $isChecked ? 'checked="checked"' : '';
		$htmlOptions = '';
		foreach($options as $k => $option) {
			$htmlOptions .= $k . "='" . $option . "' ";
		}

	    return '<input type="checkbox" name="'. $name .'" id="'. $name .'" value="'. $val .'" '. $checked .' '. $htmlOptions .' />';
	}); @endphp

	<div class="pagination-row-count text-right">
		{{ $photobookApps->count() . ' of ' . $photobookApps->total() }} records
	</div>
	{!! Form::open(['route' => ['admin.photobook_apps'], 'method' => 'get', 'id' => 'grid_filter_form']) !!}
	<table class="grid-table">
  		<tr>
  			<th class="text-center">{!! Form::myCheckbox('check_all_grid_items', '1', false, ['id' => 'check_all_grid_items']) !!}</th>
  			<th>Release name</th>
  			<th>Version</th>
  			<th>Platform</th>
  			<th>Archi</th>
  			<th>Type</th>
  			<th>Stability</th>
  			<th>Is latest</th>
  			<th>Release type</th>
  			<th>Priority</th>
  			<th>Created date</th>
  			<th>Published</th>
  			<th></th>
  		</tr>
  		<tr>
  			<td></td>
  			<td>{!! Form::text('filter_release_name', '', ['id' => 'filter_release_name', 'class' => 'form-control input-sm filter-input']) !!}</td>
  			<td>{!! Form::text('filter_version', '', ['id' => 'filter_version', 'class' => 'form-control input-sm filter-input']) !!}</td>
  			<td>{!! Form::select('filter_platform', ['' => 'All'] + \App\PhotobookApp::platforms, '', ['id' => 'filter_platform', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
  			<td>{!! Form::select('filter_architecture', ['' => 'All'] + \App\PhotobookApp::architectures, '', ['id' => 'filter_architecture', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
  			<td>{!! Form::select('filter_file_type', ['' => 'All'] + \App\PhotobookApp::fileTypes, '', ['id' => 'filter_file_type', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
  			<td>
  				{!! Form::select('filter_stable_level', [''=>'All'] + \App\PhotobookApp::appStableLevel, '', ['id' => 'filter_stable_level', 'class' => 'form-control input-sm filter-input-select']) !!}
  			</td>
  			<td></td>
  			<td>{!! Form::select('filter_release', ['' => 'All'] + \App\PhotobookApp::releaseType, '', ['id' => 'filter_release', 'class' => 'form-control input-sm filter-input-select']) !!}</td>
  			<td>
  				{!! Form::select('filter_priority', [''=>'All'] + \App\PhotobookApp::releasePriority, '', ['id' => 'filter_priority', 'class' => 'form-control input-sm filter-input-select']) !!}
  			</td>
  			<td></td>
  			<td>
  				{!! Form::select('filter_published', [''=>'All'] + \App\PhotobookApp::publishState, '', ['id' => 'filter_published', 'class' => 'form-control input-sm filter-input-select']) !!}
  			</td>
  			<td></td>
  		</tr>
		@if(count($photobookApps) > 0)		
			@foreach($photobookApps as $photobookApp)
	  		<tr class="grid-row">
	  			<td>{!! Form::myCheckbox('grid_item_'.$photobookApp->id, $photobookApp->id, false, ['class' => 'check-grid-item']) !!}</td>
	  			<td>{{ $photobookApp->release_name }}</td>
	  			<td>{{ $photobookApp->version }}</td>
	  			<td>{{ $photobookApp->platform }}</td>
	  			<td class="text-center">{{ $photobookApp->architecture }}</td>
	  			<td class="text-center">{{ $photobookApp->file_type }}</td>
	  			<td>{{ \App\PhotobookApp::appStableLevel[$photobookApp->stable_level] }}</td>
	  			<td>{{ $photobookApp->is_latest == 1 ? 'Yes' : 'No' }}</td>
	  			<td>{{ \App\PhotobookApp::releaseType[$photobookApp->release] }}</td>
	  			<td>{{ \App\PhotobookApp::releasePriority[$photobookApp->priority] }}</td>
	  			<td>@datetime($photobookApp->create_date)</td>
	  			<td class="text-center">@statusIcon($photobookApp->published)</td>
	  			<td class="text-center">
					<a class="btn btn-primary btn-xs" href="{{ route('admin.edit_app', ['id' => $photobookApp->id]) }}" title="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
				</td>
	  		</tr>
	  		@endforeach

		@else
			<tr class="no-data"><td colspan="11" >No records found!</td></tr>
		@endif
	</table>
	{!! Form::close() !!}

	{{ $photobookApps->links() }}
	

	{!! Form::open(['route' => ['admin.photobook_apps.publish'], 'method' => 'post', 'name' => 'pulish_app_form', 'id' => 'pulish_app_form']) !!}
  		{!! Form::hidden('grid_item_ids', '', ['id' => 'grid_item_ids']) !!}
  		{!! Form::hidden('publish', '', ['id' => 'publish']) !!}
  	{!! Form::close() !!}
@endsection