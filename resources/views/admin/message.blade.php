@extends('admin.layouts.main')

@section('content')
	<h3>Message for {{ $message->product }}</h3>
	<hr/>

	<div class="message-details">
		<label>Sender:</label> {{ $message->f_name . " " . $message->l_name }} <br/>
		<label>Sender email:</label> {{ $message->email }}<br/>
		<label>Phone:</label> {{ $message->phone ? $message->phone : 'n/a' }} <br/>
		<label>Sent time:</label> @datetime($message->created_at)
	</div>

	<div class="message-body">
		{{ $message->message }}
	</div>

	<div class="message-reply">
		<h5><i class="fa fa-reply" aria-hidden="true"></i> Reply</h5>
		{!! Form::open(['route' => ['message.reply', $message->id], 'method' => 'put', 'id' => 'msg_reply_form']) !!}
			<div class="form-group">
				{!! Form::textarea('message_reply', $message->reply, ['class' => 'form-control', 'id' => 'message_reply']) !!}
			</div>
			<div class="form-group">
				@if($message->reply == "")
					{!! Form::submit('Send', ['class' => 'btn ft-btn', 'id' => 'msg_reply_btn']) !!}
				@else
					{!! Form::submit('Re-send', ['class' => 'btn ft-btn', 'id' => 'msg_reply_btn']) !!}
				@endif
				
			</div>
		{!! Form::close() !!}
	</div>
@endsection