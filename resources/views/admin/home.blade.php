@extends('admin.layouts.main')

@section('content')
	<h3>Dashboard</h3>
	<hr/>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-lg-4">
				{!! $downloadsChart->html() !!}
			</div>
			<div class="col-lg-4 col-md-4">
				{!! $appActivatedChart->html() !!}
			</div>
		</div>
	</div>

	{!! Charts::scripts() !!}
    {!! $downloadsChart->script() !!}
    {!! $appActivatedChart->script() !!}
@endsection