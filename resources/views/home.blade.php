@extends('layouts.layout')

@section('content')
	<div class="ft-head ft-scrollify-section" data-section-name="home">
		<div class="container">
			<div class="ft-content">
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="key-points">
                            <ul>
                                <li><i class="fa fa-fw fa-thumbs-o-up" aria-hidden="true"></i> Easy to use</li>
                                <li><i class="fa fa-fw fa-clock-o" aria-hidden="true"></i> Save your time</li>
                                <li><i class="fa fa-fw fa-money" aria-hidden="true"></i> Save your money</li>
                                <li><i class="fa fa-fw fa-smile-o" aria-hidden="true"></i> Happy customers</li>
                            </ul>
                        </div>
                        <div class="special-notice">
                            <div class="notice-header"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Notice for Mac users</div>
                            <div class="notice-body">If your Mac blocks installing or openning PhotoBook software, <br/>please do <b>Right click &#8702; Open</b>.<br/>Sorry for the inconvenience faced.<br/><b><small>PhotoBook Team</small></b></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="ft-banner text-center">
                            <img src="{{asset('/images/banner_web.png')}}">
                        </div>
                        <div class="ft-buttons"> 
                            <a href="/downloads">                   
                                <div class="platform-btn" title="download for Windows">
                                    <i class="fa fa-windows fa-2x" aria-hidden="true"></i><br/><span class='platform'>Windows</span>
                                </div>
                            </a>
                             <a href="/downloads">
                                <div class="platform-btn" title="download for Mac">
                                   <i class="fa fa-apple fa-2x" aria-hidden="true"></i><br/><span class='platform'>Mac</span>
                                </div>
                            </a>
                            <div class="platform-btn down-for-this-computer" title="download for this computer">
                                <i class="fa fa-arrow-circle-o-down fa-2x" aria-hidden="true"></i><br/>
                                <span class="ft-platform"></span>
                            </div>
                            <a href="/" class="download-userguide">
                                <div class="platform-btn" title="User guide">
                                    <i class="fa fa-info-circle fa-2x" aria-hidden="true"></i><br/><span class='platform'>Guide</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="special-news">
                            @php $newsConfig = config('pbconfig.news') @endphp
                            @if($newsConfig['published'])
                                @if($newsConfig['link'])
                                <a href="{{ $newsConfig['link'] }}">
                                @endif
                                    <div class="news-header blink">{{ $newsConfig['title'] }}</div>
                                    <div class="news-body">{{ $newsConfig['body'] }}</div>
                                    <div class="news-footer">{{ $newsConfig['footer'] }}</div>
                                @if($newsConfig['link'])
                                </a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>

			</div>
		</div>
        <div class="header-bottom">
            <a href="#about-photobook" class="scroll scroll-pointer">Scroll for more</a>
        </div>
	</div>
	
	@include('partials.about_photobook')

	@include('partials.products_and_services')

	@include('partials.latest_templates')

	@include('partials.contact_us')

    <div class="back-to-top" title="got to top">
        <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>

	<script>
        $(function() {
          $.scrollify({
            section : ".ft-scrollify-section",
            sectionName : "section-name",
            interstitialSection: ".footer",
            scrollSpeed: 600,
            offset: -51,
            before: function(index, sections) {
                if(index == 0) {
                    $('.ft-banner').hide();
                    $('.key-points').hide();
                    $('.platform-btn').hide();
                    $('.special-news').hide();
                    $('.special-notice').hide();
                    $('.back-to-top').fadeOut('slow');
                } else if(index == 1) {
            		$('.ft-summary').hide();
            	} else if(index == 2) {
            		$('.ft-service-box').hide();
                    $('.show-all-btn').hide();
            	} else if(index == 3) {
            		$('.ft-template-box').hide();
                    $('.show-all-btn').hide();
            	}

                if(index > 0) {
                    $('.back-to-top').fadeIn('slow');
                }
            },
            after:function(index, sections) {
                if(index == 0) {
                    $('.ft-banner').addClass('animated fadeInDown').show();
                    $('.key-points').addClass('animated fadeInLeft').show();
                    $('.special-news').addClass('animated fadeInRight').show();
                    $('.special-notice').addClass('animated fadeInLeft').show();
                    $.each($('.platform-btn'), function(k, obj){
                        if(!$(this).hasClass('no-for-this-computer')) {
                            $(this).addClass('animated bounceIn').show();
                        } 
                    });
                                       
                } else if(index == 1) {
            		$('.ft-summary').addClass('animated flipInY');
        			$('.ft-summary').show();
            	} else if(index == 2) {
            		$('.ft-service-box').addClass('animated bounceIn');
        			$('.ft-service-box').show();

                    $('.show-all-btn').addClass('animated flipInX');
                    $('.show-all-btn').show();
            	} else if(index == 3) {
            		//$('.ft-template-box').addClass('animated fadeIn');
            		$('.ft-template-box').each(function(k, v){
            			if(k == 0) {
            				$(this).fadeIn(500);
            			} else {
            				$(this).delay(200).fadeIn(500);
            			}
            		});

                    $('.show-all-btn').addClass('animated flipInX');
                    $('.show-all-btn').show();
            	}
            },
          });
        });
  	</script>
@endsection