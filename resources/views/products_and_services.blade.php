@extends('layouts.layout')

@section('content')
	<div class="container inner-page">
		<h3>PhotoBook album design software</h3>
		<p>
			PhotoBook is the first product of a series of our elegant software applications which you can use in your day to day life.
		</p>
		<p>
			PhotoBook is especially designed for photography personals, graphic designers and wedding album makers. It consists of unlimited album design templates which carefully planed and designed by our experienced graphic designers. Everybody can download new templates from our website photobook.lk anytime. Templates list is being updated ceaselessly.
		</p>
		<p>
			Once your are registered with the website and you have an activated PhotoBook app, you are good to go! Your PhotoBook account and the application is always connected. Click <a href="{{ route('register') }}">here</a> if you do not have an account yet.
		</p>
		<p><button class="btn ft-btn download-userguide"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i> Download user guide now</button></p>

		<br/>
		<h3>PhotoBook Templates designing</h3>
		<p>
			PhotoBook Templates are the useful element of PhotoBook application. We are responsible for develop brand new unique template designs for you PhotoBook application. Basically almost all the templates are public. Everyone can buy them through our website photobook.lk.
		</p>
		<p>
			Apart from that, we are happy to inform you that, you can request for totally private templates for your PhotoBook application as well. These templates are called <b>"Private Templates"</b>. We do not publish them on our web at all. You will have them under a different license agreement.
		</p>

		<br/>
		<h3>Photography album designing</h3>
		<p>
			We do photography album designing. If you need to get special and private attention for your album designing job, we are willing to fulfill your requirements too. We have a dedicated designing team for private album designing. So, we guarantee on-time album completion. We provide free on-line <span class="help-popover" data-toggle="popover" title="Proofing" data-placement="top" data-trigger="hover" data-content="We provide an interface for you to preview your design work. You can add comments on each design if you need any changes or accept as a final design.">proofing</span> while our design job is going on. Then you can check them and propose the changes. 
		</p>

		<br/>
		<h3>Artistic Fonts designing</h3>
		<p>
			Artistic fonts are really important for an attractive graphic design. Our designers and programmers can design fonts for you according to your needs. We have our own fonts list which you can buy separately as a package.
		</p>
		<p>
			If you need to design your own fonts, we are ready to do that for you. We will design and provide installable fonts package as your request.
		</p>

		<br/>
		<h3>Artwork designing</h3>
		<p>
			Designers know the importance of an SVG artworks for a perfect design. PhotoBook has a bunch of creative artwork artists. They are capable of producing phenomenal results for our Artwork Designing section. Graphic designers and our software team converts them for using in your graphic designs. You will be able feel a unique experience with these artworks we produce for you.
		</p>
	</div>
@endsection