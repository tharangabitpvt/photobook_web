@extends('emails.layout')

@section('content')

<p><b>Contact name: </b>{{ $contactName }}</p>
<p><b>Email: </b>{{ $contactEmail }}</p>
<p><b>Phone: </b>{{ $contactPhone }}</p>
<p><b>Product or service: </b>{{ $contactReason }}</p>
<p><b>Message:</b><br/>{{ $contactMessage }}</p>

@endsection