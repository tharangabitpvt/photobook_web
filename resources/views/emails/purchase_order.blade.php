@extends('emails.layout')

@section('content')

Dear {{$userName}},

<p>Thank you for purchasing with PhotoBook.lk.</p>
<p>Your order reference number is {{ $orderRefNo }}.</p>
<p>Please find attached Invoice ({{ $orderRefNo }}.pdf) for more details.</p>

@if($paymentStatus == App\Purchase::PAYMENT_STATUS_NOT_PAID)
	<p>Please make your payment without any delay.</p>
	<p>Remember to inform about your payment details once the payment is done.</p>
	<p>Once you done the payment and let us know the payment details, purchased items will be available for you.</p>
@endif

@if($paymentStatus == App\Purchase::PAYMENT_STATUS_PAID)
	<p>Thank you for making payment for your order ({{ $orderRefNo }}).</p>
	<p>Hope that, you are enjoying working with PhotoBook.</p>
@endif

@endsection