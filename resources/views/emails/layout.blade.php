<div style="width: 97%; padding: 20px; font-family: Arial, Helvetica, sans-serif;">
	<div style="color: #00656f; margin-bottom: 20px; background: #eff3f5; padding: 5px; border-bottom: 2px solid #bcd3da;">
		<div style="display: table-cell; width: 125px; margin-right: 10px;">
			<img src="http://photobook.lk/images/square_logo.png" width="100" />
		</div>
		<div style="display: table-cell; vertical-align: middle; padding-right: 50px; border-right: 1px dashed #99c4d4;">
			<div style="font-size: 30px; font-weight: bold; color: #00656f">PhotoBook</div>
			<div style="color: #0e919e; font-size: 14px;">brings you the creativity</div>
		</div>
		<div style="display: table-cell; vertical-align: middle; margin-left: 10px; padding-left: 50px; padding-right: 10px; font-size: 12px;">
			<div style="margin-bottom: 7px; border-bottom: 1px dashed #91bcd2;"><b>Phone:</b> {{ config('pbconfig.contactNo') }}</div>
			<div style="margin-bottom: 7px; border-bottom: 1px dashed #91bcd2;"><b>Email:</b> {{ config('pbconfig.salesEmail') }}</div>
			<div style="margin-bottom: 7px; border-bottom: 1px dashed #91bcd2;"><b>Website:</b> http://photobook.lk</div>
		</div>
	</div>
	
	<div style="margin-top: 20px; font-size: 14px;">
		@yield('content')
	</div>

	<div style="margin-top: 10px; font-size: 14px;">
		<p>Thank you for working with PhotoBook.lk.</p>
		<p>If you have any further inquiry, please contact us over {{ config('pbconfig.contactNo') }}/{{ config('pbconfig.salesEmail') }} in our openning hours.</p>
	</div>

	<div style="margin-top: 20px; font-size: 14px;">Thank you</div>
	<div style="font-size: 14px;"><b>PhotoBook Team</b></div>

	<div style="margin-top: 10px; font-size: 14px;">
		<b>Openning hours:</b><br/>
		{{ config('pbconfig.openningHours') }}
	</div>
	
	<div style="border-top: 1px solid #eee; margin-top: 20px; padding: 10px 0; font-size: 11px; text-decoration: italic;">
		<b>Disclaimer</b><br/>
		This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the PhotoBook if possible. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.
	</div>
</div>