@extends('emails.layout')

@section('content')
	<p style="margin-bottom: 10px;">Dear {{ $username }},</p>
	<p style="margin-bottom: 10px;">Thank you for contacting PhotoBook.</p>
	<p style="margin-bottom: 10px;">You were asking about {{ $messageReason }}.</p>
	<p><b>Your ariginal message was:</b></p>
	<p style="margin-bottom: 10px;">{{ $originalMessage }}</p>
	<p><b>Our reply:</b></p>
	<p>{{ $messageBody }}</p>
@endsection