@extends('emails.layout')

@section('content')

Dear {{$userName}},

@if($appKey)
	<p>Your PhotoBook activation key: {{ $appKey }}</p>
	<p>Please use this key for activating your PhotoBook application.</p>
	<p>This key is a one time activation key. Cannot be re-used.!</p>
@else
	<p>Congratulations!! Your PhotoBook Application is now activated.</p>
	<p>Enjoy your lifetime app activation.!</p>
@endif

@endsection