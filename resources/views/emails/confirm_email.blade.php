@extends('emails.layout')

@section('content')

Dear {{$userName}},
<p>Welcome to PhotoBook.</p>
<p>Contratulations! Your registration is completed.</p>
<p>Please click on following link to verify your email address. Then your PhotoBook account will be activated.</p>
<a href="{{$confirmEmailLink}}">{{$confirmEmailLink}}</a>

@endsection