$(document).ready(function(){

	var collection_items = [];

	swal.setDefaults({
		buttonsStyling: false,
		confirmButtonClass: 'btn ft-btn',
		cancelButtonClass: 'btn btn-default',
	});

	var ft = {
		'showPopover' : function(popover, content) {

			if (typeof(popover)==='undefined') popover = null;
			if (typeof(content)==='undefined') content = "";

			if(popover && content != "") {
				$('#' + popover).popover();
				$('#' + popover).attr('data-content', content);
				$('#' + popover).popover('show');
				setTimeout(function(){
					$('#' + popover).popover('hide');
				}, 3000);
			}
			
		},

		'loadingDialog': function(message) {
			swal({
				title: '<div class="swal-loading"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + message + '</div>',
				showConfirmButton: false,
				showCancelButton: false,
				customClass: 'loading-swal',
				allowOutsideClick: false,
				allowEscapeKey: false,
			});
		},

		'closeSwal': function() {
			if(swal.isVisible()){
				swal.close();
			}
		}
	}

	if(shopColSession != "") {
		collection_items = shopColSession;
		//console.log(collection_items);
		$('#ft_shop_collection').find('.collection-items').html(collection_items.length);
	}

	var platformFamily = platform.os.family;
	//console.log(platformFamily, platform.os.architecture);
	if(platformFamily == 'Windows' || platformFamily == "Mac" || platformFamily == "OS X") {
		var architecture = platform.os.architecture;

		var platformIcon = '<i class="fa fa-windows" aria-hidden="true"></i>';
		$('#app_platform').val("Windows");
		$('#app_file_type').val('exe');
		
		if(platformFamily == "Mac" || platformFamily == "OS X") {
			if(architecture == 64) {
				platformIcon = '<i class="fa fa-apple" aria-hidden="true"></i>';
				$('#app_platform').val("Mac OS");
				$('#app_file_type').val('dmg');
			} else {
				$('.down-for-this-computer').addClass('no-for-this-computer');
				$('.down-for-this-computer').hide();
			}
			
		}
		var platformStr = platformIcon + "&nbsp;" + architecture + "bit ";
		$('.ft-platform').html(platformStr);
		$('#app_arch').val(architecture);

	} else {
		$('.down-for-this-computer').addClass('no-for-this-computer');
		$('.down-for-this-computer').hide();
	}

	$('.down-for-this-computer').on('click', function(e){
		e.preventDefault();
		//ft.loadingDialog('Your download will start shortly...');
		swal({
			title: 'PhotoBook App download',
			html: '<i class="fa fa-download" aria-hidden="true"></i> Your download will start shortly.',
		});

		/*$.ajax({
			method: 'POST',
			url: '/download_trail',
			data: {app_platform: $('#app_platform').val(), app_arch: $('#app_arch').val(), app_latest: 'true'},
			success: function(resp) {
				console.log(resp);
			}
		});*/

		$('#latest_app_download_form').submit();
	});

	$('.app-download-btn').on('click', function(e){
		e.preventDefault();

		var dowmloadItemId = $(this).data('download-itemid');
		$('#installer_id').val(dowmloadItemId);
		$('#app_download_form').submit();
	});

	$('.scroll-pointer').on('click', function(e){
		e.preventDefault();

		$.scrollify.move("#about-photobook");
	});

	$('#template_preview_modal').on('show.bs.modal', function(event){
		var clickItem = $(event.relatedTarget);
		var itemId = clickItem.data('template-id');
		var temName = clickItem.data('template-name');
		var priceLKR = clickItem.data('price-lkr');
		var priceUSD = clickItem.data('price-usd');
		var tmplStatus = clickItem.data('tmpl-status');

		$('#album_name_on_preview').html(temName);
		if(tmplStatus == 1) {
			$('#template_preview_modal').find('.album-buttons').show();
			$('#template_preview_modal').find('.price-lkr').html(priceLKR);
			$('#template_preview_modal').find('.price-usd').html(priceUSD);
			$('#template_preview_modal').find('.buy-template-btn').data('ft-itemid', itemId);
		} else {
			$('#template_preview_modal').find('.album-buttons').hide();
		}
		

		$.scrollify.disable({
			section : ".ft-scrollify-section",
		});
	});

	$('#template_preview_modal').on('hidden.bs.modal', function(event){
		$('#template_preview_modal').find('.modal-body').html('');
		$.scrollify.enable({
			section : ".ft-scrollify-section",
		});
	});

	$('#template_preview_modal').on('shown.bs.modal', function(event) {
		var clickItem = $(event.relatedTarget);
		var tempId = clickItem.data('template-id');

		$('#template_preview_modal').find('.modal-body').html('<div class="loading-div"><div class="loader"></div></div>');
		// load thumnails
		$.ajax({
		    method: 'GET', 
		    url: '/template/thumb',
		    data: {'id' : tempId},
		    success: function(html){
		    	if(html) {
					$('#template_preview_modal').find('.modal-body').html(html);
					$('#template_preview_modal').find('.carousel').carousel();
		    	}		        
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		    	alert("something went wrong! Try again later.")
		        //console.log(JSON.stringify(jqXHR));
		        //console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		    }
		});
	});

	// add to collection - template
	$('.buy-template-btn').on('click', function(e){
		e.preventDefault();
		var itemId = $(this).data('ft-itemid');
		var itemType = $(this).data('ft-item-type');
		var appSerial = $(this).data('app-serial');

		var itemObj = {item_type: itemType, item_id: itemId, app_serial: appSerial};


		if($.grep(collection_items, function(n) { return n.item_type == itemType && n.item_id == itemId && n.app_serial == appSerial; }).length == 0) {
			collection_items.push(itemObj);
		} else {
			return;
		}			

		//console.log(collection_items);
		
		addToCollection();

	});

	function addToCollection(callback) {
		$.ajax({
			method: 'POST',
			url: '/purchase/addtocart',
            data: {collection: collection_items},
            success: function(resp) {
            	var itemCount = 0;
            	if(resp !== 'false') {
            		itemCount = resp.length;          		
            	}
            	$('#ft_shop_collection').find('.collection-items').html(itemCount);
        		$('#ft_shop_collection').find('.collection-items').removeClass('animated bounce');
        		setTimeout(function(){
        			$('#ft_shop_collection').find('.collection-items').addClass('animated bounce');
        		},50);

        		if(itemCount == 0) {
        			ft.showPopover('ft_shop_collection', 'No items in the collection.');
        		} else if(itemCount == 1) {
        			ft.showPopover('ft_shop_collection', '1 item in the collection.');
        		} else {
        			ft.showPopover('ft_shop_collection', itemCount + ' items in the collection.');
        		}

        		if(typeof callback === 'function') {
        			callback();
        		}
        		
            },
            error: function(jqXHR, textStatus, errorThrown) {
            	//ajax failed
            }
		});
	}

	function updateShoppingCollection() {
		$('#shop_collection_modal').find('.shoping-items-list').html('<div class="loader"></div>');
		
		$.ajax({
			method: 'post',
			url: '/purchase/show_collection',
			data: {collection: collection_items},
			success: function(resp) {
				$('#shop_collection_modal').find('.shoping-items-list').html(resp);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				// ajax error
			}
		});
	}

	$('#shop_collection_modal').on('shown.bs.modal', function(event){
		var clickItem = $(event.relatedTarget);
		$(this).find('.item-count').html('(' + collection_items.length + ' items)');
		//console.log(collection_items);
		updateShoppingCollection();
	});

	$('#shop_collection_modal').on('hidden.bs.modal', function(event){
		$(this).find('.shoping-items-list').html('<div class="loader"></div>');
	});

	// show collection
	$('#ft_shop_collection').on('click', function(e){
		e.preventDefault();
		if(collection_items.length == 0) {
			return;
		}
		$('#shop_collection_modal').modal('show');
	});

	// remove item from collection
	$('#shop_collection_modal').on('click', '.remove-item', function(e){
		e.preventDefault();

		var itemId = $(this).data('item-id');
		var itemType = $(this).data('item-type');

		swal({
			title: 'Remove item from the collection',
			html: 'Do you want to remove this item from your collection?',
			showConfirmButton: true,
			showCancelButton: true,
			confirmButtonText: 'Yes, go ahead',
		}).then(function(confirmed){
			if(confirmed) {
				var currentCollection = collection_items;
				collection_items = $.grep(currentCollection, function(n) { return n.item_type != itemType && n.item_id != itemId; })

				addToCollection();

				if(collection_items.length > 0) {
    				updateShoppingCollection();
    			} else {
    				$('#shop_collection_modal').modal('hide');
    			}
			} else {
				return;
			}
		})
		.catch(function (err) {
	        console.log(err)
      	});
	});

	// discard shopping collection
	$('.ft-discard-collection-btn').on('click', function(e){
		e.preventDefault();

		swal({
			title: 'Discard collection',
			html: 'Do you want discard your collection?<br/><small><i class="fa fa-info-circle" aria-hidden="true"></i> Remember, you have pay later option. You can save this collection and pay later.</small>',
			showConfirmButton: true,
			showCancelButton: true,
			confirmButtonText: 'Yes, discard it',
			cancelButtonText: 'No, go back',
		}).then(function(){
			collection_items = [];
			addToCollection(returnToHome);
			$('#shop_collection_modal').modal('hide');
		});		
	});

	function returnToHome() {
		window.location.assign('/');
	}

	// complete purches
	$('.payment-options').on('click', '.item', function(e){
		e.preventDefault();

		if($(this).hasClass('na')) {
			return;
		}

		$(this).find('.payment-type-radio').prop('checked', true);
	});

	$('#purchase_complete_btn').on('click', function(e){
		e.preventDefault();

		swal({
			title: 'Complete your purchase',
			html: 'Ready to complete your purchase request?<br/><small>Make sure that correct payment option is selected.</small>',
			showConfirmButton: true,
			showCancelButton: true,
			confirmButtonText: 'Yes, go ahead',
			cancelButtonText: 'No, I want to check again',
		}).then(function(res) {
			if(res){
				$('#checkout_form').submit();
			} else {
				return false;
			}
		});
	});

	$('#ft_navbar').on('click', '.get-app-key', function(e){
		e.preventDefault();
		var itemType = $(this).data('item-type');
		var itemId = 0;

		var itemObj = {item_type: itemType, item_id: itemId};

		if($.grep(collection_items, function(n) { return n.item_type == itemType && n.item_id == itemId; }).length == 0) {
			collection_items.push(itemObj);

			addToCollection(returnToHome);
		} else {
			$('#shop_collection_modal').modal('show');
			return;
		}		
		
	});

	$('#dashboard_tabs a[href="#profile"]').on('shown.bs.tab', function(e){
		$('#profile').find('.ft-select2').select2({
			theme: "bootstrap",
		});
	});

	$('.ft-select2').select2({
		theme: "bootstrap",
	});

	$('[data-toggle="popover"]').popover();

	$('#tc_modal').on('shown.bs.modal', function(e){
		$('#tc_modal .modal-body').scrollTop(0);
		$.scrollify.disable();
	});

	$('#tc_modal').on('hidden.bs.modal', function(e){
		$.scrollify.enable();
	});

	$('.ft-tc-link').on('click', function(e){
		e.preventDefault();
		$('#tc_modal').modal('show');
	});

	$('.contact-us-btn').on('click', function(e){
		e.preventDefault();
		var contactFor = $(this).data('contactfor');

		$('#contact_for').val(contactFor);
		$.scrollify.move('#contact-us');
	});

	$('.back-to-top').on('click', function(e){
		$.scrollify.move('#home');
	});

	$('.download-userguide').on('click', function(e){
		e.preventDefault();

		$('#user_guide_modal').modal('show');
	});
	
});