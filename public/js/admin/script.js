$(document).ready(function(){
	swal.setDefaults({
		buttonsStyling: false,
		confirmButtonClass: 'btn ft-btn',
		cancelButtonClass: 'btn btn-default',
	});

	$('.inbox-message').on('click', function(e){
		e.preventDefault();
		
		var messageId = $(this).data('message-id');
		var url = '/admin/messages/view/' + messageId;
		window.location.href = url;
	});

	$('#msg_reply_btn').on('click', function(e){
		e.preventDefault();

		if($('#message_reply').val() == "") {
			swal({
				title: 'PhotoBook Messages',
				html: 'Please enter some text on reply box.',
			});

			return false;
		} else {
			$('#msg_reply_form').submit();
		}
	});

	var selectedGridItems = [];
	$('#check_all_grid_items').on('change', function(e){
		$('.check-grid-item').each(function(){
			$(this).prop('checked', !$(this).prop('checked'));
			if($(this).prop('checked')) {
				selectedGridItems.push($(this).val());
			} else {
				selectedGridItems.splice($.inArray($(this).val(), selectedGridItems), 1);
			}
		});

		$('#grid_item_ids').val(selectedGridItems);
	});

	$('table tr .check-grid-item').on('click', function(e){
		e.stopPropagation();
	    
	    if(($(this).prop('checked'))) {
	    	selectedGridItems.push($(this).val());
	    } else {
	    	selectedGridItems.splice($.inArray($(this).val(), selectedGridItems), 1);
	    }

	    $('#grid_item_ids').val(selectedGridItems);
	});

	$('#msg_delete_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Delete messages',
				html: 'Please select messages you want to delete.',
			})
		} else {
			swal({
				title: 'Delete messages',
				html: 'Do you really want to delete selected messages?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#delete_msg_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});;
			
		}
	});

	$('#grid_filter_form').on('keypress', '.filter-input', function(e){
		if(e.which == 13) {
			$('#grid_filter_form').submit();
		}
	});

	$('#grid_filter_form').on('blur', '.filter-input', function(e){
		$('#grid_filter_form').submit();
	});

	$('#grid_filter_form').on('change', '.filter-input-select', function(e){
		$('#grid_filter_form').submit();
	});

	$('#trash_user_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Delete users',
				html: 'Please select user(s) you want to delete.',
			})
		} else {
			swal({
				title: 'Delete users',
				html: 'Do you really want to delete selected user(s)?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#delete_user_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});;
			
		}
	});

	$('#restore_user_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Restore users',
				html: 'Please select user(s) you want to restore.',
			})
		} else {
			swal({
				title: 'Restore users',
				html: 'Do you really want to restore selected user(s)?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#restore_user_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});;
			
		}
	});

	$('#publish_app_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Publish app',
				html: 'Please select app(s) you want to publish.',
			})
		} else {
			swal({
				title: 'Publish app',
				html: 'Do you really want to publish selected app(s)?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#publish').val('publish');
					$('#pulish_app_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});;
			
		}
	});

	$('#unpublish_app_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Unpublish app',
				html: 'Please select app(s) you want to un-publish.',
			})
		} else {
			swal({
				title: 'Unpublish app',
				html: 'Do you really want to un-publish selected app(s)?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#publish').val('unpublish');
					$('#pulish_app_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});;
			
		}
	});

	$('#publish_template_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Publish template',
				html: 'Please select template(s) you want to publish.',
			})
		} else {
			swal({
				title: 'Publish template',
				html: 'Do you really want to publish selected template(s)?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#publish').val('publish');
					$('#publish_template_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});;
			
		}
	});

	$('#unpublish_template_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Unpublish template',
				html: 'Please select template(s) you want to un-publish.',
			})
		} else {
			swal({
				title: 'Unpublish template',
				html: 'Do you really want to un-publish selected template(s)?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#publish').val('unpublish');
					$('#publish_template_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});
			
		}
	});

	$('#commingsoon_template_btn').on('click', function(e){
		if(selectedGridItems.length == 0) {
			swal({
				title: 'Coming soon template',
				html: 'Please select template(s) you want to set as coming soon.',
			})
		} else {
			swal({
				title: 'Coming soon template',
				html: 'Do you really want to set selected template(s) as coming soon?',
				showConfirmButton: true,
				showCancelButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#publish').val('coming_soon');
					$('#publish_template_form').submit();
				}
			})
			.catch(function (err) {
		        console.log(err)
	      	});
			
		}
	});

	$('#delete_purchas_items_btn').on('click', function(e){
		e.preventDefault();

		if(selectedGridItems.length == 0) {
			swal({
				title: 'Remove purchase items',
				html: 'Please select item(s) you want to remove.',
			})
		} else {
			swal({
				title: 'Remove purchase items',
				html: 'Remove items from purhcase list?',
				showCancelButton: true,
				showConfirmButton: true,
			}).then((confirmed) => {
				if(confirmed) {
					$('#delete_purchase_form').submit();
				}
			}).catch(function (err) {
		        console.log(err)
	      	});
		}
		
	});

	$('#change_payment_status_dropdown li a').on('click', function(e){
		e.preventDefault();

		swal({
			title: 'Change Payment status',
			html: 'You are going to change the payment status of this invoice.<br/>Are you sure?',
			showCancelButton: true,
			showConfirmButton: true,
		}).then((confirmed) => {
			if(confirmed) {
				var paymentStatus = $(this).data('payment-status');
				$('#payment_status').val(paymentStatus);
				$('#set_payment_method_form').submit();
			}
		}).catch(function (err) {
	        console.log(err)
      	});
	});

	$('#select_all_sizes').on('click', function(e){
		e.preventDefault();

		var sizes = $(this).data('sizes').split(",");
		console.log(sizes);
		$('#sizes').val(sizes);
		$('#sizes').trigger('change');
	});

	$('.window-close').on('click', function(e){
		swal({
			title: 'Change Payment status',
			html: 'Do you want to close this window?',
			showCancelButton: true,
			showConfirmButton: true,
		}).then((confirmed) => {
			if(confirmed) {
				window.close();
			}
		}).catch(function (err) {
	        console.log(err)
      	});
	});

	$('.menu-items li').popover({
		trigger: 'hover',
	});
});